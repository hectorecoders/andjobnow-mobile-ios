
# AndJobNow iOS

Frontend preparado para la integración con la api de AndJobNow, realizado en React Native. 



## Información técnica

La aplicación está desarrollada en [React Native](https://facebook.github.io/react-native/) v.0.55.4 y diseñada para compilar version en iOS.

### Librerías principales

A continuación se mostrará un breve resumen de cada una de las librerías principales para el funcionamiento correcto de la App.

-  [**React-navigation**](https://reactnavigation.org) : Conjunto de componentes y funciones utilizados para establecer un uso cómodo en la navegación durante la app. v.1.5.7

-  [**fbsdk**](https://github.com/facebook/react-native-fbsdk) : Librería encargada de toda la lógica de registro, inicio y cierre de sesión por parte del usuario a través de facebook. v.0.7.0

-  [**fcm**](https://github.com/evollu/react-native-fcm): React native fcm te permite la integración completa de todos los servicios disponibles de firebase dentro de react native. v.16.2.0

-  [**Gifted chat**](https://github.com/FaridSafi/react-native-gifted-chat): Componente de chat con un gran abanico de funcionalidades para su uso. v.0.4.3

-  [**React-native-modal**](https://github.com/react-native-community/react-native-modal): Componente que ofrece un modal con múltiples props para su configuración. v.5.3.0

-  [**React-native-swiper**](https://github.com/leecade/react-native-swiper): Swiper utilizado para deslizar las proposal en el Wall y en Requests. v.1.5.13

-  [**React-native-webrtc**](https://github.com/oney/react-native-webrtc): Librería utilizada para la integración de llamadas de voz y video por p2p con la tecnología de webrtc. v.1.67.1

-  [**React-native-incall-manager**](https://github.com/zxcpoiu/react-native-incall-manager): Librería utilizada junto a las funciones de Webrtc para el control total de los streams de video y audio durante una llamada. v.3.2.2

-  [**React-native-document-picker**](https://github.com/Elyx0/react-native-document-picker): Se utilizará para poder seleccionar archivos dentro de una conversación dando acceso al sistema de ficheros de iCloud. v.2.1.0

A su vez se utilizan otras librerías no tan esenciales con un uso básico en la app, como puede ser los Date picker, multiple select picker, calendar, easy-toast, vector icons, sound-player, image-picker...



## Estructura

El código de la app está dividido en un conjunto de clases que conformarán cada una de las pantallas disponibles, para esto se utiliza el archivo **App.js** como base inicial donde se controlarán cada una de las operaciones iniciales para la persistencia de la sesión, la obtención de datos pertinente y la estructura de enrutamiento que se seguirá para la navegación, haciendo uso del **TabNavigator** proporcionado por React-navigation para una vez iniciada la sesión trabajar con una navegación por Tabs.

####globales.js
Este archivo es utilizado para usar la persistencia de variables a lo largo del ciclo de vida de la app. Información sensible como el **Token** del usuario, su **ID** e información relativa al usuario, así como las url de acceso a la api y almacenamientod e imágenes: **apiURL** y **imageUrl** respectivamente.

La mayoría de esa información es almacenada también localmente  en el dispositivo (**AsyncStorage**) una vez se inicia sesión correctamente, permitiendo la persistencia de la sesión y su eliminación tras el cierre de esta. Ejemplos:

**Almacenar items**	
			
```sh
AsyncStorage.setItem('jobToken', globales.token.toString());
AsyncStorage.setItem('jobUID', user.id.toString());
```

**Obtención de items**			
			
```sh
const token = await AsyncStorage.getItem('jobToken');
const UID = await AsyncStorage.getItem('jobUID');
```

**Eliminar items**			
			
```sh
AsyncStorage.removeItem('jobToken');
AsyncStorage.removeItem('jobUID');
```

###Clases

Cada una de las llamadas a la API de AndJobNow cuenta con la siguientes estructura:

```sh
return fetch(globales.apiURL+'/api/offer/application-offer',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {

				if (responseJson["status"] == "success") {
					this.refs.toastOk.show(responseJson["message"], 2000);
					this.actualizarCandidaturas(propsOferta)
				} else {
					this.refs.toastKo.show(responseJson["message"], 2000);
				}
			})
			.catch((error) => {
				console.error(error);
			});
```
Como se observa en la función fetch, se indica la ruta en los parámetros junto al contenido necesario para esa llamada. El parámetro **'Authorization'** se mandará únicamente cuanto sea necesario mandar el token para la petición.

También cabe destacar que la mayoría de clases dispone acceso al archivo de globales.js y funciones de logout individuales.


-  **Login, Recover y Register**: Cada una de estas clases corresponde a las funcionalidades de logueo, recuperación de contraseña y registro por parte del usuario respectivamente. 

Si el inicio de sesión ya sea por persistencia o por inserción de datos, en la clase de **App.js** se hará un enrutamiento a al **TabNavigator** disponible en  Navigation.js:

-  **Navigation**: Aquí se gestionará la navegación de la app una vez iniciada la sesión. Para la integración de la videollamada se ha creado un **StackNavigator** que integra la vista de las ofertas y la de videollamada para ser accesible en cualquier momento

```sh
const OfertasStack = StackNavigator({
	Ofertas: {
	  screen: Ofertas,
	  navigationOptions: () => ({
		title: 'Explorar Ofertes',
		swipeEnabled: false,
	  }),
	},
	Video: {
		screen: Video,
		navigationOptions: () => ({
		  title: 'Videocall',
		  swipeEnabled: false,
		}),
	  }
},
	{
		initialRouteName: 'Ofertas',
		headerMode: 'none' ,
		navigationOptions:({ navigation }) => ({
		  tabBarOnPress: ({ scene, jumpToIndex }) => {
			const { route, focused, index } = scene;
			if (focused) {
			  if (route.index > 0) {
				const { routeName, key } = route.routes[1]
				navigation.dispatch(NavigationActions.back({ key }))
			  }
			} else {
			  jumpToIndex(index);
			}
		  },
		}),
	  },
	);
```

-  **Ofertas**: Clase inicial accesible una vez se ha iniciado sesión. Esta clase contiene toda la funcionalidad para la obtención de las ofertas disponibles desde la API, el filtrado de las mismas y el rechazo o la inscripción de cada una de ellas. Se hará uso de la librería **react-native-deck-swiper** para la creación de tarjetas deslizables. La clase ofertas a su vez es la que lleva el control de **notificaciones** en caso de mensajes y para recibir la videollamada.

```sh
startNotis(userID) {

		FCM.subscribeToTopic('notifications_' + userID);

		FCM.getInitialNotification().then((notif) => {
			if (notif && notif.opened_from_tray) {
				if(notif.custom_notification){
					notiData = JSON.parse(notif.custom_notification);
					if (notiData.type == "call") {
						SoundPlayer.playSoundFile('phonecall', 'mp3');
						this.setState({
							noti: notiData,
							modalCallVisible: true
						})
					}
				}
			}
		  });
		this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
			if(notif.custom_notification){
				notiData = JSON.parse(notif.custom_notification);
				console.log('NOTIFI2',notiData )
	
				if (notiData.type == "call") {
					this.props.navigation.navigate('Ofertas');
					SoundPlayer.playSoundFile('phonecall', 'mp3');
					this.setState({
						noti: notiData,
						modalCallVisible: true
					})
				}
			}
		
				if (AppState.currentState != 'active') {
					if (notif.local_notification) {
						FCM.presentLocalNotification({
							id: "UNIQ_ID_STRING",
							title: notif.aps.alert.title,
							body: notif.aps.alert.body,
							sound: "phonecall.mp3",
							priority: "high",
							click_action: "ACTION",
							icon: "ic_launcher",
							show_in_foreground: false,
						});
	
					}
				}
	
				 if (Platform.OS === 'ios' && notif._notificationType === NotificationType.WillPresent && !notif.local_notification) {
					notif.finish(WillPresentNotificationResult.All)
					return;
				} 
	
				if (Platform.OS === 'ios') {
					switch (notif._notificationType) {
						case NotificationType.Remote:
							notif.finish(RemoteNotificationResult.NewData)
							break;
						case NotificationType.NotificationResponse:
							notif.finish();
							break;
						case NotificationType.WillPresent:
							notif.finish(WillPresentNotificationResult.None)
							break;
					}
				}
				FCM.cancelAllLocalNotifications();
		})
	}
```

En esta función hay varios aspectos a tener en cuenta. El usuario cada vez que se loguea se subscribe a un topic de FCM de **firebase** el cual se nombra de la siguiente forma: **'notifications_' + userID**. Cada vez que se reciba un mensaje en ese canal, el usuario recibirá la notifiación en su dispositivo ya esté en foreground o background. La notificación se filtrará para diferenciar entre una notifiación de mensajes y una de llamada. En el caso de la llamada si la app está en background o cerrada, una vez se pulse sobre la notificación se iniciará la app y aparecerá el componente de la videollamada. Si se está en un estado de foreground directamente aparecerá el modal de la videollamada para aceptar o denegar la misma.



-  **Curriculum**: Segundo Tab dentro del TabNavigator, encargada de mostrar información relativa al usuario y la edición de la misma. 

-  **Candidaturas**: En esta clase se mostrarán cada unas de las inscripciones a las ofertas realizadas, mostrando su estado, información de la misma y notificación de mensajes nuevos en caso de que el chat esté disponible. Actualmente está la opción de borrar candidaturas haciendo un SwipeLeft apareciendo el botón que da acceso para la eliminación de ésta.

-  **Noticias**: Clase encargada de mostrar las noticias disponibles.

-  **Configuración**: Último tab de la app donde se muestra un breve formulario para la modificación de contraseña.

Estas son las clases que ofrecen las vistas disponibles en los tabs de la app. A continuación se detallará otras clases que ofrecerán componentes extra que se utilizarán a lo largo del ciclo de vida de la app.

-  **Chat**: El archivo index.js dentro de este subconjunto de archivos dentro de la carpeta Chat es el que contiene la información primordial de la clase. En este componente se hará uso de la librería de **Gifted-chat** para la composición del chat. **Firebase** actuará como base de datos para los mensajes almacenados entre los usuarios. También se hará uso de la librería **react-native-document-picker** para la carga de archivos desde el dispositivo.

-  **Cards**: Componente para la interfaz de las tarjetas de las ofertas.

-  **Descripción**: Componente que se cargará en los modales indicados para mostrar la información relativa a cada una de las ofertas.

-  **Video**: Clase principal de la videollamada en webRTC. Una vez se recibe la notificación de videollamada y ésta es aceptada, se da acceso a esta clase. Cargado el componente, en la función **componentDidMount** crearemos una nueva conexión encargada de todo el proceso de la llamada:

```sh
pc = new RTCPeerConnection(servers);
```
Esta conexión se encargará de toda la gestión de la llamada. Llamaremos a la función **getLocalStream** que nos proporcionará nuestro stream de video y audio para posteriormente añadirlo a la conexión **pc** creada. Se utiliza también la librería **'react-native-incall-manager'** para el control de audio durante la llamada, permitiendo controlar el flujo de audio y especificar el canal de salida y así enviar el audio por los altavoces correspondientes.

Una vez se va a finalizar la llamada, se activa la función nativa **componentWillUnmount** que permitirá cerrar correctamente el stream de la llamada.

Para el buen funcionamiento de la videollamada se ha realizado una breve integración con firebase para la creación de canales de llamada que se crearán y eliminarán automáticamente para el acceso de cada uno de los streamers. El backend disponible de AndJobNow en este caso es el encargado de realizar todas esas tareas. En este caso únicamente se tendrá que hacer uso del canal proporcionado a través de la información extra de la notificación de llamada.

##IMPORTANTE

###Ejecución 
Una vez bajado el repositorio es necesario disponer del instalador de paquetes [NPM](https://www.npmjs.com) o en su defecto [Yarn](https://yarnpkg.com/lang/en/) para la instalación de los módulos necesarios en la App. Los pasos necesarios para ejecutar la aplicación en modo desarrollo son los siguientes:

1. Instalación de paquetes: **npm install** 
2. Ejecución en simulador iOS: **npm run ios**  (será necesario un dispositivo con sistema operativo MacOS para la ejecución del simulador y un simulador disponibe).

El simulador ejecutará la aplicación y será posible debuggearla a través del navegador chrome y su entorno para desarrolladores o como recomendación hacer uso de [react-native-debugger](https://github.com/jhen0409/react-native-debugger).

###Compilación

Para su correcta compilación y subida de build a itunnes Connect se utilizarán los mismos pasos que con cualquier otra app desarrollada en iOS con **Xcode**. Se deberá cargar el proyecto creado con la instalación de Pods y dentro de Xcode únicamente modificar las versiones de la applicación en el caso de ser necesario.

Si se tienen problemas de compilación relativos al **FBSDK** hay que asegurar de que se dispone del SDK de Facebook correspondiente en la carpeta Documentos del Mac desde donde se esté realizando la compilación. Asegurarse que no está guardado igualmente esta carpeta en iCloud. 

Tras el uso de la librería WebRTC es aconsejable seguir los pasos siguientes para una correcta compilación y subida a la Store en el caso de que se encuentren errores.[react-native-WebRTC Install iOS](https://github.com/oney/react-native-webrtc/blob/master/Documentation/iOSInstallation.md)

Seguir los pasos del **Appendix B - Apple Store Submission**
