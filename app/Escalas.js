import { Dimensions, PixelRatio } from 'react-native';
import { alerta } from './Alerta.js';

// const { ancho, alto } = Dimensions.get('window');

const pR = PixelRatio.get();

const ancho = (Dimensions.get('window').width) * pR;
const alto = (Dimensions.get('window').height) * pR;

const escala = pR / 2.625;

const anchoBase = 1080 * escala;
const altoBase = 1794 * escala;

const escalaW = ancho / anchoBase 
const escalaH = alto / altoBase
//Escala ancho (width)
const eW = tamanio => escalaW * tamanio;
const eH = tamanio => escalaH * tamanio;

// console.log("PixelRatio.get() : " + PixelRatio.get());
// console.log("Dimensions.get('window')");
// console.log(Dimensions.get('window'));
// console.log("ancho : " + ancho);
// console.log("alto : " + alto);
// console.log("escalaW : " + escalaW);
// console.log("escalaH : " + escalaH);

//Escala Modificable
const eM = (tamanio, prop = 0.5) => tamanio + ( eW(tamanio) - tamanio ) * prop;
// console.log("eW(1) : " + eW(1));
// console.log("eH(1) : " + eH(1));
// console.log("eM(1) : " + eM(1));

export {eW, eH, eM};