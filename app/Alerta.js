import { Alert } from 'react-native';

const alerta = (texto) => Alert.alert(
  'Log',
  texto,
  [
    {text: 'Ok'},
  ],
)

export {alerta};