import React from 'react';
import { Text, View, Image, StyleSheet, FlatList, ToastAndroid, TouchableOpacity, Modal, Alert, AppState, AsyncStorage } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';

import Chat from './Chat'
import { Descripcion } from './Descripcion'

import { NavigationActions } from 'react-navigation';
import RNRestart from 'react-native-restart';
import FCM from 'react-native-fcm';
import { LoginManager } from 'react-native-fbsdk';
import Toast, { DURATION } from 'react-native-easy-toast'
import firebase from 'react-native-firebase';
import Swipeout from 'react-native-swipeout';

import RNFileSelector from 'react-native-file-selector';

var dbRef = firebase.database().ref();
var refChats = dbRef.child("chats");
var refUsers = dbRef.child("users");

var gettingData = false



function getActividad(id) {


	let actividades = [];

	actividades[1] = strings.activity1;
	actividades[2] = strings.activity2;
	actividades[3] = strings.activity3;
	actividades[4] = strings.activity4;
	actividades[5] = strings.activity5;
	actividades[6] = strings.activity6;
	actividades[7] = strings.activity7;
	actividades[8] = strings.activity8;
	actividades[9] = strings.activity9;
	actividades[10] = strings.activity10;
	actividades[11] = strings.activity11;
	actividades[12] = strings.activity12;
	actividades[13] = strings.activity13;
	actividades[14] = strings.activity14;
	actividades[15] = strings.activity15;
	actividades[16] = strings.activity16;
	actividades[17] = strings.activity17;
	actividades[18] = strings.activity18;
	actividades[19] = strings.activity19;
	actividades[20] = strings.activity20;
	actividades[21] = strings.activity21;
	actividades[22] = strings.activity22;
	actividades[23] = strings.activity23;

	return actividades[id];


}

function getDuration(id) {


	let duration = [];

	duration[1] = strings.duration1;
	duration[2] = strings.duration2;
	duration[3] = strings.duration3;


	return duration[id];


}


function getStatus(id) {

	let status = [];
	let color = [];

	status[0] = strings.status0;
	status[1] = strings.status1;
	status[2] = strings.status2;
	status[3] = strings.status3;

	color[0] = "#f39c10";
	color[1] = "#FF2B19";
	color[2] = "#82bc29";
	color[3] = "#1a94af";

	return [status[id], color[id]];

}


export class Candidaturas extends React.Component {


	constructor(props) {
		super(props)

		this.state = {
			modalVisible: false,
			chatVisible: false,
			appState: AppState.currentState,
			show: false,
			offers: [],
			page: 1

		}

	}




	openModal() {
		this.setState({ modalVisible: true });
	}

	closeModal() {
		this.setState({ modalVisible: false });
	}


	openChat(offer) {

		let idOferta = this.state.idOfertaDesc;
		let idUser = offer.idUser;
		let status = offer.statusReal;

		//console.log(" idOferta ", idOferta)
		//console.log(" status ", status)
		//console.log(" idUser ", idUser)
		// console.log(" arrStatus ", globales.arrStatusCand)
		//console.log(" arrOfferUserIds ", globales.arrOfferUserIds)

		// Object.keys(propsOferta).forEach(function(clave) {
		// 	console.log("props offer ", clave,propsOferta[clave])
		// })
		console.log('Imagen avatar', this.state.imgEmpresaDesc)
		if (status == 2 || status == 3) {
			globales.otherAvatar = this.state.imgEmpresaDesc.uri;
			globales.otherUID = idUser;
			//this.props.navigation.push('Chat')
			this.setState({ chatVisible: true });
		} else {
			this.closeModal()

			this.refs.toastKoD.show(strings.needToChat, 2000);

		}
	}

	closeChat() {
		this.setState({ chatVisible: false });
	}

	obtenerCandidaturas(page) {
		gettingData = true;
		setTimeout(() => { gettingData = false }, 3000)
		let dataBody = {
			"user": {
				"id": globales.UID
			},
			page: page
		}

		let data = JSON.stringify(dataBody);
		//console.log("data obtenerCandidaturas",typeof(data),data);

		return fetch(globales.apiURL + '/api/offer/applications-offers-new',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: data

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				console.log("responseJson obtenerCandidaturas", responseJson);
				if (responseJson.offers) {
					let candidacies = [];
					if (page === 1) {
						candidacies = responseJson.offers;
					} else {
						candidacies = [...this.state.candidacies, ...responseJson.offers];
					}

					this.setState({
						candidacies: candidacies
					})
					this.tratarCandidaturas(candidacies);
				}

			})
			.catch((error) => {
				console.error(error);
			});

	}

	tratarCandidaturas(offersJson) {


		let newOffers = [];
		let arrStatus = {};

		console.log("cargarU", offersJson);

		offersJson.forEach((offer, index) => {

			console.log('OFerta real', offer)
			otherUserID = offer.company.user_id;
			let readed = false;;

			//this.firebase.list(`/chats/${key}`, ref => ref.limitToLast(1));

			let status, color;
			[status, color] = getStatus(offer.status_application);
			console.log('STATUS', status, color)
			let imgEmpresa;

			if (offer.company.logo) {
				imgEmpresa = { uri: globales.imageUrl + offer.company.logo };
			} else {
				imgEmpresa = require('../img/noimg.png');
			}

			let offerUpdated = {
				key: offer.id,
				img: imgEmpresa,
				empresa: offer.company.name_company,
				puesto: offer.profession,
				salario: offer.min_salary,
				jornada: offer.workshift,
				duration: getDuration(offer.contract_duration),
				status: status,
				statusReal: offer.status_application,
				statusColor: color,
				company: offer.company,
				web: offer.company.web ? offer.company.web : '',
				idOferta: offer.id,
				idEmpresa: offer.company_id,
				actividad: getActividad(offer.company.sector),
				ubication: offer.address,
				tiempoExp: offer.exp_time,
				estudiosReq: offer.studies,
				descripcion: offer.description,
				idUser: offer.company.user_id,
				titulo: offer.title

			};
			const _this = this;
			refUsers.child(globales.UID + "/chats/" + otherUserID).once("value")
				.then((dataSS) => {
					var valor = dataSS.val();
					//console.log("valor", valor);
					let refChatAct;
					if (valor) {
						firebase.database().ref().child("chats").child(valor).limitToLast(1).on('child_added', function (childSnapshot) {

							var snap = childSnapshot.val();
							console.log("CHILD", snap);
							if (snap.user._id == globales.UID) {
								readed = true;
							} else {
								readed = snap.read
							}
							offerUpdated.readed = readed;
							let add = true;
							let offerIndex = null;
							newOffers.forEach((offerIn, indexAux) => {
								if (offerIn.key === offerUpdated.key) {
									add = false;
									offerIndex = indexAux;
								}
							});
							if (add) {
								newOffers.push(offerUpdated);
								_this.setState({ offers: newOffers });
							} else {
								console.log('Reactualiza candidatura', readed)
								newOffers[offerIndex].readed = readed;
								_this.setState({ offers: newOffers });
							}

							//arrStatus[keyStr] = offer.status_application;
							//globales.arrStatusCand = arrStatus;
						});
						//this.cargarMensajes();

					} else {
						readed = true
						offerUpdated.readed = readed;
						newOffers.push(offerUpdated);
						_this.setState({ offers: newOffers });
						/* arrStatus[keyStr] = objOffer["status_application"];

						globales.arrStatusCand = arrStatus; */
					}

					//console.log("OFERRRRRRRRTAS", offers);

				}).catch(function (e) {
					console.log('Error firebase', e); // "oh, no!"
				});



		});




		/* const setParamsAction = NavigationActions.setParams({
			params: { datosCandidaturas: offers, statusCandidaturas: arrStatus },
			key: 'Ofertas',
		});
		this.props.navigation.dispatch(setParamsAction);

		this.props.navigation.setParams({
			datos: offers,
		})
		
		this.setState({ datos: this.props.navigation.state.params.datos, offers: offers }); */


	}


	clickCorazon(item, index) {
		let { datos } = this.state;

		// let target = datos[index];

		// console.log(target);
		// console.log(item);
		// // Flip the 'liked' property of the target
		// console.log(target.click);
		// target.click = !target.click;
		// console.log(target.click);

		// Then update targetPost in 'datos'
		// You probably don't need the following line.
		//datos[index].click = !datos[index].click;


		// Then reset the 'state.datos' property



		///////////// Prueba Add Candidatura ////////



		//    let dats = this.props.navigation.state.params.datos
		// // console.log("navigation props state datos",dats);

		// Object.keys(dats).forEach(function(clave1) {
		// 	Object.keys(dats[clave1]).forEach(function(clave) {
		// 		console.log("campo offer navig",clave1, clave,dats[clave1][clave])
		// 	})
		// })


		// const setParamsAction = NavigationActions.setParams({
		// 		params: { datosCandidaturas: dats},
		// 		key: 'Ofertas',
		// 	});
		// this.props.navigation.dispatch(setParamsAction);

		// this.setState({datos:dats})


	}

	openDescription(item) {



		this.setState({

			idOfertaDesc: item.idOferta,
			idEmpresaDesc: item.idEmpresa,
			imgEmpresaDesc: item.img,
			nombreEmpresaDesc: item.empresa,
			actividadDesc: item.actividad,
			ubicacionDesc: item.ubication,
			cargoDesc: item.puesto,
			tiempoExpDesc: item.tiempoExp,
			estudiosReqDesc: item.estudiosReq,
			descripcionDesc: item.descripcion,
			idUserDesc: item.idUser,
			oferta: item,
			titulo: item.titulo

		});
		globales.otherUID = item.idUser;


		this.setState({
			modalVisible: true
		})




	}

	logoutAccept() {
		FCM.unsubscribeFromTopic('notifications_' + globales.UID);
		globales.UID = null;
		globales.token = '';
		globales.email = '';
		globales.apellidos = '';
		globales.nombre = '';
		//AsyncStorage.removeItem('tourjob');
		AsyncStorage.removeItem('jobToken');
		AsyncStorage.removeItem('jobUID');
		AsyncStorage.removeItem('jobNombre');
		AsyncStorage.removeItem('jobApellidos');
		AsyncStorage.removeItem('jobEmail');
		LoginManager.logOut();
		RNRestart.Restart()
	}

	logout() {

		// Works on both iOS and Android
		Alert.alert(
			strings.closeSession,
			strings.wantCloseSession,
			[
				{ text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: strings.closeSession, onPress: () => this.logoutAccept() },
			],
			{ cancelable: false }
		)

	}


	componentDidMount() {
		AppState.addEventListener('change', this._handleAppStateChange);
		this.obtenerCandidaturas(1);
		// console.log("navigation props state",this.props.navigation.state.params.datos)
		this.props.navigation.addListener(
			'willFocus',
			payload => {
				console.log("Entra en candidaturas", payload)
				this.obtenerCandidaturas(1);
			}
		);
	}



	componentWillUnmount() {
		AppState.removeEventListener('change', this._handleAppStateChange);
	}

	_handleAppStateChange = (nextAppState) => {
		if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
			console.log('App has come to the foreground!')
			this.obtenerCandidaturas(1);
		}
		this.setState({ appState: nextAppState });
	}




	listItem = ({ item, index }) => (
		swipeoutBtns = [
			{
				text: strings.delete,
				backgroundColor: '#E30202',
				onPress: () => Alert.alert(
					strings.attention,
					strings.wantDeleteCand,
					[

						{ text: strings.no, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
						{ text: strings.yes, onPress: () => this.eliminarCandidatura(item.idOferta) },
					],
					{ cancelable: false }
				),
			}
		],


		<TouchableOpacity key={index} style={styles.filaItem} onPress={() => this.openDescription(item)}>

			{
				// var status,color;
				// [status,color] = this.getStatus(item.status)
			}

			<View style={styles.colLogo}>
			{item.empresa === 'Oculto' ? 
					<Image blurRadius={30} source={item.img} style={styles.logoEmpresa} />
				:  
				<Image source={item.img} style={styles.logoEmpresa} />
				}
				

			</View>

			<View style={styles.infoItem}>

				<View style={styles.filaNombre}>
					<Text style={styles.nombre}>{item.empresa === 'Oculto' ? strings.hiddenCompany : item.empresa}</Text>
					{item.readed ?
						null
						: <View style={{ width: 10, height: 10, backgroundColor: '#82bc29', borderRadius: 5 }} />
					}

					{/*<Icon 
						name={item.click ? 'heart' : 'heart-o'}
						size={eH(24)}
						color='#f39c10'
						type='font-awesome'
						onPress={() => {
							this.clickCorazon()
							
						}}
					/>*/}


				</View>

				<View style={styles.filaInfoUno}>
					<Text style={[styles.fecha, { backgroundColor: item.statusColor }]}>{item.status}</Text>
					<Text style={styles.cargo}>{item.puesto}</Text>

				</View>

				<View style={styles.filaInfoDos}>
					<Text style={styles.datoInfoDos}>{item.jornada}</Text>
					<Text style={styles.datoInfoDos}> | </Text>
					<Text style={styles.datoInfoDos}>{item.duration}</Text>
					<Text style={styles.datoInfoDos}> | </Text>
					<Text style={styles.datoInfoDos}>{item.salario} €</Text>
				</View>


			</View>

			<TouchableOpacity style={styles.deleteButton} onPress={() => Alert.alert(
				strings.attention,
				strings.wantDeleteCand,
				[

					{ text: strings.no, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
					{ text: strings.yes, onPress: () => this.eliminarCandidatura(item.idOferta) },
				],
				{ cancelable: false }
			)}>
				<Icon name='ios-trash-outline' type='ionicon' color='#EC4242' size={eH(35)} />
			</TouchableOpacity>
		</TouchableOpacity>


	);

	nextPage() {
		if (!gettingData) {
			let page = this.state.page + 1;
			this.setState({
				page: page
			})
			this.obtenerCandidaturas(page)
		}

	}

	eliminarCandidatura(candID) {

		let dataBody = {
			"user": {
				"id": globales.UID
			},
			offer: {
				id: candID
			}
		}

		let data = JSON.stringify(dataBody);
		// console.log("data obtenerCandidaturas",typeof(data),data);

		return fetch(globales.apiURL + '/api/application/delete-application',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: data

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				// console.log("responseJson obtenerCandidaturas",responseJson);
				this.setState({offers: []});
				this.obtenerCandidaturas(1);
			})
			.catch((error) => {
				console.error(error);
			});

	}

	render() {

		console.log('Ofertas Actualizada', this.state.offers)

		return (
			this.state.chatVisible ?
				<Chat closeChat={() => this.closeChat()} />
				:

				<View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
					<View style={styles.cabecera}>

						<View style={{ flex: 0.2, }}>
							<TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }}
								onPress={() => { this.logout() }}>
								<Icon name='power'
									type='feather'
									color='#FFFFFF'
									size={eH(28)}
								/>
							</TouchableOpacity>
						</View>
						<Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(20) }}>{strings.candidacies}</Text>

						<View style={{ flex: 0.2 }}>

						</View>

					</View>
					<FlatList
						extraData={this.state}
						data={this.state.offers}
						renderItem={this.listItem}
						onEndReached={() => this.nextPage()}
						onEndReachedThreshold={0.1}
						keyExtractor={(item, index) => index.toString()}
					/>

					<Toast
						ref="toastKo"
						opacity={0.9}
						fadeInDuration={500}
						fadeOutDuration={800}
						style={styles.toast}
						textStyle={styles.toastText}
						positionValue={eH(200)}
					/>

					<Toast
						ref="toastOk"
						opacity={0.9}
						fadeInDuration={500}
						fadeOutDuration={800}
						style={[styles.toast, { backgroundColor: "#67bf09" }]}
						textStyle={styles.toastText}
						positionValue={eH(200)}
					/>
					<Toast
						ref="toastKoD"
						opacity={0.9}
						fadeInDuration={500}
						fadeOutDuration={800}
						style={styles.toast}
						textStyle={styles.toastText}
						positionValue={eH(200)}
					/>
					<Modal
						visible={this.state.modalVisible}
						animationType={'slide'}
						onRequestClose={() => this.closeModal()}

					>
						<Descripcion
							idOferta={this.state.idOfertaDesc}
							oferta={this.state.oferta}
							idEmpresa={this.state.idEmpresaDesc}
							imgEmpresa={this.state.imgEmpresaDesc}
							nombreEmpresa={this.state.nombreEmpresaDesc}
							actividad={this.state.actividadDesc}
							ubicacion={this.state.ubicacionDesc}
							cargo={this.state.cargoDesc}
							tiempoExp={this.state.tiempoExpDesc}
							estudiosReq={this.state.estudiosReqDesc}
							descripcion={this.state.descripcionDesc}
							candScene={true}
							closeModal={() => this.closeModal()}
							openChat={() => this.openChat(this.state.oferta)}
							closeChat={() => this.closeChat()}
							titulo={this.state.titulo}
						/>




					</Modal>

				</View>

		);
	}
}





const styles = StyleSheet.create({

	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: eH(55),
		paddingHorizontal: 8,
		width: '100%',
	},
	textCabecera: {
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(20),
	},
	filaItem: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: '#FFFFFF',
		paddingHorizontal: 6,
		borderBottomWidth: 0.5,
		borderBottomColor: '#adadad',
	},
	colLogo: {
		backgroundColor: '#f4f4f4',
		marginHorizontal: 6,
		height: eH(70),
		alignSelf: 'center',
		justifyContent: 'center',
	},
	deleteButton: {
		marginHorizontal: 6,
		height: eH(70),
		alignSelf: 'center',
		justifyContent: 'center',
	},
	logoEmpresa: {
		width: eH(70),
		height: eH(70),
		// alignSelf: 'center',
		resizeMode: 'contain',
	},
	infoItem: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-between',
		height: eH(90),
		padding: 6,
		// backgroundColor:'#efefef',
	},

	filaNombre: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',

	},
	nombre: {
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		// backgroundColor:'#d2d2d2',
		color: '#6f6f6f',
	},

	filaInfoUno: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'flex-start',
		// backgroundColor:'#dedede',
	},
	fecha: {
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(10),
		paddingHorizontal: eH(6),
		paddingVertical: eH(2),

		color: '#FFFFFF',
		borderRadius: 4,
	},
	cargo: {
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(14),
		color: '#82bc29',

	},


	filaInfoDos: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'flex-end',
		// backgroundColor:'#d2d2d2',
	},
	datoInfoDos: {
		// backgroundColor:'#f2f2f2',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(12),
		color: '#adadad',
	},

	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	},

});
