import React from 'react';
import { View, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity, ToastAndroid, Alert, Dimensions, AsyncStorage } from 'react-native';
import { Icon, Card, ListItem } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';

import RNRestart from 'react-native-restart';
import { LoginManager } from 'react-native-fbsdk';
import FCM from 'react-native-fcm';
import Toast, { DURATION } from 'react-native-easy-toast'

import { Text, Button, Left, Body } from 'native-base';
import HTML from 'react-native-render-html';




var gettingData = false

export class Noticias extends React.Component {




  constructor(props) {
    super(props)

    this.state = {
      blog: [],
      page: 1
    }
  }

  componentDidMount() {
    this.getNews(1);
  }


  getNews(page) {

    gettingData = true;
    setTimeout(() => { gettingData = false }, 3000)

    let dataBody = {
      blog: {
        lang: globales.language === 'es' ? 'es' : 'cat',
      },
      page: page
    }

    let data = JSON.stringify(dataBody);



    return fetch(globales.apiURL + '/api/blog/list-posts-new',
      {
        method: "POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': globales.token
        },
        body: data

      })
      .then((response) => {

        return response.json();
      })
      .then((responseJson) => {

        console.log('Noticias', responseJson)
        if (responseJson["status"] == "success") {
          this.setState({
            blog: [...this.state.blog, ...responseJson.blog]
          })



          //this.refs.toastOk.show(responseJson["message"],2000);
        } else {
          //this.refs.toastKo.show(responseJson["message"],2000);
        }


      })
      .catch((error) => {
        console.error(error);
      });

  }

  logoutAccept() {
    FCM.unsubscribeFromTopic('notifications_' + globales.UID);
    globales.UID = null;
    globales.token = '';
    globales.email = '';
    globales.apellidos = '';
    globales.nombre = '';
    //AsyncStorage.removeItem('tourjob');
    AsyncStorage.removeItem('jobToken');
    AsyncStorage.removeItem('jobUID');
    AsyncStorage.removeItem('jobNombre');
    AsyncStorage.removeItem('jobApellidos');
    AsyncStorage.removeItem('jobEmail');
    LoginManager.logOut();
    RNRestart.Restart()
  }

  logout() {


    // Works on both iOS and Android
    Alert.alert(
      'Tancar sessió',
      'Vols tancar la sessió?',
      [
        { text: 'Cancel·lar', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Tancar Sessió', onPress: () => this.logoutAccept() },
      ],
      { cancelable: false }
    )


  }

  listItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => this.props.navigation.push('Noticia', { from: 'Noticias', news: item })}>
      <Card image={item.image ? { uri: globales.imageUrl + item.image } : null}>
        <Text style={styles.textTitle}>{item.title}</Text>
        <Button style={styles.buttonEnter} onPress={() => this.props.navigation.push('Noticia', { from: 'Noticias', news: item })}>
          <Text style={styles.textBotonGuardar}>{strings.read}</Text>
        </Button>
      </Card>
    </TouchableOpacity>
  );

  nextPage() {
    if (!gettingData) {
      let page = this.state.page + 1;
      this.setState({
        page: page
      })
      this.getNews(page)
    }

  }


  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.cabecera}>
          <View style={{ flex: 0.2, }}>
            <TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }}
              onPress={() => { this.logout() }}>
              <Icon name='power'
                type='feather'
                color='#FFFFFF'
                size={eH(28)}
              />
            </TouchableOpacity>
          </View>
          <Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(20) }}>Blog</Text>
          <View style={{ flex: 0.2 }} />
        </View>
        <FlatList
          style={{ flex: 1, backgroundColor: '#ecedeb' }}
          extraData={this.state}
          data={this.state.blog}
          renderItem={this.listItem}
          onEndReached={() => this.nextPage()}
          onEndReachedThreshold={0.5}
        />
        <Toast
          ref="toastKo"
          opacity={0.9}
          fadeInDuration={500}
          fadeOutDuration={800}
          style={styles.toast}
          textStyle={styles.toastText}
          positionValue={eH(200)}
        />
        <Toast
          ref="toastOk"
          opacity={0.9}
          fadeInDuration={500}
          fadeOutDuration={800}
          style={[styles.toast, { backgroundColor: "#67bf09" }]}
          textStyle={styles.toastText}
          positionValue={eH(200)}
        />
      </View>
    );
  }
}





const styles = StyleSheet.create({

  cabecera: {
    backgroundColor: '#f39c10',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: eH(55),
    paddingHorizontal: 8,
    width: '100%',
  },
  textCabecera: {
    flex: 0.6,
    textAlign: 'center',
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: eH(20),
  },
  cancelarCabecera: {
    textAlign: 'left',
    paddingLeft: 10,
    flex: 0.2,
    color: 'white',
    fontSize: eH(20),

  },
  guardarCabecera: {
    textAlign: 'right',
    paddingRight: 10,
    flex: 0.2,
    color: 'white',
    fontSize: eH(20),

  },
  campos: {
    marginTop: eH(50),
  },
  filaInfo: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: eH(10),
    paddingVertical: eH(6),
    borderTopWidth: 0.5,
    borderTopColor: '#adadad',
  },
  colIzda: {
    color: '#adadad',
    fontFamily: 'Montserrat-Regular',
    fontSize: eH(16)
  },
  colDcha: {
    color: '#6f6f6f',
    fontFamily: 'Montserrat-Regular',
    fontSize: eH(16),
  },

  botonGuardar: {
    // textAlign:'center',
    backgroundColor: '#f39c10',
    borderRadius: 4,
    paddingHorizontal: eH(20),
    marginTop: eH(10),
    height: eH(54),
  },
  textBotonGuardar: {
    fontFamily: 'Montserrat-Bold',
    fontSize: eH(17),
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: '600',
  },
  input: {
    // flex:1,
    // borderColor: 'white',
    // height:eH(46),
    color: '#6f6f6f',
    fontFamily: 'Montserrat-Regular',
    fontSize: eH(16),
    // fontWeight: '300',
    // marginLeft: 0,
    // paddingLeft: eH(20),
    // backgroundColor: 'red',
  },
  containerInput: {
    // paddingLeft: eH(20),
    // paddingRight: eH(20),
    // borderRadius: eH(40),
    // backgroundColor:'#689c15',
    // width:'100%',
    // height:eH(46),
    borderBottomWidth: 0,
  },
  toast: {
    width: 300,
    borderRadius: 20,
    backgroundColor: '#FF2B19'
  },
  toastText: {
    textAlign: 'center',
    color: 'white',
  },
  buttonEnter: {
    width: '100%',
    backgroundColor: '#689c15',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTitle: {
    fontFamily: 'Montserrat-Bold',
    fontSize: eH(15),
    color: '#43484D',
    textAlign: 'center',
    fontWeight: '600',
    //paddingHorizontal: 5,
    marginBottom: 10
  }
});

