'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  TextInput,
  ListView,
  Platform,
  TouchableOpacity
} from 'react-native';
import { Button, Icon } from 'native-base';

import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStreamTrack,
  getUserMedia,
} from 'react-native-webrtc';
import firebase from 'react-native-firebase';
import { globales, strings } from '../globales.js';
//var database = firebase.database().ref();
import InCallManager from 'react-native-incall-manager'

var yourId = Math.floor(Math.random()*1000000000);
var servers = {'iceServers': [{'url': 'stun:146.185.149.129'}]};
var pc = new RTCPeerConnection(servers);
let localStream=null;
let container = this;
var database = null;


var dbRef = firebase.database().ref();
var refChats = dbRef.child("chats");
var refUsers = dbRef.child("users");
// var refChatAct = refChats.child("-LEQyRAelUU-Lqq8eN41");
var refChatAct;

function sendMessage(senderId, data) {
 var msg = database.push({ sender: senderId, message: data });
 msg.remove();
}

function readMessage(data) {
 var msg = JSON.parse(data.val().message);
 var sender = data.val().sender;
 if (sender != yourId) {
 if (msg.ice != undefined)
 pc.addIceCandidate(new RTCIceCandidate(msg.ice));
 else if (msg.sdp.type == "offer")
 pc.setRemoteDescription(new RTCSessionDescription(msg.sdp))
 .then(() => pc.createAnswer())
 .then(answer => pc.setLocalDescription(answer))
 .then(() => sendMessage(yourId, JSON.stringify({'sdp': pc.localDescription})));
 else if (msg.sdp.type == "answer")
 pc.setRemoteDescription(new RTCSessionDescription(msg.sdp));
 }
};




function showFriendsFace() {
 pc.createOffer()
 .then(offer => pc.setLocalDescription(offer) )
 .then(() => sendMessage(yourId, JSON.stringify({'sdp': pc.localDescription})) );
}

function logError(error) {
  console.log("logError", error);
}


export class Video extends Component {


  constructor(props) {
    super(props)
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => true });
    this.state = {
      info: 'Initializing',
      status: 'init',
      myID: null,
      roomID: '',
      isFront: true,
      selfViewSrc: null,
      remoteViewSrc: null,
      remoteList: {},
      textRoomConnected: false,
      textRoomData: [],
      textRoomValue: '',
      database: firebase.database.Reference,
      noti: this.props.navigation.state.params.noti
    }
    
    database = firebase.database().ref(this.props.navigation.state.params.noti.room);
    database.on('child_added', readMessage);
    this.updateFirebase(this.props.navigation.state.params.noti.company_id, this.props.navigation.state.params.noti._id, this.props.navigation.state.params.noti.refChat);
  }

  componentDidMount() {
    pc = new RTCPeerConnection(servers);
    pc.onicecandidate = (event => event.candidate?sendMessage(yourId, JSON.stringify({'ice': event.candidate})):console.log("Sent All Ice") );

    container = this;
    pc.onaddstream = (event => 
      this.setState({
        remoteViewSrc: event.stream.toURL()
      })
    )
      
      
    this.getLocalStream(true, (stream) => {
      this.setState({
        selfViewSrc: stream.toURL()
      })
    })
    InCallManager.start({media: 'audio'});
    InCallManager.setForceSpeakerphoneOn( true );
  }

  componentWillUnmount() {
    InCallManager.stop();
    this.closeFirebase(this.props.navigation.state.params.noti.company_id, this.props.navigation.state.params.noti._id, this.props.navigation.state.params.noti.refChat);
    pc.close();
    
  }

  closeFirebase(otherUID, idMessage, refChat) {
    //.orderByChild("_id").equalTo(idMessage).once("value")


    refUsers.child(globales.UID + "/chats/" + otherUID).once("value")
      .then((dataSS) => {
        //console.log("Message Video dataSS", dataSS);
        var snap = dataSS.val();
        firebase.database().ref().child("chats").child(snap).orderByChild("_id")
          .equalTo(idMessage).once('value', function (snapshot) {
            snapshot.forEach(function (child) {
              child.ref.update({ statusCall: 2 });
              child.ref.update({ callApplicant: false });
            })
          })
      }
      )
  }

  updateFirebase(otherUID, idMessage, refChat){ 
    //.orderByChild("_id").equalTo(idMessage).once("value")


    refUsers.child(globales.UID + "/chats/" + otherUID).once("value")
			.then((dataSS) => {
        console.log("Message Video dataSS", dataSS);
        var snap = dataSS.val();
        firebase.database().ref().child("chats").child(snap).orderByChild("_id")
        .equalTo(idMessage).once('value', function (snapshot) {
          snapshot.forEach(function(child) {
            child.ref.update({statusCall: 1});
          })
        })
        }
			)
  }

 getLocalStream(isFront, callback) {

    let videoSourceId;
  
    // on android, you don't have to specify sourceId manually, just use facingMode
    // uncomment it if you want to specify
    if (Platform.OS === 'ios') {
      MediaStreamTrack.getSources(sourceInfos => {
      //  console.log("sourceInfos: ", sourceInfos);
  
        for (const i = 0; i < sourceInfos.length; i++) {
          const sourceInfo = sourceInfos[i];
          if(sourceInfo.kind == "video" && sourceInfo.facing == (isFront ? "front" : "back")) {
            videoSourceId = sourceInfo.id;
          }
        }
      });
    }
    getUserMedia({
      audio: true,
      video: {
        mandatory: {
          minWidth: 640, // Provide your own width, height and frame rate here
          minHeight: 360,
          minFrameRate: 30,
        },
        facingMode: (isFront ? "user" : "environment"),
        optional: (videoSourceId ? [{sourceId: videoSourceId}] : []),
      },
    }, function (stream) {
      
      
      //console.log('getUserMedia success', stream);
      pc.addStream(stream);
      callback(stream);
    }, logError);
  }
  

  _switchVideoType() {
    const isFront = !this.state.isFront;
    this.setState({ isFront });
    getLocalStream(isFront, function (stream) {
      if (localStream) {
        for (const id in pcPeers) {
          const pc = pcPeers[id];
          pc && pc.removeStream(localStream);
        }
        localStream.release();
      }
      localStream = stream;
      container.setState({ selfViewSrc: stream.toURL() });

      for (const id in pcPeers) {
        const pc = pcPeers[id];
        pc && pc.addStream(localStream);
      }
    });
  }

  closeCall() {
    console.log('cerrar llamada')
    pc.close();
     localStream=null;
     container = this;
     database = firebase.database().ref('sala');
    
     
     yourId = Math.floor(Math.random()*1000000000);
     InCallManager.stop();
     this.closeFirebase(this.props.navigation.state.params.noti.company_id, this.props.navigation.state.params.noti._id, this.props.navigation.state.params.noti.refChat);
    this.props.navigation.pop();
  }

  render() {
    return (
      <View style={styles.container}>
        <RTCView  streamURL={this.state.selfViewSrc} style={styles.selfView} />
        {this.state.remoteViewSrc ? 
                <RTCView streamURL={this.state.remoteViewSrc} style={styles.remoteView} />

        :
        <Text style={{color: 'white', fontSize: 25, fontWeight: '500', textAlign: 'center', textAlignVertical: 'center', alignSelf: 'center'}}>{strings.connecting}</Text>
        }
          
        <TouchableOpacity style={{ position: 'absolute', alignSelf: 'center', bottom: 10, width:70, height: 70, paddingLeft: 10}} onPress={() => this.closeCall()}>
          <Icon ios='ios-close-circle-outline' android="ios-close-circle-outline" style={{fontSize: 60, color: 'red'}}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  remoteView: {
    width: '100%',
    height: '100%',
  },
  selfView: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    width: '20%',
    height: '20%',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  listViewContainer: {
    height: 150,
  },
});