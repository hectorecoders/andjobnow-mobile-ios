import React from 'react';
import { Text, View, Image, StyleSheet, ScrollView, TouchableOpacity, Modal} from 'react-native';
import { Button,Icon } from 'react-native-elements';
import Collapsible from 'react-native-collapsible';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';

import Toast, {DURATION} from 'react-native-easy-toast';
import { WebView } from "react-native-webview";

export class Descripcion extends React.Component {
	
	constructor (props) {
		super(props)
		this.state = {
			isCollapsed: true,
			showWeb: false,
		};

	}


	collapse() {
		if (this.state.isCollapsed) {
			this.setState({isCollapsed:false});
			setTimeout(() => {
				this.scrollView.scrollToEnd({animated: true})},
				300);

		}else{
			this.setState({isCollapsed:true});
			
		}
	}


	
	applyOffer(idOferta){

		let dataBody = {
			"user":{
				"id":globales.UID
			},
			"offer":{
			    "id":idOferta
			}
		}

	const _this = this;

		return fetch(globales.apiURL+'/api/offer/application-offer',
	  	{
	  		method: "POST",
			headers: {
				Accept: 'application/json',
	    		'Content-Type': 'application/json',
	    		'Authorization': globales.token
			},
			body: JSON.stringify(dataBody)
			
	  	})
	    .then((response) => {
	    	return response.json();
	    })
	    .then((responseJson) => {
	    	if (responseJson["message"].includes("correctamente")) {
				this.refs.toastOk.show(responseJson["message"],2000);

	    	}else{
				this.refs.toastKo.show(responseJson["message"],2000);

			}
			
	    	if (responseJson["status"] == "success" && this.props.type &&  this.props.type!=='rejected'){
					_this.props.reloadOffers();
	    	}else{

	    	}


	    	
	    	

	    })
	    .catch((error) =>{
	      console.error(error);
	    });

	}



	botonInscripcion = () => {

		// console.log("candScene - ",this.props.candScene)
		if (this.props.candScene){

		}else{
			return(
				<View style={{paddingVertical:20}}>
					<Button buttonStyle={styles.botonInscripcion}
							textStyle={styles.textBotonInscripcion}

							onPress={() => this.applyOffer(this.props.idOferta)} 
							text={strings.subscribe}

							clear
						/>
				</View>
			)

		}
	}

	openChatDescription(){
	
		this.props.closeModal();
		setTimeout(() => {
			this.props.openChat();
		}, 400);
		
	}

	render() {

		let idOferta		= this.props.idOferta;
		let imgEmpresa		= this.props.imgEmpresa;
		let web	= this.props.oferta.company? this.props.oferta.company.web : '';
		let nombreEmpresa	= this.props.nombreEmpresa;
		let actividad		= this.props.actividad;
		let ubicacion		= this.props.ubicacion;
		let cargo			= this.props.cargo;
		let tiempoExp		= this.props.tiempoExp;
		let estudiosReq		= this.props.estudiosReq;
		let descripcion		= this.props.descripcion;
		let idEmpresa		= this.props.idEmpresa;
		let titulo = this.props.titulo;

		console.log('Oferta seleccionada', this.props.oferta)
		console.log('Va a mostrar web',web)
		return (
			<View style={{ flex:1}}>
				<View style={styles.cabecera}>
					<TouchableOpacity style={styles.cancelarCabecera} onPress={this.props.closeModal}>
						<View style={styles.izqCabecera}>
							<Icon name='ios-arrow-back'
							  type='ionicon'
							  color='#FFFFFF'
							  size={eH(35)}
							/>
						</View>
					</TouchableOpacity>
					<Text style={styles.textCabecera}>{strings.offerData}</Text>
					{this.props.candScene ?
					<TouchableOpacity style={styles.guardarCabecera} onPress={() => this.openChatDescription()}>
						<Text style={styles.abrirChat}>Chat</Text>
					</TouchableOpacity> 
					:
					<View style={{ flex: 0.2 }}></View>}
				</View>

				<View style={{ flex:1, justifyContent:'space-between', backgroundColor:'#ecedeb' }}>
					<ScrollView ref={scrollView => this.scrollView = scrollView}
						style={styles.scrollView}>

						<View style={styles.intScroll}>
							
							<View>
								<View style={styles.cabeceraCuadro}>
								{nombreEmpresa === 'Oculto' ? 
									<Image blurRadius={30}  source={imgEmpresa} style={styles.logoEmpresa} />
									:
									<Image source={imgEmpresa} style={styles.logoEmpresa} />
									}
								 	<Text style={styles.textCabeceraCuadro}>{strings.companyInfo}</Text>

								</View>
								{nombreEmpresa === 'Oculto' ? 
									<View style={styles.filaInfo}>
										<Text style={[styles.colIzda, {flex:1}]}>{strings.hiddenCompany}</Text>
									</View>
									:
								<View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.name}</Text>
										<Text style={styles.colDcha}>{nombreEmpresa}</Text>
									</View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.activity}</Text>
										<Text style={styles.colDcha}>{actividad}</Text>
									</View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.ubication}</Text>
										<Text style={styles.colDcha}>{ubicacion}</Text>
									</View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>Web</Text>
										
										
										<Text style={styles.colDcha}>{web} </Text>
										{web ? 
											<Icon name='arrow-down'
											type='simple-line-icon'
											color='#6f6f6f'
											size={32}
											onPress={() => this.setState({showWeb: !this.state.showWeb})}
										/>
											: null}
										
									
									
									</View>
								
									
									
								</View>
								}
							</View>

							<View>
								<View style={styles.cabeceraCuadro}>
									<Text style={styles.textCabeceraCuadro}>{strings.offerDataCap}</Text>

								</View>
								<View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.title}</Text>
										<Text style={styles.colDcha}>{titulo}</Text>
									</View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.charge}</Text>
										<Text style={styles.colDcha}>{cargo}</Text>
									</View>
									{/* <View style={styles.filaInfo}>
										<Text style={styles.colIzda}>Experiència</Text>
										<Text style={styles.colDcha}>{tiempoExp}</Text>
									</View>
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>Formació</Text>
										<Text style={styles.colDcha}>{estudiosReq}</Text>
									</View> */}

									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.description}</Text>
										
											<Icon name='arrow-down'
											  type='simple-line-icon'
											  color='#6f6f6f'
											  size={32}
											  onPress={() => this.collapse()}
											/>
										
									</View>
									<Collapsible align='bottom' collapsed={this.state.isCollapsed}>
										<View style={styles.filaDescripcion}>
											<Text style={styles.textDescripcion}>{descripcion}</Text>
										</View>
									</Collapsible>
								</View>
								
							</View>

						</View>
						<Modal
					visible={this.state.showWeb}
					animationType={'slide'}
					onRequestClose={() => this.setState({
						showWeb: !this.state.showWeb
					})}

				>
					<View style={{ flex: 1, }}>
						<View style={styles.cabecera}>
							<TouchableOpacity style={styles.cancelarCabecera} onPress={() => this.setState({
								showWeb: !this.state.showWeb
							})}>
								<View style={styles.izqCabecera}>
									<Icon name='ios-arrow-back'
										type='ionicon'
										color='#FFFFFF'
										size={eH(35)}
									/>
								</View>
							</TouchableOpacity>
							<Text style={styles.textCabecera}>{web}</Text>
							<TouchableOpacity style={styles.guardarCabecera} >

							</TouchableOpacity>
						</View>

						<View style={{ flex: 1, backgroundColor: '#ecedeb' }}>
						<WebView 
						originWhitelist={['*']}
						startInLoadingState={true}
						source={{ uri: web ? web.startsWith("http") ? web : 'https://'+web : 'https://www.google.com' }} />
						</View>
						
					</View>


				</Modal>

					</ScrollView>
					{this.botonInscripcion()}
		
				</View>
				<Toast 
					ref="toastKo"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>
				<Toast 
					ref="toastOk"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={[styles.toast,{backgroundColor:"#67bf09"}]}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>
			</View>
		);
	}
}





const styles = StyleSheet.create({

  	cabecera:{
		backgroundColor:'#f39c10',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
		height:60,
		paddingHorizontal:8,
		width:'100%',
	},
	textCabecera:{
		paddingTop: 20,
		flex:0.6,
		textAlign:'center',
		color:'white',
		fontFamily:'Montserrat-SemiBold',
		fontSize:20,
	},
	cancelarCabecera:{
		flex:0.2,

	},
	izqCabecera:{
		alignItems:'flex-start',
		paddingLeft:10,
	},
	guardarCabecera:{
		flex:0.2,

	},
	abrirChat:{
		textAlign:'right',
		paddingRight:10,
		color:'white',
		fontSize:20,
	},
	scrollView: {
		// flex: 0.4,
		// justifyContent: 'center',
		// backgroundColor: 'grey',
	},
	intScroll: {
		// alignItems: 'center',
	},
	logoEmpresa:{
		marginTop: 10,
		width:'70%',
		height: 64,
		alignSelf: 'center',
		resizeMode: 'contain',
		// backgroundColor: '#ffffff',
	},
	cabeceraCuadro:{
		backgroundColor:'#ecedeb',
	},
	textCabeceraCuadro:{
		color:'#adadad',
		fontFamily:'Montserrat-Regular',
		fontSize: 12,
		paddingHorizontal: 4,
		paddingVertical: 12,
	},
	filaInfo:{
		flex:1,
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'space-between',
		backgroundColor: '#FFFFFF',
		paddingHorizontal: 10,
		height:50,
		// paddingVertical: 14,
		borderTopWidth: 0.5,
		borderTopColor: '#adadad',
	},
	colIzda:{
		color:'#adadad',
		fontFamily:'Montserrat-Regular',
		fontSize: 16,
		flex:0.32,
	},
	colDcha:{
		color:'#6f6f6f',
		fontFamily:'Montserrat-Regular',
		fontSize: 16,
		flex:0.68,
		textAlign: 'right',
	},
	filaDescripcion:{
		flex:1,
		padding:10,
		backgroundColor: '#FFFFFF',
		borderTopWidth: 0.5,
		borderTopColor: '#adadad',
	},
	textDescripcion:{
		color:'#6f6f6f',
		fontFamily:'Montserrat-Regular',
		fontSize: 16,
	},

	botonInscripcion:{
		// textAlign:'center',
		backgroundColor:'#f39c10',
		borderRadius: 4,
		paddingHorizontal: 20,
		marginTop: 10,
		height: 54,
	},
	textBotonInscripcion:{
		fontFamily:'Montserrat-Bold',
		fontSize: 18,
		color: '#FFFFFF',
	},
	toast:{
		width:300,
		borderRadius:20,
		backgroundColor:'#FF2B19'
	},
	toastText:{
		textAlign:'center',
		color:'white',
	},
});
