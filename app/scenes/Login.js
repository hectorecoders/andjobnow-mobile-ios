import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, TextInput, View, Image, AsyncStorage, Platform, Alert, Keyboard } from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { alerta } from '../Alerta.js';

import Spinner from 'react-native-loading-spinner-overlay';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

import * as Animatable from 'react-native-animatable';

import Toast, { DURATION } from 'react-native-easy-toast'

import { globales, strings } from '../globales.js';





export default class Login extends Component {


	constructor(props) {
		super(props)

		this.state = {
			emailLogin: "",
			passwordLogin: "",
			loadingVisible: false,

		}
	}


	cambioRegister = () => this.vista.fadeOutDown(250).then(endState =>
		this.props.registerPress()
	);

	cambioRecover = () => this.vista.fadeOutDown(250).then(endState =>
		this.props.recoverPress()
	);


	cambioLogin = () => this.vista.fadeOutDown(250).then(endState =>
		this.props.loginPress()
	);

	cambioFbLogin = () => this.vista.fadeOutDown(250).then(endState =>
		this.props.fbLoginPress()
	);




	validarLogin(estado) {

		console.log("valor estado validar", estado)

		let regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

		let resultado;

		if (regExp.test(estado.emailLogin)) {
			if (estado.passwordLogin.length > 0) {
				resultado = true;
			} else {
				resultado = strings.refill;
			}
		} else {
			resultado = strings.formatMail;
		}

		return resultado

	}

	// comprobarLogin = () => console.log("valor",this.state.emailLogin);
	comprobarLogin() {
		Keyboard.dismiss();
		let validacion = this.validarLogin(this.state);
		if (validacion === true) {

			// console.log("valor this.state",this.state);


			this.setState({
				loadingVisible: true,
			});


			let loginData = {
				email: this.state.emailLogin,
				password: this.state.passwordLogin,
			};

			// let data = new FormData();
			// data.append( "json", JSON.stringify( loginData ) );

			let data = JSON.stringify(loginData);
			console.log("data ", typeof (data), data);


			return fetch(globales.apiURL+'/api/login',
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',

					},
					body: data,
				})
				.then((response) => {
					return response.json();
				})
				.then((responseJson) => {
					console.log("resp json login", responseJson, responseJson["token"])

					this.setState({
						loadingVisible: false,
					});

					if (responseJson["token"]) {
						globales.token = responseJson["token"];
						this.realizarLogin();

					} else {

						this.refs.toast.show(responseJson["message"], 2000);
					}



					// tratarEmpresas(responseJson);
				})
				.catch((error) => {
					this.refs.toast.show('Network problem', 2000);
					this.setState({
						loadingVisible: false,
					});
					console.error(error);
				});
		} else {
			this.refs.toast.show(validacion, 2000);
		}
	}


	realizarLogin() {

		let loginData = {
			email: this.state.emailLogin,
			password: this.state.passwordLogin,
			token: true,
		};

		// let data = new FormData();
		// data.append( "json", JSON.stringify( loginData ) );

		let data = JSON.stringify(loginData);



		return fetch(globales.apiURL+'/api/login',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: data,
			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {


				this.setState({
					loadingVisible: false,
				});

				if (responseJson["status"] == "success") {
					console.log('USERLOGIN', responseJson)
					let user = responseJson["user"];
					globales.UID = user.id;
					globales.email = user.email;
					globales.nombre = user.name ? user.name : '';
					globales.apellidos = user.surname ? user.surname : '';
					AsyncStorage.setItem('jobUID', user.id.toString());
					AsyncStorage.setItem('jobEmail', user.email);
					AsyncStorage.setItem('jobNombre', globales.nombre);
					AsyncStorage.setItem('jobApellidos', globales.apellidos);
					AsyncStorage.setItem('jobToken', globales.token.toString());
					
					this.cambioLogin();

				} else {

					this.refs.toast.show(strings.errorAsk, 2000);
				}

			})
			.catch((error) => {
				this.refs.toast.show('Network problem', 2000);
				this.setState({
					loadingVisible: false,
				});
				console.error(error);
			});

	}


	responseFB = (error, result) => {
		if (error) {
			console.log('Error fetching data: ' + JSON.stringify(error));
		} else {
			console.log('Success fetching data: ', result);

			console.log("email -> ", result.email);


			if (result.email) {

				this.setState({
					loadingVisible: true,
				});


				let loginData = {
					"email": result.email,
					"password": result.id,
					"fb_login": true


				}


				let data = JSON.stringify(loginData);
				console.log("data ", typeof (data), data);


				return fetch(globales.apiURL+'/api/login',
					{
						method: "POST",
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',

						},
						body: data,
					})
					.then((response) => {
						return response.json();
					})
					.then((responseJson) => {
						console.log("resp json login", responseJson, responseJson["token"])

						this.setState({
							loadingVisible: false,
						});

						if (responseJson["status"] == "success") {

							globales.token = responseJson["token"];

							this.setState({
								emailLogin: result.email,
								passwordLogin: result.id,
							});


							this.realizarLogin();

						} else {

							this.refs.toast.show(responseJson["message"], 2000);
						}


					})
					.catch((error) => {
						console.error(error);
					});


			}

		}
	}

	loginFb() {

		const infoRequest = new GraphRequest(
			'/me?fields=id,name,email',
			null,
			this.responseFB,
		);

		LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
			function (result) {
				if (result.isCancelled) {
					console.log('Login was cancelled');
				} else {
					console.log('Login was successful with permissions: ' + result.grantedPermissions.toString());
					console.log(result);
					new GraphRequestManager().addRequest(infoRequest).start();
				}
			},
			function (error) {
				alert('Login failed with error: ' + error);
			}
		);


	}



	render() {
		return (
			<Animatable.View animation="fadeInDown" duration={700} ref={(r) => this.vista = r} style={{
				flex: 1,
				padding: 20,
				backgroundColor: "#f39c10",
				justifyContent: 'space-between'
			}}>
				<Spinner visible={this.state.loadingVisible} textStyle={{ color: '#FFF' }} />
				<View>
					<View style={{
						// marginTop:eH(60),
						// backgroundColor: 'blue',
						// flex:1,
					}}>
						<Image source={require('../img/logo.png')} style={{
							marginTop: eH(20),
							width: '70%',
							height: eH(100),
							alignSelf: 'center',
							resizeMode: 'contain',
							// backgroundColor: 'red',
						}} />
					</View>
					<View style={{
						marginTop: eH(60),
						// backgroundColor: 'blue',
						// flex:1,
					}}>

						<Input inputStyle={styles.input}
							containerStyle={styles.containerInput}
							placeholder='Email'
							placeholderTextColor="#FFFFFF"
							underlineColorAndroid='transparent'
							autoCapitalize='none'
							leftIcon={<Icon
								name='envelope-o'
								size={eH(16)}
								type='font-awesome'
								color='#FFFFFF'
								iconStyle={{ fontWeight: '200' }}
							/>}
							leftIconContainerStyle={styles.leftIcon}
							onChangeText={(emailLogin) => this.setState({ emailLogin })}
						/>
						<Input inputStyle={styles.input}
							containerStyle={[styles.containerInput, styles.containerInputPassword]}
							placeholder={strings.password} placeholderTextColor="#FFFFFF"
							secureTextEntry={true} underlineColorAndroid='transparent'
							leftIcon={<Icon
								name='lock'
								size={eH(22)}
								type='font-awesome'
								color='#FFFFFF'
								iconStyle={{ fontWeight: '200' }}
							/>}
							leftIconContainerStyle={styles.leftIcon}
							onChangeText={(passwordLogin) => this.setState({ passwordLogin })}
						/>

						<Button buttonStyle={{
							// textAlign:'center',
							backgroundColor: '#f7bc5a',
							borderRadius: eH(40),
							paddingLeft: eH(20),
							paddingRight: eH(20),
							marginTop: eH(40),

							height: eH(46),
						}}
							textStyle={{
								width: '100%',
								fontSize: eH(18),
								fontFamily: 'Montserrat-Regular',
								color: '#FFFFFF',

							}}
							onPress={() => this.comprobarLogin()}
							text={strings.access}

							clear
						/>
						<Text style={{
							alignSelf: 'center',
							color: 'white',
							fontSize: eW(16),
							fontFamily: 'Montserrat-Regular',
							marginTop: eH(40),
						}}
							onPress={this.cambioRecover}
						>
							{strings.forgotPass}
						</Text>
					</View>
				</View>
				<View style={{
					// flex:1,
					// alignSelf: 'flex-end',
					// alignItems:'center',
				}}>

					<Button buttonStyle={{
						// textAlign:'center',
						backgroundColor: '#3f5a9a',
						borderRadius: eH(40),
						paddingLeft: eH(20),
						paddingRight: eH(20),
						marginBottom: eH(20),

						height: eH(46),
					}}
						textStyle={{
							fontSize: eH(18),
							color: '#FFFFFF',
							fontFamily: 'Montserrat-Regular',
							width: '100%',

						}}
						onPress={() => this.loginFb()}
						text={strings.accessFB}

						clear
						icon={<Icon
							name='facebook-f'
							size={eH(22)}
							type='font-awesome'
							color='#FFFFFF'
							iconStyle={{ fontWeight: '200' }}
						/>}
						iconStyle={styles.leftIcon}
					/>
					<Text style={{
						alignSelf: 'center',
						color: 'white',
						fontFamily: 'Montserrat-Regular',
						fontSize: eW(18),
					}}
						//onPress={this.props.registerPress}
						onPress={this.cambioRegister}
					>{strings.notYet}</Text>

				</View>
				<Toast
					ref="toast"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>
			</Animatable.View>



		);

	}

}

const styles = StyleSheet.create({
	input: {
		flex: 1,
		borderColor: 'white',
		height: eH(46),
		fontSize: eH(20),
		fontFamily: 'Montserrat-Regular',
		fontWeight: '300',
		color: '#FFFFFF',
		marginLeft: 0,
		paddingLeft: eH(20),
		paddingTop: 0,
		paddingBottom: 0,
		// backgroundColor: 'red',
	},
	containerInput: {
		paddingLeft: eH(20),
		paddingRight: eH(20),
		borderRadius: eH(40),
		backgroundColor: '#689c15',
		width: '100%',
		height: eH(46),
		borderBottomWidth: 0,
	},
	containerInputPassword: {
		marginTop: eH(20),
	},
	leftIcon: {
		// backgroundColor:'blue',
		marginLeft: 0,
	},
	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	}
});