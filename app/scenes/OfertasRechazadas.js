import React from 'react';
import { Text, View, Image, StyleSheet, FlatList, ToastAndroid, TouchableOpacity, Modal, Alert, AppState, AsyncStorage } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';
import { Descripcion } from './Descripcion'
import RNRestart from 'react-native-restart';
import FCM from 'react-native-fcm';
import { LoginManager } from 'react-native-fbsdk';
import Toast, { DURATION } from 'react-native-easy-toast'

var gettingData = false

function getActividad(id) {


	let actividades = [];

	actividades[1] = strings.activity1;
	actividades[2] = strings.activity2;
	actividades[3] = strings.activity3;
	actividades[4] = strings.activity4;
	actividades[5] = strings.activity5;
	actividades[6] = strings.activity6;
	actividades[7] = strings.activity7;
	actividades[8] = strings.activity8;
	actividades[9] = strings.activity9;
	actividades[10] = strings.activity10;
	actividades[11] = strings.activity11;
	actividades[12] = strings.activity12;
	actividades[13] = strings.activity13;
	actividades[14] = strings.activity14;
	actividades[15] = strings.activity15;
	actividades[16] = strings.activity16;
	actividades[17] = strings.activity17;
	actividades[18] = strings.activity18;
	actividades[19] = strings.activity19;
	actividades[20] = strings.activity20;
	actividades[21] = strings.activity21;
	actividades[22] = strings.activity22;
	actividades[23] = strings.activity23;

	return actividades[id];


}

function getDuration(id) {


	let duration = [];

	duration[1] = strings.duration1;
	duration[2] = strings.duration2;
	duration[3] = strings.duration3;


	return duration[id];


}


function getStatus(id) {

	let status = [];
	let color = [];

	status[0] = strings.status0;
	status[1] = strings.status1;
	status[2] = strings.status2;
	status[3] = strings.status3;

	color[0] = "#f39c10";
	color[1] = "#FF2B19";
	color[2] = "#82bc29";
	color[3] = "#1a94af";

	return [status[id], color[id]];

}


export class OfertasRechazadas extends React.Component {


	constructor(props) {
		super(props)

		this.state = {
			offers: [],
			page: 1,
			selectedOffer: null,
			modalVisible: false
		}

	}

	componentDidMount() {
		this.obtenerOfertas(1);
	}

	obtenerOfertas(page) {
		console.log('entra')
		gettingData = true;
		setTimeout(() => { gettingData = false }, 1000)
		let filters = {
			"user": {
				"id": globales.UID,
			},
			name: null,
			sector: null,
			contract_duration: null,
			page: page
		};

		return fetch(globales.apiURL + '/api/offer/get-offers-declined',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(filters)
			})
			.then((response) => {

				return response.json();
			})
			.then((responseJson) => {
				console.log('DECLINADAS', [...this.state.offers, ...responseJson.offers])
				if (responseJson.offers) {
					this.setState({
						offers: [...this.state.offers, ...responseJson.offers]
					})
				}
			})
			.catch((error) => {
				console.error(error);
			});
	}

	logoutAccept() {
		FCM.unsubscribeFromTopic('notifications_' + globales.UID);
		globales.UID = null;
		globales.token = '';
		globales.email = '';
		globales.apellidos = '';
		globales.nombre = '';
		//AsyncStorage.removeItem('tourjob');
		AsyncStorage.removeItem('jobToken');
		AsyncStorage.removeItem('jobUID');
		AsyncStorage.removeItem('jobNombre');
		AsyncStorage.removeItem('jobApellidos');
		AsyncStorage.removeItem('jobEmail');
		LoginManager.logOut();
		RNRestart.Restart()
	}

	logout() {

		// Works on both iOS and Android
		Alert.alert(
			strings.closeSession,
			strings.wantCloseSession,
			[
				{ text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: strings.closeSession, onPress: () => this.logoutAccept() },
			],
			{ cancelable: false }
		)

	}

	listItem = ({ item, index }) => (
		<TouchableOpacity style={styles.filaItem} onPress={() => this.openModal(item)}>
			<View style={styles.colLogo}>
				{item.company.name_company === 'Oculto' ? 
					<Image blurRadius={30} source={item.company.logo ? { uri: globales.imageUrl + item.company.logo } : require('../img/noimg.png')} style={styles.logoEmpresa} />
				:  
					<Image source={item.company.logo ? { uri: globales.imageUrl + item.company.logo } : require('../img/noimg.png')} style={styles.logoEmpresa} />
				}
				
			</View>

			<View style={styles.infoItem}>
				<View style={styles.filaNombre}>
					<Text style={styles.nombre}>{(item.company.name_company && item.company.name_company !== 'Oculto' ? item.company.name_company : strings.hiddenCompany)}</Text>
				</View>
				<View style={styles.filaInfoUno}>
					<Text style={styles.cargo}>{item.title}</Text>
				</View>
				{/* 				<View style={styles.filaInfoDos}>
					<Text style={styles.datoInfoDos}>{item.jornada}</Text>
					<Text style={styles.datoInfoDos}> | </Text>
					<Text style={styles.datoInfoDos}>{item.duration}</Text>
					<Text style={styles.datoInfoDos}> | </Text>
					<Text style={styles.datoInfoDos}>{item.salario} €</Text>
				</View> */}


			</View>

			<TouchableOpacity style={styles.deleteButton} onPress={() => Alert.alert(
				strings.attention,
				strings.wantDeleteOffer,
				[

					{ text: strings.no, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
					{ text: strings.yes, onPress: () => this.eliminarOferta(item.id) },
				],
				{ cancelable: false }
			)}>
				<Icon name='ios-trash-outline' type='ionicon' color='#EC4242' size={eH(35)} />
			</TouchableOpacity>
		</TouchableOpacity>
	);

	nextPage() {
		console.log('NEXT PAGE')
		if (!gettingData) {
			let page = this.state.page + 1;
			this.setState({
				page: page
			})
			this.obtenerOfertas(page)
		}

	}

	eliminarOferta(offerID) {
		console.log('A eliminar', offerID)
		let dataBody = {
			"user": {
				"id": globales.UID
			},
			"offer": {
				"id": offerID
			}
		}

		return fetch(globales.apiURL + '/api/offer/update-offer-declined',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				console.log('eliminada', response)
				return response.json();
			})
			.then((responseJson) => {
				console.log('eliminada', responseJson)
				if (responseJson["status"] == "success") {
					let newOffers = this.state.offers;
					newOffers.splice(newOffers.findIndex(function (i) {
						return i.id === offerID;
					}), 1);
					this.setState({
						offers: newOffers
					})
				}

			})
			.catch((error) => {
				console.error(error);
			});
	}

	openModal(offer) {
		this.setState({
			selectedOffer: offer,
			modalVisible:true
		});
	}

	closeModal() {
		this.setState({modalVisible:false});
	}

	render() {

		return (

			<View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
				<View style={styles.cabecera}>
					<View style={{ flex: 0.2, }}>
						<TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }} onPress={() => this.props.navigation.pop()}>
							<Icon name='arrow-back'
								type='ionicons'
								color='#FFFFFF'
								size={eH(28)}
							/>
						</TouchableOpacity>
					</View>
					<Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(18) }}>{strings.declinedOffers}</Text>
					<View style={{ flex: 0.2 }}></View>
				</View>
				<FlatList
					extraData={this.state}
					data={this.state.offers}
					renderItem={this.listItem}
					onEndReached={() => this.nextPage()}
					onEndReachedThreshold={0.1}
				/>
				<Modal
					visible={this.state.modalVisible}
					animationType={'slide'}
					onRequestClose={() => this.closeModal()}
				>
					{this.state.selectedOffer ? 
						<Descripcion
						oferta={this.state.selectedOffer}
						idOferta={this.state.selectedOffer.id}
						idEmpresa={this.state.selectedOffer.company_id}
						imgEmpresa={this.state.selectedOffer.company.logo ? { uri: globales.imageUrl + this.state.selectedOffer.company.logo } : require('../img/noimg.png')}
						nombreEmpresa={this.state.selectedOffer.company.name_company}
						actividad={getActividad(this.state.selectedOffer.company.sector)}
						ubicacion={this.state.selectedOffer.company.address}
						cargo={this.state.selectedOffer.profession}
						tiempoExp={this.state.selectedOffer.exp_time}
						estudiosReq={this.state.selectedOffer.studies}
						descripcion={this.state.selectedOffer.description}
						titulo={this.state.selectedOffer.title}
						idUser={this.state.selectedOffer.company.user_id}
						candScene={false}
						closeModal={() => this.closeModal()}
						type='rejected'
					/>
						: null}
				</Modal>
			</View>
		);
	}
}





const styles = StyleSheet.create({

	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: eH(55),
		paddingHorizontal: 8,
		width: '100%',
	},
	textCabecera: {
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(20),
	},
	filaItem: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: '#FFFFFF',
		paddingHorizontal: 6,
		borderBottomWidth: 0.5,
		borderBottomColor: '#adadad',
	},
	colLogo: {
		backgroundColor: '#f4f4f4',
		marginHorizontal: 6,
		height: eH(70),
		alignSelf: 'center',
		justifyContent: 'center',
	},
	deleteButton: {
		marginHorizontal: 6,
		height: eH(70),
		alignSelf: 'center',
		justifyContent: 'center',
	},
	logoEmpresa: {
		width: eH(70),
		height: eH(70),
		// alignSelf: 'center',
		resizeMode: 'contain',
	},
	infoItem: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-between',
		height: eH(90),
		padding: 6,
		// backgroundColor:'#efefef',
	},

	filaNombre: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',

	},
	nombre: {
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		// backgroundColor:'#d2d2d2',
		color: '#6f6f6f',
	},

	filaInfoUno: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'flex-start',
		// backgroundColor:'#dedede',
	},
	fecha: {
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(10),
		paddingHorizontal: eH(6),
		paddingVertical: eH(2),

		color: '#FFFFFF',
		borderRadius: 4,
	},
	cargo: {
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(14),
		color: '#82bc29',

	},


	filaInfoDos: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'flex-end',
		// backgroundColor:'#d2d2d2',
	},
	datoInfoDos: {
		// backgroundColor:'#f2f2f2',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(12),
		color: '#adadad',
	},

	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	},

});
