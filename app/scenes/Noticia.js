import React from 'react';
import { View, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity, ToastAndroid, Alert, Dimensions, AsyncStorage, WebView } from 'react-native';
import { Icon as NewIcon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { globales } from '../globales.js';

import { Card, CardItem, Text, Left, Body } from 'native-base';
import HTML from 'react-native-render-html';




var gettingData = false

export class Noticia extends React.Component {
    constructor(props) {
        super(props)
        let lang = 'cat';
        if (globales.language !== 'ca') {
            lang = 'es'
        }
        this.state = {
            news: this.props.navigation.state.params.news,
            lang: lang
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.cabecera}>
                    <View style={{ flex: 0.2 }}>
                        <TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }} onPress={() => this.props.navigation.pop()}>
                            <NewIcon name='arrow-back' type='ionicons' color='#FFFFFF' size={eH(28)} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(20) }}>Blog</Text>
                    <View style={{ flex: 0.2 }}/>
                </View>
                <WebView startInLoadingState={true} source={{uri: globales.postURL+this.state.news.lang+'/'+this.state.news.blog_id}} style={{marginTop: -110}}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    cabecera: {
        backgroundColor: '#f39c10',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: eH(55),
        paddingHorizontal: 8,
        width: '100%',
        zIndex: 999
    },
    textCabecera: {
        flex: 0.6,
        textAlign: 'center',
        color: 'white',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: eH(20),
    },
    cancelarCabecera: {
        textAlign: 'left',
        paddingLeft: 10,
        flex: 0.2,
        color: 'white',
        fontSize: eH(20),

    },
    guardarCabecera: {
        textAlign: 'right',
        paddingRight: 10,
        flex: 0.2,
        color: 'white',
        fontSize: eH(20),

    },
    campos: {
        marginTop: eH(50),
    },
    filaInfo: {
        backgroundColor: '#FFFFFF',
        paddingHorizontal: eH(10),
        paddingVertical: eH(6),
        borderTopWidth: 0.5,
        borderTopColor: '#adadad',
    },
    colIzda: {
        color: '#adadad',
        fontFamily: 'Montserrat-Regular',
        fontSize: eH(16)
    },
    colDcha: {
        color: '#6f6f6f',
        fontFamily: 'Montserrat-Regular',
        fontSize: eH(16),
    },

    botonGuardar: {
        // textAlign:'center',
        backgroundColor: '#f39c10',
        borderRadius: 4,
        paddingHorizontal: eH(20),
        marginTop: eH(10),
        height: eH(54),
    },
    textBotonGuardar: {
        fontFamily: 'Montserrat-Bold',
        fontSize: eH(17),
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: '600',
    },
    input: {
        // flex:1,
        // borderColor: 'white',
        // height:eH(46),
        color: '#6f6f6f',
        fontFamily: 'Montserrat-Regular',
        fontSize: eH(16),
        // fontWeight: '300',
        // marginLeft: 0,
        // paddingLeft: eH(20),
        // backgroundColor: 'red',
    },
    containerInput: {
        // paddingLeft: eH(20),
        // paddingRight: eH(20),
        // borderRadius: eH(40),
        // backgroundColor:'#689c15',
        // width:'100%',
        // height:eH(46),
        borderBottomWidth: 0,
    },
    toast: {
        width: 300,
        borderRadius: 20,
        backgroundColor: '#FF2B19'
    },
    toastText: {
        textAlign: 'center',
        color: 'white',
    },
    buttonEnter: {
        width: '100%',
        backgroundColor: '#689c15',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textTitle: {
        fontFamily: 'Montserrat-Bold',
        fontSize: eH(15),
        color: '#43484D',
        textAlign: 'center',
        fontWeight: '600',
        //paddingHorizontal: 5,
    }
});

