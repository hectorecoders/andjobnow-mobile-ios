import React from 'react';
import { findNodeHandle, Text, View, Image, StyleSheet, ScrollView, TouchableOpacity, Alert, AsyncStorage, ListView, FlatList } from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';

import DatePicker from 'react-native-datepicker';
import ModalWrapper from 'react-native-modal-wrapper';
import ImagePicker from 'react-native-image-picker';
import RNRestart from 'react-native-restart';
import { LoginManager } from 'react-native-fbsdk';
import RNPickerSelect from 'react-native-picker-select';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

import Toast, { DURATION } from 'react-native-easy-toast'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FCM from 'react-native-fcm';

const countries = [
	{ value: 1, label: "Afganistán" },
	{ value: 2, label: "Afgana" },
	{ value: 3, label: "Alemania" },
	{ value: 4, label: "Andorra" },
	{ value: 5, label: "Arabia Saudita" },
	{ value: 6, label: "Argentina" },
	{ value: 7, label: "Australia" },
	{ value: 8, label: "Bélgica" },
	{ value: 9, label: "Bolivia" },
	{ value: 10, label: "Brasil" },
	{ value: 11, label: "Camboya" },
	{ value: 12, label: "Canadá" },
	{ value: 13, label: "Cataluña" },
	{ value: 14, label: "Chile" },
	{ value: 15, label: "China" },
	{ value: 16, label: "Colombia" },
	{ value: 17, label: "Corea" },
	{ value: 18, label: "Costa Rica" },
	{ value: 19, label: "Cuba" },
	{ value: 20, label: "Dinamarca" },
	{ value: 21, label: "Ecuador" },
	{ value: 22, label: "Egipto" },
	{ value: 23, label: "El Salvador" },
	{ value: 24, label: "España" },
	{ value: 25, label: "Estados Unidos" },
	{ value: 26, label: "Estonia" },
	{ value: 27, label: "Etiopia" },
	{ value: 28, label: "Filipinas" },
	{ value: 29, label: "Finlandia" },
	{ value: 30, label: "Francia" },
	{ value: 31, label: "Gales" },
	{ value: 32, label: "Grecia" },
	{ value: 33, label: "Guatemala" },
	{ value: 34, label: "Haití" },
	{ value: 35, label: "Holanda" },
	{ value: 36, label: "Honduras" },
	{ value: 37, label: "Indonesia" },
	{ value: 38, label: "Inglaterra" },
	{ value: 39, label: "Irak" },
	{ value: 40, label: "Irán" },
	{ value: 41, label: "Irlanda" },
	{ value: 42, label: "Israel" },
	{ value: 43, label: "Italia" },
	{ value: 44, label: "Japón" },
	{ value: 45, label: "Jordania" },
	{ value: 46, label: "Laos" },
	{ value: 47, label: "Letonia" },
	{ value: 48, label: "Lituania" },
	{ value: 49, label: "Malasia" },
	{ value: 50, label: "Marrueco" },
	{ value: 51, label: "México" },
	{ value: 52, label: "Nicaragua" },
	{ value: 53, label: "Noruega" },
	{ value: 54, label: "Nueva Zelanda" },
	{ value: 55, label: "Panamá" },
	{ value: 56, label: "Paraguay" },
	{ value: 57, label: "Perú" },
	{ value: 58, label: "Polonia" },
	{ value: 59, label: "Portugal" },
	{ value: 60, label: "Puerto" },
	{ value: 61, label: "República Dominicana" },
	{ value: 62, label: "Rumania" },
	{ value: 63, label: "Rusia" },
	{ value: 64, label: "Suecia" },
	{ value: 65, label: "Suiza" },
	{ value: 66, label: "Tailandia" },
	{ value: 67, label: "Taiwán" },
	{ value: 68, label: "Turquía" },
	{ value: 69, label: "Ucrania" },
	{ value: 70, label: "Uruguay" },
	{ value: 71, label: "Venezuela" },
	{ value: 72, label: "Vietnam" }
]

function totalString(val) {
	if (val) {
		val = val.toString();
	} else {
		val = "";
	}
	return val;
}


function getDate(timeStamp) {
	var date = new Date(timeStamp * 1000);
	var dia = date.getDate();
	var mes = (date.getMonth() + 1);
	var year = date.getFullYear();

	return mes + "/" + dia + "/" + year;
}

let nFilaExp = 0;



export class Curriculum extends React.Component {

	getNationality(nationality) {
		let textNationality = ''
		if (this.state.nationality > 0) {
			textNationality = countries[nationality - 1];
		}
		//console.log('NATION', textNationality.label)
		return textNationality.label;
	}


	constructor(props) {
		super(props)
		const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
		this.inputRefs = {};
		this.state = {
			addExpEduDisabled: false,
			dataSource: ds.cloneWithRows([]),
			dataSourceExp: ds.cloneWithRows([]),
			hasExp: false,
			hasEdu: false,
			experiences: [],
			experiencesEdit: [],
			experiencesEdu: [],
			experiencesEduEdit: [],
			rerender: false,
			editedEXP: [],
			editedEXPEdu: [],
			editResident: 0,
			editSexo: 0,
			editNationality: 0,
			sector: '',
			cno: '',
			sector2: '',
			cno2: '',
			sector3: '',
			cno3: '',
			itemsSex: [{ label: 'Hombre', value: '1' }, { label: 'Mujer', value: '2' }],
			itemsResident: [{ label: 'Si', value: '0' }, { label: 'No', value: '1' }],
			itemsCountries: countries,
			nationality: 0,
			description: '',
			languages: [],
			languagesList: [
				{
					id: 1,
					name: strings.catalan
				},
				{
					id: 2,
					name: strings.spanish
				},
				{
					id: 3,
					name: strings.french
				},
				{
					id: 4,
					name: strings.english
				},
				{
					id: 5,
					name: strings.russian
				},
				{
					id: 6,
					name: strings.portuguese
				},
				{
					id: 7,
					name: strings.others
				}
			],
			sectors: [
				{
					"value": 1,
					"label": strings.activity1
				},
				{
					"value": 2,
					"label": strings.activity2
				},
				{
					"value": 3,
					"label": strings.activity3
				},
				{
					"value": 4,
					"label": strings.activity4
				},
				{
					"value": 5,
					"label": strings.activity5
				},
				{
					"value": 6,
					"label": strings.activity6
				},
				{
					"value": 7,
					"label": strings.activity7
				},
				{
					"value": 8,
					"label": strings.activity8
				},
				{
					"value": 9,
					"label": strings.activity9
				},
				{
					"value": 10,
					"label": strings.activity10
				},
				{
					"value": 11,
					"label": strings.activity11
				},
				{
					"value": 12,
					"label": strings.activity12
				},
				{
					"value": 13,
					"label": strings.activity13
				},
				{
					"value": 14,
					"label": strings.activity14
				},
				{
					"value": 15,
					"label": strings.activity15
				},
				{
					"value": 16,
					"label": strings.activity16
				},
				{
					"value": 17,
					"label": strings.activity17
				},
				{
					"value": 18,
					"label": strings.activity18
				},
				{
					"value": 19,
					"label": strings.activity19
				},
				{
					"value": 20,
					"label": strings.activity20
				},
				{
					"value": 21,
					"label": strings.activity21
				},
				{
					"value": 22,
					"label": strings.activity22
				},
				{
					"value": 23,
					"label": strings.activity23
				}
			],
			contracts: [
				{
					"value": 1,
					"label": strings.duration1
				},
				{
					"value": 2,
					"label": strings.duration2
				},
				{
					"value": 3,
					"label": strings.duration3
				}
			],
			cnos: [
				{
					"value": 1,
					"label": strings.cno1
				},
				{
					"value": 2,
					"label": strings.cno2
				},
				{
					"value": 3,
					"label": strings.cno3
				},
				{
					"value": 4,
					"label": strings.cno4
				},
				{
					"value": 5,
					"label": strings.cno5
				},
				{
					"value": 6,
					"label": strings.cno6
				}
			],
			level: [
				{
					"value": 1,
					"label": strings.level1
				},
				{
					"value": 2,
					"label": strings.level2
				},
				{
					"value": 3,
					"label": strings.level3
				},
				{
					"value": 4,
					"label": strings.level4
				},
				{
					"value": 5,
					"label": strings.level5
				}
			],
			
		
		}

	}

	obtenerUsuario() {
		// console.log("globidUsu ",globales.UID;
		// console.log("token ",globales.token);

		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}

		return fetch(globales.apiURL + '/api/applicant/get-applicant-profile-new',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				console.log("obtenerUsuario responseJson", responseJson)

				this.tratarUsuario(responseJson.applicant);
			})
			.catch((error) => {
				console.error(error);
			});

	}


	tratarUsuario(usuarioJson) {

		let b64Img = globales.imageUrl + usuarioJson["image"]
		let sexo = totalString(usuarioJson["sex"])
		let telefono = totalString(usuarioJson["phone"]);
		let resident = totalString(usuarioJson["resident"])
		let sector = usuarioJson.sector ? this.state.sectors[parseInt(usuarioJson.sector)-1].label : '';
		let sector2 = usuarioJson.sector2 ? this.state.sectors[parseInt(usuarioJson.sector2)-1].label : '';
		let sector3 = usuarioJson.sector3 ? this.state.sectors[parseInt(usuarioJson.sector3)-1].label : '';
		let cno = usuarioJson.cno ? this.state.cnos[parseInt(usuarioJson.cno)-1].label : '';
		let cno2 = usuarioJson.cno2 ? this.state.cnos[parseInt(usuarioJson.cno2)-1].label : '';
		let cno3 = usuarioJson.cno3 ? this.state.cnos[parseInt(usuarioJson.cno3)-1].label : '';
		let contract = usuarioJson.contract_duration ? this.state.contracts[parseInt(usuarioJson.contract_duration)-1].label : '';
		let nationality = totalString(usuarioJson["nationality"])
		let nacimiento = "1981/01/01"
		if (usuarioJson["birthdate"] != 0) {
			nacimiento = getDate(usuarioJson["birthdate"]);
		}

		// console.log("nacimiento",nacimiento);

		this.setState({
			nombre: usuarioJson.name,
			apellidos: usuarioJson.surname,
			b64Img: b64Img,
			sexo: sexo,
			nacimiento: nacimiento,
			telefono: telefono,
			resident: resident,
			sector: sector,
			sector2: sector2,
			sector3: sector3,
			cno: cno,
			cno2: cno2,
			cno3: cno3,
			contract: contract,
			nationality: nationality,
			editNombre: usuarioJson.name,
			editApellidos: usuarioJson.surname,
			editB64Img: b64Img,
			editSexo: sexo,
			editNacimiento: nacimiento,
			editResident: resident,
			editNationality: nationality,
			editTelefono: telefono,
			EditedExperiencias: [],
			deleteExps: [],
			hasEdu: !usuarioJson.academic_inexperience,
			hasExp: !usuarioJson.work_inexperience,
		});

	}
	getDescription() {

		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}

		return fetch(globales.apiURL + '/api/applicant/get-applicant-introduction',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {

				return response.json();
			})
			.then((responseJson) => {
				if (responseJson.status == "success") {
					this.setState({
						description: responseJson.introduction,
					})
				}
			})
			.catch((error) => {
				console.error(error);
			});
	}

	obtenerLenguajes() {
		// console.log("globidUsu ",globales.idUsuario);


		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}

		return fetch(globales.apiURL + '/api/applicant/get-applicant-language',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {

				return response.json();
			})
			.then((responseJson) => {
				//this.tratarExperiencias(responseJson.experience);
				let selectedLang = [];
				let auxLang;
				let languageLevels = responseJson.nivel_idiomas;
				responseJson.language.map((lang, index) => {
					console.log('El index es,', index);
					console.log('El languageLevels es,', languageLevels);
					auxLang = this.state.languagesList.find(x => x.id === parseInt(lang));
					if(languageLevels){
						auxLevel = this.state.level.find(x => x.value === (languageLevels[index] ? languageLevels[index] : null)) ;
					} else {
						auxLevel = null;
					}
					if(auxLang && auxLevel){
						selectedLang.push(auxLang.name+'-'+auxLevel.label);
					}
					
				})

				this.setState({
					languages: <View style={{ width: '100%', padding: 5 }}>{
						selectedLang.map((language,index) => {
							return <View key={'language'+index} style={{width: '100%',justifyContent: 'flex-start',alignItems: 'flex-start', flexDirection: 'row', borderWidth: 1, borderRadius: 5, borderColor: 'gray', padding: 5, marginBottom: 10}}>
										<Text style={{width: '100%', color: 'gray',fontFamily: 'Montserrat-Medium', fontSize: eH(12)}}>{language}</Text>
									</View>
							
						})
					}</View>
				})

			})
			.catch((error) => {
				console.error(error);
			});

	}

	obtenerExperiencias() {
		// console.log("globidUsu ",globales.UID;


		let dataBody = {
			"user": {
				"id": globales.UID,
			}
		}

		return fetch(globales.apiURL + '/api/applicant/get-applicant-experience',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				console.log('UPDATE EXP')
				this.tratarExperiencias(responseJson.experience);
			})
			.catch((error) => {
				console.error(error);
			});

	}

	obtenerExperienciasEducacion() {
		// console.log("globidUsu ",globales.idUsuario);


		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}

		return fetch(globales.apiURL + '/api/applicant/get-applicant-education',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				console.log('UPDATE EDU')
				this.tratarExperienciasEducacion(responseJson.education);

			})
			.catch((error) => {
				console.error(error);
			});

	}

	tratarExperienciasEducacion(jsonExperiencias) {


		let experiencias = [];

		let varios = false;


		Object.keys(jsonExperiencias).forEach(function (clave) {


			let objExperiencia = jsonExperiencias[clave];

			let name = objExperiencia["name"];
			let center = objExperiencia["center"];
			let fechaInicio = objExperiencia["start_year"];
			let fechaFinal = objExperiencia["end_year"];
			let description = objExperiencia["description"];
			// console.log(clave,typeof(objExperiencia),objExperiencia,objExperiencia["cargo"]);

			nFilaExp = nFilaExp + 1;

			// console.log(fechaInicio);

			if (varios) {

				experiencias.push(
					<View key={"linea" + nFilaExp} style={{ backgroundColor: '#adadad', height: eH(10) }} />
				);

			} else {
				varios = true;
			}

			experiencias.push(


				<View key={"exp" + nFilaExp}>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.name}</Text>
						<Text style={[styles.colDcha, { fontFamily: 'Montserrat-SemiBold' }]}>{name}</Text>
					</View>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.academy}</Text>
						<Text style={styles.colDcha}>{center}</Text>
					</View>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.startYear}</Text>
						<Text style={styles.colDcha}>{fechaInicio}</Text>
					</View>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.endYear}</Text>
						<Text style={styles.colDcha}>{fechaFinal}</Text>
					</View>
					<View style={styles.filaInfoCol}>
						<Text style={styles.colFull2}>{strings.description}</Text>
						<Text style={styles.colFull}>{description}</Text>
					</View>
				</View>

			);

		});





		this.setState({
			experienciaEdu: experiencias,
			jsonEditExperienciasEdu: jsonExperiencias,

		});

	}

	tratarExperiencias(jsonExperiencias) {


		let experiencias = [];

		let varios = false;


		Object.keys(jsonExperiencias).forEach(function (clave) {


			let objExperiencia = jsonExperiencias[clave];

			let cargo = objExperiencia["departament"];
			let empresa = objExperiencia["company"];
			let fechaInicio = objExperiencia["start_year"];
			let fechaFinal = objExperiencia["end_year"];
			let description = objExperiencia["description"];
			// console.log(clave,typeof(objExperiencia),objExperiencia,objExperiencia["cargo"]);

			nFilaExp = nFilaExp + 1;

			// console.log(fechaInicio);

			if (varios) {

				experiencias.push(
					<View key={"linea" + nFilaExp} style={{ backgroundColor: '#adadad', height: eH(10) }} />
				);

			} else {
				varios = true;
			}

			experiencias.push(


				<View key={"exp" + nFilaExp}>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.charge}</Text>
						<Text style={[styles.colDcha, { fontFamily: 'Montserrat-SemiBold' }]}>{cargo}</Text>
					</View>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.company}</Text>
						<Text style={styles.colDcha}>{empresa}</Text>
					</View>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.startYear}</Text>
						<Text style={styles.colDcha}>{fechaInicio}</Text>
					</View>
					<View style={styles.filaInfo}>
						<Text style={styles.colIzda}>{strings.endYear}</Text>
						<Text style={styles.colDcha}>{fechaFinal}</Text>
					</View>
					<View style={styles.filaInfoCol}>
						<Text style={styles.colFull2}>{strings.description}</Text>
						<Text style={styles.colFull}>{description}</Text>
					</View>
				</View>

			);


		});



		//this.edicionExperiencia(jsonExperiencias);

		this.setState({
			experiencia: experiencias,
			jsonEditExperiencias: jsonExperiencias,

		});

	}


	cabeceraDinamica() {


		let botonCancelar;
		let botonDerecho;


		botonDerecho =
			<TouchableOpacity style={styles.intDchaCabecera} onPress={() => this.props.navigation.push('Wizard', { from: 'cv', reload: this.reloadView.bind(this) })}>
				<Text style={[styles.textoDerecho,{ fontFamily: 'Montserrat-SemiBold'}]}>{strings.edit}</Text>
			</TouchableOpacity>;



		return (

			<View style={styles.cabecera}>

				<View style={{ flex: 0.2, }}>
					<TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }}
						onPress={() => { this.logout() }}>
						<Icon name='power'
							type='feather'
							color='#FFFFFF'
							size={eH(28)}
						/>
					</TouchableOpacity>
				</View>
				<Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(20) }}>Currículum Vitae</Text>

				<View style={{ flex: 0.2 }}>
					{botonDerecho}
				</View>

			</View>
		);



	}

	logoutAccept() {
		FCM.unsubscribeFromTopic('notifications_' + globales.UID);
		globales.UID = null;
		globales.token = '';
		globales.email = '';
		globales.apellidos = '';
		globales.nombre = '';
		//AsyncStorage.removeItem('tourjob');
		AsyncStorage.removeItem('jobToken');
		AsyncStorage.removeItem('jobUID');
		AsyncStorage.removeItem('jobNombre');
		AsyncStorage.removeItem('jobApellidos');
		AsyncStorage.removeItem('jobEmail');
		LoginManager.logOut();
		RNRestart.Restart()
	}

	logout() {

		// Works on both iOS and Android
		Alert.alert(
			strings.closeSession,
			strings.wantCloseSession,
			[
				{ text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: strings.closeSession, onPress: () => this.logoutAccept() },
			],
			{ cancelable: false }
		)

	}

	reloadView() {
		this.obtenerUsuario();
		this.obtenerLenguajes();
		this.obtenerExperiencias();
		this.getDescription();
		this.obtenerExperienciasEducacion();
	}

	componentDidMount() {
		strings.setLanguage(globales.language);
		this.setState({});
		this.reloadView()
	}


	onSelectedItemsChange = (selectedItems) => {
		this.setState({ selectedItems });
	}
	_scrollToInput(reactNode: any) {
		// Add a 'scroll' ref to your ScrollView
		this.scroll.props.scrollToFocusedInput(reactNode)
	}

	render() {

		// let idUsuario        = this.state.idUsuario;
		let nombre = this.state.nombre;
		let apellidos = this.state.apellidos;
		let sexo = this.state.sexo;
		let resident = this.state.resident;
		let nationality = this.state.nationality;

		let nacimiento = this.state.nacimiento;
		let telefono = this.state.telefono;
		let experiencia = this.state.experiencia;
		let experienciaEdu = this.state.experienciaEdu;
		let b64Img = {};

		//console.log("b64Img ant",b64Img);
		if (this.state.b64Img) {
			// console.log("b64Img ant",this.state.b64Img);
			b64Img = { uri: this.state.b64Img };
			globales.avatar = this.state.b64Img;
		}
		else {
			b64Img = require('../img/default.png');
			globales.avatar = require('../img/default.png');
		}
		// console.log("b64Img dsp",b64Img);

		return (
			<View style={{ flex: 1, }}>
				{this.cabeceraDinamica()}

				<View style={{ flex: 1, backgroundColor: '#ecedeb' }}>
					<ScrollView style={styles.scrollView}>
						<View style={{
							flex: 1, flexDirection: 'column',
							justifyContent: 'space-between',
						}}>
							<View style={styles.cabeceraCuadro}>
								<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.profileImage}</Text>
								<Image source={this.state.b64Img ? { uri: this.state.b64Img } : require('../img/default.png')} style={styles.imgPerfil} />
							</View>
							<View style={[styles.cabeceraConBoton]}>
								<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.generalData}</Text>
							</View>

							<View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.name}</Text>
									<Text style={styles.colDcha}>{nombre}</Text>
								</View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.lastName}</Text>
									<Text style={styles.colDcha}>{apellidos}</Text>
								</View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.email}</Text>
									<Text style={[styles.colDcha, { fontSize: eH(14), }]}>{globales.email}</Text>
								</View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.sex}</Text>
									<Text style={styles.colDcha}>{sexo == 1 ? strings.man : strings.woman}</Text>
								</View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.resident}</Text>
									<Text style={styles.colDcha}>{resident == 0 ? strings.yes : strings.no}</Text>
								</View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.contractType}</Text>
									<Text style={styles.colDcha}>{this.state.contract}</Text>
								</View>
								
								

								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.nation}</Text>
									<Text style={styles.colDcha}>{this.getNationality(nationality)}</Text>
								</View>

								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.birthDate}</Text>
									<Text style={styles.colDcha}>{nacimiento}</Text>
								</View>
								<View style={styles.filaInfo}>
									<Text style={styles.colIzda}>{strings.contactPhone}</Text>
									<Text style={styles.colDcha}>{telefono}</Text>
								</View>
							</View>
						</View>
						<View>
							<View style={[styles.cabeceraConBoton]}>
								<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.presentation} </Text>
							</View>
							<View style={styles.filaInfo}>
								<Text style={styles.colFull}>{this.state.description}</Text>
							</View>
						</View>
						<View style={[styles.cabeceraConBoton]}>
							<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.workExp}</Text>
						</View>
						{this.state.hasExp ?
							<View>{ experiencia }</View>
							:
							<View style={styles.filaInfo}>
								<Text style={styles.colFull}>{strings.noExp}</Text>
							</View>
						}

						<View>
							<View style={[styles.cabeceraConBoton]}>
								<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.formation}</Text>
							</View>
							{this.state.hasEdu ?
								<View>{ experienciaEdu }</View>
								:
								<View style={styles.filaInfo}>
									<Text style={styles.colFull}>{strings.noFormation}</Text>
								</View>
							}

						</View>
						<View>
							<View style={[styles.cabeceraConBoton]}>
								<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.languages} </Text>
							</View>
							<View style={styles.filaInfo}>
								{this.state.languages}
							</View>
						</View>
						<View>
								<View style={[styles.cabeceraConBoton]}>
									<Text style={[styles.textCabeceraCuadro, { fontFamily: 'Montserrat-SemiBold', color: '#6F6F6F' }]}>{strings.sectors} </Text>
								</View>
								{this.state.sector ? 
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.sector} 1</Text>
										<Text style={styles.colDcha}>{this.state.sector}</Text>
									</View>
									: null}
								{this.state.cno ? 
									<View style={[styles.filaInfo, { marginBottom: 20}]}>
										<Text style={styles.colIzda}>{strings.position} 1</Text>
										<Text style={styles.colDcha}>{this.state.cno}</Text>
									</View>
									: null}
									{this.state.sector2 ? 
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.sector} 2</Text>
										<Text style={styles.colDcha}>{this.state.sector2}</Text>
									</View>
									: null}
								{this.state.cno2 ? 
									<View  style={[styles.filaInfo, { marginBottom: 20}]}>
										<Text style={styles.colIzda}>{strings.position} 2</Text>
										<Text style={styles.colDcha}>{this.state.cno2}</Text>
									</View>
									: null}
									{this.state.sector3 ? 
									<View style={styles.filaInfo}>
										<Text style={styles.colIzda}>{strings.sector} 3</Text>
										<Text style={styles.colDcha}>{this.state.sector3}</Text>
									</View>
									: null}
								{this.state.cno3 ? 
									<View  style={[styles.filaInfo, { marginBottom: 20}]}>
										<Text style={styles.colIzda}>{strings.position} 3</Text>
										<Text style={styles.colDcha}>{this.state.cno3}</Text>
									</View>
									: null}
								
							</View>
						
					</ScrollView>

					<Toast
						ref="toastKo"
						opacity={0.9}
						fadeInDuration={500}
						fadeOutDuration={800}
						style={styles.toast}
						textStyle={styles.toastText}
						positionValue={eH(200)}
					/>

					<Toast
						ref="toastOk"
						opacity={0.9}
						fadeInDuration={500}
						fadeOutDuration={800}
						style={[styles.toast, { backgroundColor: "#67bf09" }]}
						textStyle={styles.toastText}
						positionValue={eH(200)}
					/>

				</View>
			</View>
		);
	}
}




const styles = StyleSheet.create({

	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: eH(55),
		paddingHorizontal: 8,
		width: '100%',
	},
	textCabecera: {
		flex: 0.6,
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(20),
	},

	izdaCabecera: {
		flex: 0.2,

	},
	intIzdaCabecera: {
		alignItems: 'flex-start',
		paddingLeft: 10,
	},
	dchaCabecera: {
		flex: 0.2,

	},
	intDchaCabecera: {

	},
	textoDerecho: {
		textAlign: 'right',
		paddingRight: 10,
		color: 'white',
		fontSize: 20,
		paddingTop: 5
	},
	scrollView: {
		flex: 1,
		// justifyContent: 'center',
		// backgroundColor: 'grey',
	},
	intScroll: {
		// alignItems: 'center',
	},
	imgPerfil: {
		height: 150,
		width: 150,
		borderRadius: 75,
		alignSelf: 'center',
		// backgroundColor: '#ffffff',
	},
	cabeceraCuadro: {
		flexDirection: 'column',
		justifyContent: 'space-between',
		backgroundColor: '#ecedeb',
	},
	cabeceraConBoton: {
		backgroundColor: '#ecedeb',
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'stretch',
	},
	textCabeceraPerfil: {
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(12),
	},
	textCabeceraCuadro: {
		color: '#adadad',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		paddingHorizontal: eH(10),
		paddingVertical: eH(10),
		alignSelf: 'flex-start',
		justifyContent: 'flex-start',
	},
	botonCabeceraCuadro: {
		backgroundColor: '#f39c10',
		// height:eH(26),
		// paddingVertical: eH(0),
		paddingHorizontal: eH(6),
		marginHorizontal: eH(10),
	},
	textBotonCabeceraCuadro: {
		alignSelf: 'flex-end',
		justifyContent: 'flex-end',
		paddingVertical: eH(2),
	},
	filaInfo: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',

		alignItems: 'stretch',
		backgroundColor: '#FFFFFF',
		paddingHorizontal: eH(10),
		paddingVertical: eH(14),
		borderTopWidth: 0.5,
		borderTopColor: '#adadad',
	},
	filaInfoCol: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-between',

		alignItems: 'stretch',
		backgroundColor: '#FFFFFF',
		paddingHorizontal: eH(10),
		paddingVertical: eH(14),
		borderTopWidth: 0.5,
		borderTopColor: '#adadad',
	},
	colIzda: {
		color: '#adadad',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		alignSelf: 'flex-start',
		width: '50%'
	},
	colDcha: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		alignSelf: 'flex-end',
		width: '50%'
	},
	colFull: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		alignSelf: 'flex-start',
		width: '100%'
	},
	colFull2: {
		color: '#adadad',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		alignSelf: 'flex-start',
		width: '100%'
	},
	botonInscripcion: {
		// textAlign:'center',
		backgroundColor: '#f39c10',
		borderRadius: 4,
		paddingHorizontal: eH(20),
		marginTop: eH(10),
		height: eH(54),
	},
	textBotonInscripcion: {
		fontFamily: 'Montserrat-Bold',
		fontSize: eH(18),
		color: '#FFFFFF',
		fontWeight: '200',
	},
	wrapperGenerales: {
		width: "90%",
		height: "80%",
		justifyContent: 'space-between',
		paddingLeft: 24,
		paddingRight: 24
	},
	wrapperFila: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		paddingHorizontal: eH(10),
		paddingVertical: eH(24),
		height: eH(40),
		// borderBottomWidth: 0.5,
		// borderBottomColor: '#adadad',
	},
	wrapperColIzda: {
		color: '#3f3f3f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(14),
		fontWeight: '600',
		marginTop: eH(16),
		flexDirection: 'row',
		flex: 1,
		height: eH(40),
		// backgroundColor: '#312321',
		// height: eH(100),
	},
	inputWrapperCol: {
		height: eH(40),
		flex: 1,
		flexDirection: 'row',
		// width: "50%",
		alignItems: "center",
		// alignSelf: "center"
		// backgroundColor: "red",
	},
	inputWrapper: {
		// width: "50%",
		flex: 1,
		// borderColor: 'white',
		// height:eH(46),
		alignSelf: 'flex-end',
		// alignItems: 'flex-end',
		textAlign: 'right',
		color: '#adadad',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(14),
		height: eH(17),
		marginLeft: 0,
		// marginTop:-20,
		// paddingLeft: eH(0),
		// paddingTop: 0,
		//paddingBottom: 0,
		// backgroundColor: 'red',
	},
	containerInputWrapper: {
		alignItems: 'flex-end',
		alignSelf: 'flex-end',
		// paddingLeft: eH(20),
		// paddingRight: eH(20),
		// borderRadius: eH(40),
		// backgroundColor:'#689c15',
		width: eH(140),
		// height:eH(46),
		// borderBottomWidth:0,
	},
	infWrapper: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-around",
	},
	botonWrapper: {
		// textAlign:'center',
		backgroundColor: '#f39c10',
		borderRadius: 4,
		marginTop: eH(10),
		height: eH(44),
		width: eH(120),
	},
	textBotonWrapper: {
		fontFamily: 'Montserrat-Bold',
		fontSize: eH(18),
		color: '#FFFFFF',
		fontWeight: '200',
	},

	wrapperExperiencias: {
		width: "90%",
		height: "80%",
		justifyContent: 'space-between',
		// paddingLeft: 24, 
		// paddingRight: 24 
	},

	wrapperScrollView: {
		// width: '90%',
		flex: 1,
		paddingHorizontal: eH(24),
		marginTop: eH(16),
		// justifyContent: 'center',
		// backgroundColor: 'grey',
	},
	wrapperFilaExperiencias: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
		// backgroundColor: 'red',
		paddingHorizontal: eH(10),
		paddingVertical: eH(4),
		// borderBottomWidth: 0.5,
		// borderBottomColor: '#adadad',
	},
	wrapperColIzdaExperiencia: {
		color: '#3f3f3f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(14),
		fontWeight: '600',
		marginTop: eH(16),
		flex: 1,
	},
	inputWrapperColExperiencia: {
		flex: 1,
	},
	blockRemoveExp: {
		justifyContent: 'flex-end',
		height: eH(56),
	},
	botonRemoveExp: {
		backgroundColor: '#f45c42',
		borderRadius: 4,
		paddingHorizontal: eH(12),
		// paddingVertical:eH(0),
		marginBottom: eH(8),
		marginTop: eH(10),
		// height: eH(30),

	},
	textBotonRemoveExp: {
		paddingVertical: eH(8),
	},
	blockAddExp: {
		justifyContent: 'flex-end',
		height: eH(50),
	},
	botonAddExp: {
		// textAlign:'center',
		backgroundColor: '#f39c10',
		borderRadius: 4,
		paddingHorizontal: eH(16),
		// marginVertical: eH(10),
		marginBottom: eH(10),
		// marginTop: eH(20),
		// height: eH(44),
		// width: eH(120),
	},
	infWrapperExperiencias: {
		height: eH(60),
		// paddingBottom:eH(10),
		marginBottom: eH(16),
		alignItems: 'center',
		flexDirection: "row",
		justifyContent: "space-around",
	},
	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	},
	buttonDisabled: {
		// opacity:0.4,
		backgroundColor: '#eeeeee',
	},
	bloqueActImagen: {
		justifyContent: 'center',
		alignItems: 'center',
		height: eH(32),
	},
	textActImagen: {
		color: '#f39c10',
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(16),
	},



});

const pickerSelectStyles = StyleSheet.create({

	inputIOS: {
		fontSize: 16,
		fontFamily: 'Montserrat-Regular',
		paddingTop: 13,
		paddingHorizontal: 10,
		paddingBottom: 12,
		borderWidth: 1,
		borderColor: 'gray',
		borderRadius: 4,
		backgroundColor: 'white',
		color: 'black',
	},
});