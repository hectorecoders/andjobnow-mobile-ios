import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, TextInput, View, Image, Linking, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { alerta } from '../Alerta.js';

import * as Animatable from 'react-native-animatable';
/* import { RTCPeerConnection,
	RTCIceCandidate,
	RTCSessionDescription,
	RTCView,
	MediaStream,
	MediaStreamTrack,
	getUserMedia } from 'react-native-webrtc'; */




export default class Home extends Component {


	constructor (props) {
		super(props)

		this.state = {
		}
	}


	enterLogin = () => this.vista.fadeOutDown(250).then( endState => 
		this.props.enterLogin()	
	);

	/* componentDidMount(){
		const configuration = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
		const pc = new RTCPeerConnection(configuration);
		const { isFront } = this.state;
		MediaStreamTrack.getSources(sourceInfos => {
			console.log(sourceInfos);
			let videoSourceId;
			for (let i = 0; i < sourceInfos.length; i++) {
			  const sourceInfo = sourceInfos[i];
			  if(sourceInfo.kind === "video" && sourceInfo.facing === (isFront ? "front" : "back")) {
				videoSourceId = sourceInfo.id;
			  }
			}
			getUserMedia({
			  audio: true,
			  video: Platform.OS === 'ios' ? false : {
				mandatory: {
				  minWidth: 500, // Provide your own width, height and frame rate here
				  minHeight: 300,
				  minFrameRate: 30
				},
				facingMode: (isFront ? "user" : "environment"),
				optional: (videoSourceId ? [{sourceId: videoSourceId}] : [])
			  }
			}, (stream) => {
			  console.log('Streaming OK', stream);
			  this.setState({
				  videoURL: stream.toURL()
			  });
			  pc.addStream(stream);
			  
			}, error => {
				console.log('Oops, we getting error', error.message);
				throw error;
			});
		  });

		  pc.createOffer((desc) => {
			pc.setLocalDescription(desc,  () => {
			  // Send pc.localDescription to peer
			  console.log('setLocalDescription');
			}, (e) => {throw e;});
		  }, (e) => {throw e;});
		  
		  pc.onicecandidate =  (event) => {
			// send event.candidate to peer
			console.log('onicecandidate', event);
		  };
	} */





	render() {
		const { videoURL } = this.state;
        
		/* return (
			<RTCView streamURL={this.state.video} styles={{width: '20%', height: '30%', backgroundColor: '#ccc', borderWidth: 1, borderColor: '#000'}}/>	
		); */
		 return (
			<Animatable.View animation="fadeInDown" duration={700} ref={(r) => this.vista = r} style={{
				flex:1, 
				padding: 20, 
				backgroundColor: "#f39c10", 
				justifyContent: 'space-between'
			}}>
				<View>
					<View style={{
							marginTop:eH(80),
							// backgroundColor: 'blue',
							// flex:1,
						}}>
						<Image source={require('../img/logo.png')} style={{
							marginTop: eH(20),
							width:'70%',
							height: eH(120),
							alignSelf: 'center',
							resizeMode: 'contain',
							// backgroundColor: 'red',
						}} />
						<Image source={require('../img/textlogo.png')} style={{
							marginTop: eH(20),
							width:'74%',
							height: eH(50),
							alignSelf: 'center',
							resizeMode: 'contain',
							// backgroundColor: 'red',
						}} />
					</View>
					<View style={{
							marginTop:eH(10),
							// backgroundColor: 'blue',
							// flex:1,
						}}>

						
						<Button buttonStyle={{
							// textAlign:'center',
							backgroundColor:'#f7bc5a',
							borderRadius: eH(40),
							paddingLeft: eH(16),
							paddingRight: eH(16),
							marginTop: eH(40),
							height: eH(40),
							}}
							textStyle={{
								fontSize:eH(18),
								fontFamily:'Montserrat-Regular',
								color: '#FFFFFF',
								width:'80%',
							}}
							onPress={() => this.enterLogin()}
							text="Trobar Oportunitats"
		
							clear
						/>
						

					</View>
	

				</View>
				
			</Animatable.View>
		) 

	}

}

const styles = StyleSheet.create({
	
});