import React from 'react';
import { Text, View, Image, TouchableOpacity, Modal, Button, StyleSheet } from 'react-native';
import { Descripcion } from './Descripcion'
import Chat from './Chat'
import { eW, eH, eM } from '../Escalas';


import Toast, {DURATION} from 'react-native-easy-toast'

import { globales, strings } from '../globales.js';

let ofertas = [];
let empresas = [];
let cards = [];






function getActividad(id){


	let actividades = [];

	actividades[1] = strings.activity1;
	actividades[2] = strings.activity2;
	actividades[3] = strings.activity3;
	actividades[4] = strings.activity4;
	actividades[5] = strings.activity5;
	actividades[6] = strings.activity6;
	actividades[7] = strings.activity7;
	actividades[8] = strings.activity8;
	actividades[9] = strings.activity9;
	actividades[10] = strings.activity10;
	actividades[11] = strings.activity11;
	actividades[12] = strings.activity12;
	actividades[13] = strings.activity13;
	actividades[14] = strings.activity14;
	actividades[15] = strings.activity15;
	actividades[16] = strings.activity16;
	actividades[17] = strings.activity17;
	actividades[18] = strings.activity18;
	actividades[19] = strings.activity19;
	actividades[20] = strings.activity20;
	actividades[21] = strings.activity21;
	actividades[22] = strings.activity22;
	actividades[23] = strings.activity23;

	return actividades[id];


}


function obtenerOfertas(){

	return fetch('https://andjobnow-cb1f.restdb.io/rest/ofertas',
  	{
  		method: "GET",
		headers: {
			Accept: 'application/json',
    		'Content-Type': 'application/json',
	
 			'x-apikey': '4e6fc413ae5bf3d51267926be01bd4790c689'
		}
  	})
    .then((response) => {
    	return response.json();
    })
    .then((responseJson) => {
    	tratarOfertas(responseJson);
    })
    .catch((error) =>{
      console.error(error);
    });

}

function obtenerOfertasDos(){
	console.log('ofertas')
	return fetch(globales.apiURL+'/api/offer/all-offers-client',
  	{
  		method: "POST",
		headers: {
			Accept: 'application/json',
    		'Content-Type': 'application/json',
		}
  	})
    .then((response) => {
    	return response.json();
    })
    .then((responseJson) => {
		 console.log("responseJson ",responseJson);
		
    	tratarOfertas(responseJson);
    })
    .catch((error) =>{
      console.error(error);
    });

}



function tratarOfertas(ofertasJson){

	ofertas = [];

	
	Object.keys(ofertasJson.offers).forEach(function(key) {


		let oferta = ofertasJson.offers[key];

		ofertas.push(oferta);

		// console.log("oferta -> ",key,oferta);
		// console.log("empresa -> ",key,empresa);

		// Object.keys(oferta).forEach(function(key2){
		// 	console.log("campo oferta",key2,oferta[key2]);
		// });

		// Object.keys(oferta.company).forEach(function(key3){
		// 	console.log("campo empresa",key3,oferta.company[key3]);
		// });
	});	

	cargarCards();

	// obtenerEmpresas();

}



function obtenerEmpresas(){

	return fetch('https://andjobnow-cb1f.restdb.io/rest/empresas',
  	{
  		method: "GET",
		headers: {
			Accept: 'application/json',
    		'Content-Type': 'application/json',
	
 			'x-apikey': '4e6fc413ae5bf3d51267926be01bd4790c689'
		}
  	})
    .then((response) => {
    	return response.json();
    })
    .then((responseJson) => {
    	tratarEmpresas(responseJson);
    })
    .catch((error) =>{
      console.error(error);
    });

}



function tratarEmpresas(empresasJson){
	// console.log("cargarC",empresasJson);
	
	Object.keys(empresasJson).forEach(function(key) {

		let empresa = empresasJson[key];

		empresas[empresa["id_"]] = empresa;
		// empresas.push(empresa);

	});

	cargarCards();

}







function cargarCards(){


	// console.log("Empresas -> ",empresas);

	let n = 0;

	let arrOfferUserIds = {};
	if(cards.length>0){
		cards = [];
	}

	ofertas.forEach(function(oferta) {

		let src;

		let keyStr 				=	oferta["id"].toString();

		let cargo				=	oferta["profession"];
		let salario				=	oferta["min_salary"];
		let horario				=	oferta["workshift"];
		let descripcion			=	oferta["description"];
		let tiempoExp 			=	oferta["exp_time"];
		let estudiosReq			=	oferta["studies"];
		let idOferta			=	oferta["id"];
		let idEmpresa			=	oferta["company_id"];
		let duration			=	oferta["contract_duration"];

		let empresa				=	oferta.company;
		let nombreEmpresa		=	empresa["name_company"];
		// let b64Imagen			=	empresa["logo"];
		let actividad			=	getActividad(empresa["sector"]);
		let ubicacion			=	empresa["address"];
		let idUser				=	empresa.user_id;


		let cargoShort = cargo;
		if (cargoShort.length > 24) {
			cargoShort = cargoShort.substring(0,24)+"..";
		}

		let descripcionShort = descripcion;		
		if (descripcionShort.length > 300) {
			descripcionShort = descripcionShort.replace(/(\r\n\t|\n|\r\t)/gm,"");
			descripcionShort = descripcionShort.substring(0,294)+" [...]";
		}


		arrOfferUserIds[keyStr] = idUser;

		//globales.arrOfferUserIds = arrOfferUserIds;
		//globales.arrOfferUserIds2 = arrOfferUserIds;
		let imgEmpresa;

		if (empresa["logo"]){
			imgEmpresa = {uri:empresa["logo"]};
		}else{
			imgEmpresa = require('../img/noimg.png');
		}


		// Object.keys(empresa).forEach(function(key2){
		// 	console.log("empresa  campo ", key2,empresa[key2])
		// })


		// urlImagen = "https://andjobnow-cb1f.restdb.io/media/" + urlImagen;

		// console.log("oferta -> ",typeof(oferta),oferta);
		// console.log("empresa -> ",typeof(empresa),empresa);
		// console.log("b64Imagen -> ",b64Imagen);



		cards.push(
	    	<CardOferta
				idOferta={idOferta}
				idEmpresa={idEmpresa}
				imgEmpresa={imgEmpresa}
	    		nombreEmpresa={nombreEmpresa}
	    		cargoShort={cargoShort}
	    		cargo={cargo}
				salario={salario}
				horario={horario}
				descripcionShort={descripcionShort}
				descripcion={descripcion}
				tiempoExp={tiempoExp}
				estudiosReq={estudiosReq}
				actividad={actividad}
				ubicacion={ubicacion}
				duration={duration}
				idUser={idUser}
	    	/>
	    );

		
	    
	});



}




export function cargarCardsOfertas(sector){


	// obtenerOfertas();
	obtenerOfertasDos(sector);

	return cards
}








export class CardOferta extends React.Component{


	constructor (props) {
		super(props)
		this.state = {
			modalVisible: false,
			chatVisible: false,
		};

	}



	openModal() {
		this.setState({modalVisible:true});
	}

	closeModal() {
		this.setState({modalVisible:false});
	}



	openChat(propsOferta) {

		let idOferta = propsOferta.idOferta;
		let idUser = propsOferta.idUser;
		let status = globales.arrStatusCand[idOferta];

		// console.log(" idOferta ", idOferta)
		// console.log(" status ", status)
		// console.log(" idUser ", idUser)
		// console.log(" arrStatus ", globales.arrStatusCand)

		// Object.keys(propsOferta).forEach(function(clave) {
		// 	console.log("props offer ", clave,propsOferta[clave])
		// })


		if (status == 2 || status == 3){
			if (propsOferta.b64Imagen) {
				globales.otherAvatar = propsOferta.b64Imagen;
			}
			else{
				globales.otherAvatar = require('../img/default.png');
			}
			globales.otherUID = idUser;
			this.setState({chatVisible:true});	
		}else{
			alert(strings.canChat);
		}
	}




	closeChat() {
		this.setState({chatVisible:false});
	}

	render(){
		let oferta = this.props.oferta;
		let idOferta = this.props.idOferta;
		let idEmpresa = this.props.idEmpresa;
		let imgEmpresa = this.props.imgEmpresa;
		let nombreEmpresa = this.props.nombreEmpresa;
		let cargo = this.props.cargo
		let cargoShort = this.props.cargoShort
		let salario = this.props.salario;
		let salarioMax = this.props.salarioMax;
		let horario = this.props.horario;
		let descripcion = this.props.descripcion;
		let descripcionShort = this.props.descripcionShort;
		let tiempoExp = this.props.tiempoExp;
		let estudiosReq = this.props.estudiosReq;
		let actividad = this.props.actividad;
		let ubicacion = this.props.ubicacion;
		let idUser = this.props.idUser;
		let titulo = this.props.titulo;
		// console.log ("globales - ",globales);

		return (
			<View style={{flex:1,justifyContent:'space-between'}}>
				<View >
				{nombreEmpresa==='Oculto' ? 
					<Image blurRadius={30} source={imgEmpresa} style={styles.logoEmpresa} />
				:  
				<Image source={imgEmpresa} style={styles.logoEmpresa} />
				}
				
					<View style={styles.bloqueInfo}>
						<Text style={styles.textoEmpresa}>
							{nombreEmpresa==='Oculto' ? strings.hiddenCompany : nombreEmpresa}
						</Text>

						<Text style={styles.textoCargo}>
							{titulo}
						</Text>

						<View style={styles.bloqueSalario}>

							<Image source={require('../img/euro.png')} style={styles.imgEuro} />
							<Text style={styles.textoSalario}>
								{salario} {salarioMax ? "- "+salarioMax : null} salari brut mensual
							</Text>
						</View>
						
						<View style={styles.bloqueHorario}>

							<Image source={require('../img/reloj.png')} style={styles.imgReloj} />
							<Text style={styles.textoHorario}>
								{horario} h
							</Text>
						</View>

						<Text style={styles.tituloDescripcion}>{strings.description}</Text>
						<Text style={styles.textoDescripcion}>
							{descripcionShort}
						</Text>
					</View>
				</View>
				<View style={styles.botonDescripcion}>
					<TouchableOpacity onPress={() => this.openModal()}>
						<Text style={styles.modalDescripcion}>{strings.descriptionComplete}</Text>
					</TouchableOpacity>
				</View>

				<Modal
					visible={this.state.modalVisible}
					animationType={'slide'}
					onRequestClose={() => this.closeModal()}
					
					>
					<Descripcion 
						reloadOffers={() => this.props.reloadOffers()}
						oferta={oferta}
						idOferta={idOferta}
						idEmpresa={idEmpresa}
						imgEmpresa={imgEmpresa}
						nombreEmpresa={nombreEmpresa}
						actividad={actividad}
						ubicacion={ubicacion}
						cargo={cargo}
						tiempoExp={tiempoExp}
						estudiosReq={estudiosReq}
						descripcion={descripcion}
						titulo={titulo}
						idUser={idUser}
						candScene={false}
						closeModal={() => this.closeModal()} 
						openChat={() => this.openChat(this.props)} 
						closeChat={() => this.closeChat()} 
					/>


		        	
					
				</Modal>

				<Modal
					visible={this.state.chatVisible}
					animationType={'slide'}
					onRequestClose={() => this.closeChat()}
					
					>
					<Chat closeChat={() => this.closeChat()} />
					
				</Modal>

			</View>
		);
	}


}




const styles = StyleSheet.create({

	logoEmpresa:{
		marginTop: eH(10),
		width:'80%',
		height: eH(80),
		alignSelf: 'center',
		resizeMode: 'contain',
		// backgroundColor: 'red',
	},
	bloqueInfo:{
		marginTop:eH(6),
		alignItems:'center',
		// backgroundColor: 'blue',
		// height:200,
		// flex:1,
		// justifyContent:'flex-end',
	},
	textoEmpresa:{
		color:'#0E0E0E',
		fontFamily:'Montserrat-SemiBold',
		fontSize: eH(22),
		textAlign: 'center',
		// fontWeight: 'bold',
	},
	textoCargo:{
		color:'#0E0E0E',
		fontFamily:'Montserrat-Regular',
		fontSize: eH(21),
		textAlign: 'center',
	},
	bloqueSalario:{
		marginTop: eH(16),
		alignItems:'center',
		justifyContent: 'center',
		// flexWrap:'wrap',
		flexDirection:'row',
		width: '70%'
	},
	imgEuro:{
		height:eH(30),
		resizeMode:'contain',
		marginLeft:eH(-40),
	},
	textoSalario:{
		fontFamily:'Montserrat-Regular',
		fontSize: eH(16),
		color:'#0E0E0E',
		textAlign: 'center'
	},
	bloqueHorario:{
		// marginTop: eH(2),
		alignItems:'center',
		justifyContent: 'center',
		// flexWrap:'wrap',
		flexDirection:'row',
		width: '70%'
	},
	imgReloj:{
		height:eH(30),
		marginLeft:eH(-40),
		resizeMode:'contain',
	},
	textoHorario:{
		fontFamily:'Montserrat-Regular',
		fontSize: eH(14),
		color:'#0E0E0E',
		textAlign: 'center'
	},
	tituloDescripcion:{
		marginTop:eH(12),
		color:'#f39c10',
		fontFamily:'Montserrat-Medium',
		fontSize: eH(16),
		textAlign: 'center',
	},
	textoDescripcion:{
		color:'#6f6f6f',
		fontFamily:'Montserrat-Medium',
		fontSize: eH(13),
		textAlign: 'center',
		lineHeight: eH(12),
		padding:eH(10),
	},
	botonDescripcion:{
		borderTopColor: '#d2d2d2',
		borderTopWidth: 1,
		width:'90%',
		alignSelf:'center',
	},
	modalDescripcion:{
		paddingBottom:eH(16),
		paddingTop:eH(10),
		color:'#f39c10',
		fontFamily:'Montserrat-Medium',
		fontSize: eH(16),
		textAlign: 'center',
	},

	toast:{
		width:300,
		borderRadius:20,
		backgroundColor:'#FF2B19'
	},
	toastText:{
		textAlign:'center',
		color:'white',
	},
});
