import React, { Component } from 'react';
import Swiper from 'react-native-deck-swiper';
import { AsyncStorage, StyleSheet, Text, View, Image, Navigator, Animated, Dimensions, Modal, TouchableOpacity, TouchableHighlight, Alert, Platform, AppState } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { CardUno, CardDos, cargarCardsOfertas, CardOferta } from './Cards';
import { Descripcion } from './Descripcion';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';
import Chat from './Chat'

import { NavigationActions } from 'react-navigation';

import Spinner from 'react-native-loading-spinner-overlay';
import RNRestart from 'react-native-restart';

import Toast, { DURATION } from 'react-native-easy-toast'

import * as Animatable from 'react-native-animatable';
import { LoginManager } from 'react-native-fbsdk';

import RNPickerSelect from 'react-native-picker-select';

import { Button as ButtonBase, Icon as IconBase } from 'native-base';

import SoundPlayer from 'react-native-sound-player'

import FCM, {
	FCMEvent,
	NotificationType,
	WillPresentNotificationResult,
	RemoteNotificationResult
} from 'react-native-fcm';


let ofertas = [];
let empresas = [];
let cards = [];


function getDuration(id) {


	let duration = [];

	duration[1] = strings.duration1;
	duration[2] = strings.duration2;
	duration[3] = strings.duration3;


	return duration[id];


}


//let cardsOfertas = cargarCardsOfertas(null);



export class Ofertas extends Component {
	constructor(props) {
		super(props)
		this.inputRefs = {};
		this.state = {
			cards: [],
			// cardsOfertas,
			// [<CardUno logo="logo3"/>,<CardUno logo="logo4"/>,<CardUno logo="mcdonalds"/>,<CardUno logo="logo5"/>,<CardUno logo="logo3"/>,<CardUno logo="logo6"/>,<CardUno logo="logo5"/>,<CardUno logo="logo7"/>],
			swipedAllCards: false,
			swipeDirection: '',
			isSwipingBack: false,
			cardIndex: 0,
			chatVisible: false,
			loadingVisible: false,
			settingsModal: false,
			modalCallVisible: false,
			selectedFilter: 0,
			selectedFilter2: 0,
			page: 1,
			itemsFilter: [
				{
					"value": 0,
					"label": strings.all
				},
				{
					"value": 1,
					"label": strings.activity1
				},
				{
					"value": 2,
					"label": strings.activity2
				},
				{
					"value": 3,
					"label": strings.activity3
				},
				{
					"value": 4,
					"label": strings.activity4
				},
				{
					"value": 5,
					"label": strings.activity5
				},
				{
					"value": 6,
					"label": strings.activity6
				},
				{
					"value": 7,
					"label": strings.activity7
				},
				{
					"value": 8,
					"label": strings.activity8
				},
				{
					"value": 9,
					"label": strings.activity9
				},
				{
					"value": 10,
					"label": strings.activity10
				},
				{
					"value": 11,
					"label": strings.activity11
				},
				{
					"value": 12,
					"label": strings.activity12
				},
				{
					"value": 13,
					"label": strings.activity13
				},
				{
					"value": 14,
					"label": strings.activity14
				},
				{
					"value": 15,
					"label": strings.activity15
				},
				{
					"value": 16,
					"label": strings.activity16
				},
				{
					"value": 17,
					"label": strings.activity17
				},
				{
					"value": 18,
					"label": strings.activity18
				},
				{
					"value": 19,
					"label": strings.activity19
				},
				{
					"value": 20,
					"label": strings.activity20
				},
				{
					"value": 21,
					"label": strings.activity21
				},
				{
					"value": 22,
					"label": strings.activity22
				},
				{
					"value": 23,
					"label": strings.activity23
				}
			],
			itemsFilter2: [
				{
					"value": 0,
					"label": strings.all
				},
				{
					"value": 1,
					"label": strings.duration1
				},
				{
					"value": 2,
					"label": strings.duration2
				},
				{
					"value": 3,
					"label": strings.duration3
				}
			],
			cnoFilter: [
				{
					"value": 1,
					"label": strings.cno1
				},
				{
					"value": 2,
					"label": strings.cno2
				},
				{
					"value": 3,
					"label": strings.cno3
				},
				{
					"value": 4,
					"label": strings.cno4
				},
				{
					"value": 5,
					"label": strings.cno5
				},
				{
					"value": 6,
					"label": strings.cno6
				}
			],
			
			noti: null
		}
	}

	setModalCallVisible(visible) {
		this.setState({ modalVisible: visible });
	}

	openChat() {
		this.props.navigation.navigate('Candidaturas')
		/* let cardIndex = this.state.cardIndex;
		let propsOferta = this.swiper.props.cards[cardIndex].props;


		let idOferta = propsOferta.idOferta;
		let idUser = propsOferta.idUser;
		console.log('PARAMS', globales.arrStatusCand)
		let status = globales.arrStatusCand[idOferta];

		// console.log(" idOferta ", idOferta)


		// Object.keys(propsOferta).forEach(function(clave) {
		// 	console.log("props offer ", clave,propsOferta[clave])
		// })


		if (status == 2 || status == 3) {
			if (propsOferta.b64Imagen) {
				globales.otherAvatar = propsOferta.b64Imagen;
			}
			else {
				globales.otherAvatar = require('../img/default.png');
			}
			globales.otherUID = idUser;
			this.setState({ chatVisible: true });
		} else {
			this.refs.toastKo.show("Per poder chatejar necesites ser acceptat en el procés de selecció", 2000);
		} */
	}

	closeChat() {
		this.setState({ chatVisible: false });
	}


	applyOffer(propsOferta) {

		let dataBody = {
			"user": {
				"id": globales.UID
			},
			"offer": {
				"id": propsOferta.idOferta
			}
		}

		return fetch(globales.apiURL + '/api/offer/application-offer',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {

				if (responseJson["status"] == "success") {

					// this.swiper.swipeRight()
				}

				if (responseJson["message"].includes("correctamente")) {
					this.refs.toastOk.show(responseJson["message"], 2000);
					//this.actualizarCandidaturas(propsOferta)

				} else {
					this.refs.toastKo.show(responseJson["message"], 2000);

				}


			})
			.catch((error) => {
				console.error(error);
			});

	}
	declineOffer(propsOferta) {

		let dataBody = {
			"user": {
				"id": globales.UID
			},
			"offer": {
				"id": propsOferta.idOferta
			}
		}

		return fetch(globales.apiURL + '/api/offer/set-offer-declined',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {

				if (responseJson["status"] == "success") {
					console.log('Se declina bien')
					// this.swiper.swipeRight()
				}

				/* 				if (responseJson["message"].includes("correctamente")) {
									this.refs.toastOk.show(responseJson["message"], 2000);
				
				
								} else {
									this.refs.toastKo.show(responseJson["message"], 2000);
				
								} */


			})
			.catch((error) => {
				console.error(error);
			});

	}

	actualizarCandidaturas(propsOferta) {

		globales.arrStatusCand[propsOferta.idOferta] = 0;

		let offer = {
			key: propsOferta.idOferta.toString(),
			img: propsOferta.imgEmpresa,
			empresa: propsOferta.nombreEmpresa,
			puesto: propsOferta.cargo,
			salario: propsOferta.salario,
			jornada: propsOferta.horario,
			duration: getDuration(propsOferta.duration),

			status: strings.status0,
			statusColor: "#f39c10",

			idOferta: propsOferta.idOferta,
			idEmpresa: propsOferta.idEmpresa,
			actividad: propsOferta.actividad,
			ubication: propsOferta.ubicacion,
			tiempoExp: propsOferta.tiempoExp,
			estudiosReq: propsOferta.estudiosReq,
			descripcion: propsOferta.descripcion,
			idUser: propsOferta.idUser,

		}

		let offers = this.props.navigation.state.params ? this.props.navigation.state.params.datosCandidaturas : [];

		// Object.keys(offers).forEach(function(clave1) {
		// 	Object.keys(offers[clave1]).forEach(function(clave) {
		// 		console.log("campo offer navig",clave1, clave,offers[clave1][clave])
		// 	})
		// })

		let newDatos = offers.concat(offer);

		const setParamsAction = NavigationActions.setParams({
			params: { datos: newDatos },
			key: 'Candidaturas',
		});
		this.props.navigation.dispatch(setParamsAction);


		this.props.navigation.setParams({
			datosCandidaturas: newDatos,
		})

	}


	renderCard = (card) => {
		return (
			<View style={styles.card}>
				{card}
			</View>
		)
	};

	onSwipedAllCards = () => {
		this.setState({
			swipedAllCards: true
		})
	};

	onSwiped = (index) => {

		this.setState(prevState => {


			let newIndex = prevState.cardIndex + 1;
			console.log('SWIPED', newIndex)
			if (newIndex != 0) {

				if (newIndex % 7 == 0) {
					let page = this.state.page;
					this.setState({
						page
					})
					this.obtenerOfertasDos(null, null, page);
				}
			}


			return { cardIndex: newIndex }
		})

	}

	swipeBack = () => {
		if (!this.state.isSwipingBack) {
			this.setState({
				loadingVisible: true,
			});
			this.setIsSwipingBack(true, () => {
				this.swiper.swipeBack(() => {
					this.setState({
						loadingVisible: false,
					});
					this.setIsSwipingBack(false)
				})
			})
		}
	};

	setIsSwipingBack = (isSwipingBack, cb) => {
		this.setState(
			{
				isSwipingBack: isSwipingBack
			},
			cb
		)
	};

	swipeLeft = (swipedCardIndex) => {

		// Hacer some stuff para left
		console.log("ENtra en left")
		let propsOferta = this.swiper.props.cards[swipedCardIndex].props;
		this.declineOffer(propsOferta)

	};

	swipeRight = (swipedCardIndex) => {

		let cardIndex = this.state.cardIndex;
		let propsOferta = this.swiper.props.cards[swipedCardIndex].props;



		this.setState({
			cardIndex: swipedCardIndex
		})


		Alert.alert(
			strings.offer,
			strings.haveExp,
			[

				{ text: strings.no, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: strings.yes, onPress: () => this.applyOffer(propsOferta) },
			],
			{ cancelable: false }
		);


	};

	logoutAccept() {
		FCM.unsubscribeFromTopic('notifications_' + globales.UID);
		globales.UID = null;
		globales.token = '';
		globales.email = '';
		globales.apellidos = '';
		globales.nombre = '';
		//AsyncStorage.removeItem('tourjob');
		AsyncStorage.removeItem('jobToken');
		AsyncStorage.removeItem('jobUID');
		AsyncStorage.removeItem('jobNombre');
		AsyncStorage.removeItem('jobApellidos');
		AsyncStorage.removeItem('jobEmail');
		LoginManager.logOut();
		RNRestart.Restart()
	}
	logout() {


		// Works on both iOS and Android
		Alert.alert(
			strings.closeSession,
			strings.wantCloseSession,
			[
				{ text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: strings.closeSession, onPress: () => this.logoutAccept() },
			],
			{ cancelable: false }
		)


	}


	componentDidMount() {
		SoundPlayer.onFinishedPlaying((success) => { // success is true when the sound is played
			console.log('finished playing', success)
		})
		this.obtenerOfertasDos(null, null, 1);
		if (globales.UID) {
			this.startNotis(globales.UID);
		}
		FCM.requestPermissions({ badge: false, sound: true, alert: true })
		const willFocusSubscription = this.props.navigation.addListener(
			'willFocus',
			payload => {
			  console.debug('willFocus', payload);
			  this.setState({cards: []});
			  this.obtenerOfertasDos(null, null, 1);
			}
		  );
	}

	componentWillUnmount() {
		willFocusSubscription.remove();
		SoundPlayer.unmount()
	}

	saveFilter() {

		selected = this.state.selectedFilter > 0 ? this.state.selectedFilter : null;
		selected2 = this.state.selectedFilter2 > 0 ? this.state.selectedFilter2 : null;
		this.setState({
			cards: []
		})
		this.obtenerOfertasDos(selected, selected2, 1);
		this.setState({

			settingsModal: !this.state.settingsModal
		})




	}

	obtenerOfertasDos(sector, contract_duration, page) {

		console.log('Página', page)
		let filters = {
			"user": {
				"id": globales.UID,
			},
			page: page
		};
		console.log('Payload', filters)
		return fetch(globales.apiURL + '/api/offer/get-offers-not-declined',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(filters)
			})
			.then((response) => {

				return response.json();
			})
			.then((responseJson) => {
				console.log('OFERTAS', responseJson);
				this.setState({
					cards: [],
				});
				this.cargarCards(responseJson.offers);
			})
			.catch((error) => {
				console.error(error);
			});

	}

	cargarCards(ofertasJson) {

		let n = 0;


		let arrOfferUserIds = {};
		const _this = this;
		cards = this.state.cards;

		ofertasJson.map(function (oferta, index) {
			let actividades = [];

			actividades[1] = strings.activity1;
			actividades[2] = strings.activity2;
			actividades[3] = strings.activity3;
			actividades[4] = strings.activity4;
			actividades[5] = strings.activity5;
			actividades[6] = strings.activity6;
			actividades[7] = strings.activity7;
			actividades[8] = strings.activity8;
			actividades[9] = strings.activity9;
			actividades[10] = strings.activity10;
			actividades[11] = strings.activity11;
			actividades[12] = strings.activity12;
			actividades[13] = strings.activity13;
			actividades[14] = strings.activity14;
			actividades[15] = strings.activity15;
			actividades[16] = strings.activity16;
			actividades[17] = strings.activity17;
			actividades[18] = strings.activity18;
			actividades[19] = strings.activity19;
			actividades[20] = strings.activity20;
			actividades[21] = strings.activity21;
			actividades[22] = strings.activity22;
			actividades[23] = strings.activity23;

			let src;

			let keyStr = oferta["id"].toString();

			let cargo = oferta["profession"];
			let salario = oferta["min_salary"];
			let salarioMax = oferta["max_salary"];
			let horario = oferta["workshift"];
			let descripcion = oferta["description"];
			let tiempoExp = oferta["exp_time"];
			let estudiosReq = oferta["studies"];
			let idOferta = oferta["id"];
			let idEmpresa = oferta["company_id"];
			let duration = oferta["contract_duration"];
			let titulo = oferta["title"];
			let empresa = oferta.company;

			let nombreEmpresa = empresa["name_company"];
			let actividad = actividades[empresa["sector"]];
			let ubicacion = empresa["address"];
			let idUser = empresa.user_id;



			let cargoShort = cargo;
			if (cargoShort.length > 24) {
				cargoShort = cargoShort.substring(0, 24) + "..";
			}

			let descripcionShort = descripcion;
			if (descripcionShort.length > 200) {
				descripcionShort = descripcionShort.replace(/(\r\n\t|\n|\r\t)/gm, "");
				descripcionShort = descripcionShort.substring(0, 200) + " [...]";
			}

			//console.log('COMPANY', empresa)
			arrOfferUserIds[keyStr] = idUser;

			globales.arrOfferUserIds = arrOfferUserIds;

			let imgEmpresa;

			if (empresa["logo"]) {
				imgEmpresa = { uri: globales.imageUrl + empresa["logo"] };
			} else {
				imgEmpresa = require('../img/noimg.png');
			}


			// Object.keys(empresa).forEach(function(key2){
			// 	console.log("empresa  campo ", key2,empresa[key2])
			// })


			// urlImagen = "https://andjobnow-cb1f.restdb.io/media/" + urlImagen;

			// console.log("oferta -> ",typeof(oferta),oferta);
			// console.log("empresa -> ",typeof(empresa),empresa);
			// console.log("b64Imagen -> ",b64Imagen);
			let add = true;
			cards.includes();
			cards.forEach(card => {
				if (card.props.idOferta === idOferta && add) {
					add = false;
				}
			});

			if (add) {
				cards.push(
					<CardOferta
						reloadOffers={() => _this.obtenerOfertasDos(null, null, 1)}
						oferta={oferta}
						idOferta={idOferta}
						idEmpresa={idEmpresa}
						imgEmpresa={imgEmpresa}
						nombreEmpresa={nombreEmpresa}
						cargoShort={cargoShort}
						cargo={cargo}
						salario={salario}
						salarioMax={salarioMax}
						horario={horario}
						descripcionShort={descripcionShort}
						descripcion={descripcion}
						tiempoExp={tiempoExp}
						estudiosReq={estudiosReq}
						actividad={actividad}
						ubicacion={ubicacion}
						duration={duration}
						idUser={idUser}
						titulo={titulo}
						salarioMax={salarioMax}
					/>
				);
			}

		})

		console.log("CARDS", cards)
		//console.log('ARRAYS', globales.arrOfferUserIds)
		this.setState({
			cards: cards,
		});

	}



	render() {
		return (
			<Animatable.View ref={(r) => this.vista = r} style={styles.container}>

				<Spinner visible={this.state.loadingVisible} textStyle={{ color: '#FFF' }} />

				<View style={styles.cabecera}>

					<View style={{ flex: 0.2, }}>
						<TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }}
							onPress={() => { this.logout() }}>
							<Icon name='power'
								type='feather'
								color='#FFFFFF'
								size={eH(28)}
							/>
						</TouchableOpacity>
					</View>
					<Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(16) }}>{strings.newOpp}</Text>

					<View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'flex-end' }}>
						<TouchableOpacity style={{ alignItems: 'flex-end', paddingRight: eH(10) }}
							onPress={() => this.props.navigation.push('OfertasRechazadas')}>
							<Icon name='ban'
								type='font-awesome'
								color='#FFFFFF'
								size={eH(27)}
							/>
						</TouchableOpacity>
{/* 						<TouchableOpacity style={{ alignItems: 'flex-end', paddingRight: eH(10) }}
							onPress={() => this.setState({
								settingsModal: !this.state.settingsModal
							})}>
							<Icon name='filter'
								type='font-awesome'
								color='#FFFFFF'
								size={eH(27)}
							/>
						</TouchableOpacity> */}
					</View>

				</View>

				{this.state.cards.length > 0 ?
					<View style={styles.bloqueSwiper}>

						<Swiper
							//marginBottom={1}
							ref={swiper => {
								this.swiper = swiper
							}}
							backgroundColor={'#f1eaea'}
							onSwiped={this.onSwiped}
							//onTapCard={this.swipeRight}
							cards={this.state.cards}
							shouldComponentUpdate={true}
							cardIndex={0}
							cardVerticalMargin={eH(20)}
							renderCard={this.renderCard}
							onSwipedLeft={(swipedCardIndex) => this.swipeLeft(swipedCardIndex)}
							onSwipedRight={(swipedCardIndex) => this.swipeRight(swipedCardIndex)}
							onSwipedAll={this.onSwipedAllCards}
							//horizontalSwipe={false}
							verticalSwipe={false}
							showSecondCard={true}
							stackSize={3}
						>


						</Swiper>

						<View style={styles.bloqueAcciones}>
							<View style={styles.acciones}>
								<View style={[styles.boton, styles.botonExterior]}>
									<TouchableOpacity onPress={this.swipeBack} style={[styles.intBoton, styles.intBotonExterior]}>
										<Image source={require('../img/btnvolver.png')}
											style={[styles.imgBoton, styles.imgBotonExterior]} />

									</TouchableOpacity>
								</View>


								<View style={[styles.boton, styles.botonInterior]}>
									<TouchableOpacity onPress={() => this.swiper.swipeLeft()} style={[styles.intBoton, styles.intBotonInterior]}>
										<Image source={require('../img/btncruz.png')}
											style={[styles.imgBoton, styles.imgBotonCruz]} />
									</TouchableOpacity>

								</View>


								<View style={[styles.boton, styles.botonInterior]}>
									<TouchableOpacity onPress={() => this.swiper.swipeRight()} style={[styles.intBoton, styles.intBotonInterior]}>
										<Image source={require('../img/btncurriculum.png')}
											style={[styles.imgBoton, styles.imgBotonCurriculum]} />
									</TouchableOpacity>

								</View>


								<View style={[styles.boton, styles.botonExterior]}>
									<TouchableOpacity onPress={() => this.openChat()} style={[styles.intBoton, styles.intBotonExterior]}>
										<Image source={require('../img/btnmensaje.png')}
											style={[styles.imgBoton, styles.imgBotonExterior]} />
									</TouchableOpacity>

								</View>

							</View>

						</View>
					</View>
					:
					<View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center' }}>
						<Text style={{
							textAlign: 'center',
							color: 'grey',
							fontFamily: 'Montserrat-Medium',
							fontSize: eH(20)
						}}>{strings.noResults}</Text>
					</View>
				}

				<Toast
					ref="toastKo"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>

				<Toast
					ref="toastOk"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={[styles.toast, { backgroundColor: "#67bf09" }]}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>

				<Modal
					visible={this.state.chatVisible}
					animationType={'slide'}
					onRequestClose={() => this.closeChat()}

				>
					<Chat closeChat={() => this.closeChat()} />

				</Modal>

				<Modal
					visible={this.state.settingsModal}
					animationType={'slide'}
					onRequestClose={() => this.setState({
						settingsModal: !this.state.settingsModal
					})}

				>
					<View style={{ flex: 1, }}>
						<View style={styles.cabecera}>
							<TouchableOpacity style={styles.cancelarCabecera} onPress={() => this.setState({
								settingsModal: !this.state.settingsModal
							})}>
								<View style={styles.izqCabecera}>
									<Icon name='ios-arrow-back'
										type='ionicon'
										color='#FFFFFF'
										size={eH(35)}
									/>
								</View>
							</TouchableOpacity>
							<Text style={styles.textCabecera}>{strings.offersFilter}</Text>
							<TouchableOpacity style={styles.guardarCabecera} >

							</TouchableOpacity>
						</View>

						<View style={{ flex: 1, backgroundColor: '#ecedeb', padding: 20 }}>
							<Text>{strings.chooseFilter}</Text>
							<RNPickerSelect style={{ ...pickerSelectStyles }}
								placeholder={{
									label: strings.durationFilter,
									value: null,
								}}
								items={this.state.itemsFilter2}
								onValueChange={(value) => this.setState({
									selectedFilter2: value,
								})}
								onUpArrow={() => {
									this.inputRefs.name.focus();
								}}
								onDownArrow={() => {
									this.inputRefs.picker2.togglePicker();
								}}

								value={this.state.selectedFilter2}
								ref={(el) => {
									this.inputRefs.picker = el;
								}}

							/>
							<RNPickerSelect style={{ ...pickerSelectStyles }}
								placeholder={{
									label: strings.offerFilter,
									value: null,
								}}
								items={this.state.itemsFilter}
								onValueChange={(value) => this.setState({
									selectedFilter: value,
								})}
								onUpArrow={() => {
									this.inputRefs.name.focus();
								}}
								onDownArrow={() => {
									this.inputRefs.picker2.togglePicker();
								}}

								value={this.state.selectedFilter}
								ref={(el) => {
									this.inputRefs.picker = el;
								}}

							/>
							<Text style={{ paddingTop: 20 }}>{strings.duration}</Text>
							
							<View style={{ paddingVertical: eH(20) }}>
								<Button buttonStyle={styles.botonGuardar}
									textStyle={styles.textBotonGuardar}

									onPress={() => this.saveFilter()}
									text={strings.save}

									clear
								/>
							</View>
						</View>
						<Toast
							ref="toastKo"
							opacity={0.9}
							fadeInDuration={500}
							fadeOutDuration={800}
							style={styles.toast}
							textStyle={styles.toastText}
							positionValue={eH(200)}
						/>

						<Toast
							ref="toastOk"
							opacity={0.9}
							fadeInDuration={500}
							fadeOutDuration={800}
							style={[styles.toast, { backgroundColor: "#67bf09" }]}
							textStyle={styles.toastText}
							positionValue={eH(200)}
						/>
					</View>


				</Modal>


				<Modal
					visible={this.state.modalCallVisible}
					animationType={'fade'}
					onRequestClose={() => this.setState({
						modalCallVisible: !this.state.modalCallVisible
					})}

				>
					<View style={{
						flex: 1, backgroundColor: 'rgba(52, 52, 52, 1)', justifyContent: 'center',
						alignItems: 'center'
					}}>
						<View style={{
							width: '80%', height: '40%', backgroundColor: 'white', flexDirection: 'column',
							justifyContent: 'space-between',
							alignItems: 'center', borderRadius: 10
						}}>
							<Text style={{
								marginTop: 20, color: '#f39c10', fontFamily: 'Montserrat-Medium',
								fontSize: eH(20),
							}}>{strings.incomingCall}</Text>
							<Text style={{
								marginTop: 20, color: '#f39c10', fontFamily: 'Montserrat-Medium',
								fontSize: eH(25),
							}}>{this.state.noti ? this.state.noti.title : null}</Text>
							<View style={{
								justifyContent: 'flex-end',
								alignItems: 'center', flexDirection: 'row', marginBottom: 20
							}}>

								<TouchableOpacity style={{
									justifyContent: 'center',
									alignItems: 'center', alignSelf: 'flex-start', width: 50, height: 50, borderRadius: 25, backgroundColor: '#F43D3D', marginTop: 20, marginRight: 25
								}} onPress={() => this.closeCallModal()}>}>
							<IconBase ios='md-close' android="md-close" style={{ fontSize: 40, color: 'white', alignSelf: 'center', paddingTop: 5 }} />
								</TouchableOpacity>
								<TouchableOpacity style={{
									justifyContent: 'center',
									alignItems: 'center', alignSelf: 'flex-end', width: 50, height: 50, borderRadius: 35, backgroundColor: '#6CD30A', marginTop: 20, marginLeft: 25,
								}} onPress={() => this.goToCall()}>}>
							<IconBase ios='md-call' android="md-call" style={{ fontSize: 40, color: 'white', alignSelf: 'center', paddingTop: 5 }} />
								</TouchableOpacity>

							</View>

						</View>
					</View>


				</Modal>

			</Animatable.View>
		)
	}


	startNotis(userID) {

		FCM.createNotificationChannel({
			id: 'default',
			name: 'Default',
			description: 'used for example',
			priority: 'high'
		})


		/* 		FCM.getFCMToken()
				   .then(token => {
					   console.log('TOKEN FIREBASE',token)
				   });
		 */
		FCM.subscribeToTopic('notifications_' + userID);

		FCM.getInitialNotification().then((notif) => {
			if (notif && notif.opened_from_tray) {
				if (notif.custom_notification) {
					notiData = JSON.parse(notif.custom_notification);
					if (notiData.type == "call") {
						//this.props.navigation.navigate('Ofertas');
						SoundPlayer.playSoundFile('phonecall', 'mp3');
						this.setState({
							noti: notiData,
							modalCallVisible: true
						})


					}
				}
			}
		});
		this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
			if (notif.custom_notification) {
				notiData = JSON.parse(notif.custom_notification);
				console.log('NOTIFI2', notiData)

				if (notiData.type == "call") {
					this.props.navigation.navigate('Ofertas');
					SoundPlayer.playSoundFile('phonecall', 'mp3');
					this.setState({
						noti: notiData,
						modalCallVisible: true
					})

					//this.showAlert(notiData)
				}
			}
			/* 			if (notif.opened_from_tray && notiData.type == "call") {
							this.props.navigation.navigate('Ofertas');
							this.showAlert(notiData)
						} */
			//this.showAlert('Videllamada', 'Ecoders')
			if (AppState.currentState != 'active') {
				if (notif.local_notification) {
					FCM.presentLocalNotification({
						id: "UNIQ_ID_STRING",
						title: notif.aps.alert.title,
						body: notif.aps.alert.body,
						sound: "phonecall.mp3",
						priority: "high",
						click_action: "ACTION",
						icon: "ic_launcher",
						show_in_foreground: false,
					});

				}
			}


			if (Platform.OS === 'ios' && notif._notificationType === NotificationType.WillPresent && !notif.local_notification) {
				notif.finish(WillPresentNotificationResult.All)
				return;
			}

			if (Platform.OS === 'ios') {
				switch (notif._notificationType) {
					case NotificationType.Remote:
						notif.finish(RemoteNotificationResult.NewData)
						break;
					case NotificationType.NotificationResponse:
						notif.finish();
						break;
					case NotificationType.WillPresent:
						notif.finish(WillPresentNotificationResult.None)
						break;
				}
			}
			FCM.cancelAllLocalNotifications();
		});

		/* 		this.notificationListener2 = FCM.getInitialNotification().then(notif => {
					console.log('NOTIFI2', notif)
					//this.showAlert('Videllamada', 'Ecoders')
				}) */

	}



	goToCall() {
		SoundPlayer.stop();
		this.setState({
			modalCallVisible: false
		})
		this.props.navigation.push('Video', { noti: this.state.noti })

	}
	closeCallModal() {
		/* callSound.stop((success) => {
			if (!success) {
			  console.log('Sound did not play')
			}
		  }) */
		SoundPlayer.stop();
		this.setState({
			modalCallVisible: !this.state.modalCallVisible
		})
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		opacity: 1,
		// justifyContent:'flex-end',
		// backgroundColor: '#F5FCFF',
		// justifyContent: 'flex-end',
	},
	botonGuardar: {
		// textAlign:'center',
		backgroundColor: '#f39c10',
		borderRadius: 4,
		paddingHorizontal: eH(20),
		marginTop: eH(10),
		height: eH(54),
	},
	textBotonGuardar: {
		fontFamily: 'Montserrat-Bold',
		fontSize: eH(18),
		color: '#FFFFFF',
		fontWeight: '200',
	},
	bloqueSwiper: {
		flex: 1,
		// justifyContent:'flex-start',
		backgroundColor: '#F5FCFF',
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	swiper: {
		// flex: 1,
	},
	card: {
		flex: 0.68,
		borderRadius: 4,
		borderWidth: 2,
		borderColor: '#E8E8E8',
		backgroundColor: 'white'
	},
	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: eH(55),
		paddingHorizontal: 8,
		width: '100%',
	},
	textCabecera: {
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(20),
	},
	bloqueAcciones: {
		// flex:0.2,
		// alignSelf:'center',
		// flexDirection:'row',
		paddingBottom: 4,
		// backgroundColor:'red',
		justifyContent: 'flex-end',
		// alignItems:'flex-end',
	},
	acciones: {
		// flex:1,
		flexDirection: 'row',
		justifyContent: 'center',
	},
	boton: {
		backgroundColor: '#F5F5F5',
		alignItems: 'center',
		justifyContent: 'center',

	},
	botonExterior: {
		// backgroundColor:'red',
		width: eH(80),
		height: eH(80),
		borderRadius: eH(40),
		marginHorizontal: -4,
	},
	botonInterior: {
		// backgroundColor:'blue',
		width: eH(100),
		height: eH(100),
		borderRadius: eH(50),
		marginHorizontal: -4,
	},
	intBoton: {
		backgroundColor: '#FFFFFF',
		alignItems: 'center',
		justifyContent: 'center',

	},
	intBotonExterior: {
		width: eH(60),
		height: eH(60),
		borderRadius: eH(30),
		// marginHorizontal:-5,
	},
	intBotonInterior: {
		width: eH(80),
		height: eH(80),
		borderRadius: eH(40),
		// marginHorizontal:-6,
	},
	imgBoton: {
		resizeMode: 'contain',
	},
	imgBotonExterior: {
		width: eH(40),
	},
	imgBotonCruz: {
		width: eH(34),
	},
	imgBotonCurriculum: {
		width: eH(64),
	},
	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	},
})

const labels = {
	bottom: {
		title: 'BLEAH',
		style: {
			label: {
				backgroundColor: 'black',
				borderColor: 'black',
				color: 'white',
				borderWidth: 1
			},
			wrapper: {
				flexDirection: 'column',
				alignItems: 'center',
				justifyContent: 'center'
			}
		}
	},
	left: {
		title: 'NOPRE',
		style: {
			label: {
				backgroundColor: 'black',
				borderColor: 'black',
				color: 'white',
				borderWidth: 1
			},
			wrapper: {
				flexDirection: 'column',
				alignItems: 'flex-end',
				justifyContent: 'flex-start',
				marginTop: 30,
				marginLeft: -30
			}
		}
	},
	right: {
		title: 'LIKE',
		style: {
			label: {
				backgroundColor: 'black',
				borderColor: 'black',
				color: 'white',
				borderWidth: 1
			},
			wrapper: {
				flexDirection: 'column',
				alignItems: 'flex-start',
				justifyContent: 'flex-start',
				marginTop: 30,
				marginLeft: 30
			}
		}
	},
	top: {
		title: 'SUPER LIKE',
		style: {
			label: {
				backgroundColor: 'black',
				borderColor: 'black',
				color: 'white',
				borderWidth: 1
			},
			wrapper: {
				flexDirection: 'column',
				alignItems: 'center',
				justifyContent: 'center'
			}
		}
	},


}



const pickerSelectStyles = StyleSheet.create({
	inputIOS: {
		fontSize: 16,
		paddingTop: 13,
		paddingHorizontal: 10,
		paddingBottom: 12,
		borderWidth: 1,
		borderColor: 'gray',
		borderRadius: 4,
		backgroundColor: 'white',
		color: 'black',
	},
});