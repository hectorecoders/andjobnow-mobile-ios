import React, { Component } from 'react';
import {
	Animated,
	ScrollView,
	StyleSheet,
	Text,
	TextInput,
	View,
	Image,
	Linking
} from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
// import Toast from 'react-native-toast-native';
import Toast, {DURATION} from 'react-native-easy-toast'
import Spinner from 'react-native-loading-spinner-overlay';
import { globales, strings } from '../globales.js';
// import Icon from 'react-native-vector-icons/FontAwesome';

import * as Animatable from 'react-native-animatable';


export default class Recover extends Component {



	constructor (props) {
		super(props)

		this.state = {
			emailRecover: "",
			loadingVisible: false,
		}
	}






	cambioAtras = () => {
			this.vista.transition({translateY:0, opacity:1}, {translateY:-700,opacity:0}, 400);
		
			setTimeout( () => { this.props.returnPress()},350);
		}




	validarEmail(){

		
		

		let regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		
		let resultado;

		if (regExp.test(this.state.emailRecover)){
		    resultado = true;
		}else{
			resultado = strings.formatMail;
		}
		    
		return resultado

	}


	enviarEmail(){


		let validacion = this.validarEmail(this.state); 


		if (validacion === true){

			// console.log("valor this.state",this.state);

			
			this.setState({
				loadingVisible: true,
			});


			let recoverData = {
		    	email: this.state.emailRecover,
			};


			let data = JSON.stringify( recoverData ) ;
			


			return fetch(globales.apiURL+'/api/recoverypassword',
		  	{
		  		method: "POST",
				headers: {
					Accept: 'application/json',
		    		'Content-Type': 'application/json',
			
				},
				body: data,
		  	})
		    .then((response) => {
		    	return response.json();
		    })
		    .then((responseJson) => {
		    	

				this.setState({
					loadingVisible: false,
				});

				if (responseJson["state"] == "success"){
					this.refs.toastOk.show(responseJson["message"],2000);

				}else{
					
					this.refs.toastKo.show(responseJson["message"],2000);
				}

		    })
		    .catch((error) =>{
		      console.error(error);
		    });
		}else{
			this.refs.toastKo.show(validacion,2000);
		}

	}
								





	componentDidMount() {

		this.vista.transition({translateY:-500, opacity:0.5}, 
			{translateY:0,opacity:1}, 400)
    }

	render() {


		return (
			<Animatable.View ref={(r) => this.vista = r} style={{flex:1, backgroundColor:'#ededeb'}}>
				<Spinner visible={this.state.loadingVisible} textStyle={{color: '#FFF'}} />
				<View style={{
					backgroundColor: '#f39c10',
					height: eH(40),
				}}>
					<Text style={{
						color: '#FFFFFF',
						fontFamily:'Montserrat',
						fontSize: eH(18),
						alignSelf: 'center',
						// fontWeight: 'bold',
						lineHeight: eH(40),
					}}>
						{strings.recoverAccount}
					</Text>
				</View>

				<View style={{flex:1, padding: eH(20), justifyContent: 'space-between'
				}}>
						
					<View>
						<View>
							<Image source={require('../img/logogris.png')} style={{
								marginTop: eH(40),
								width:'80%',
								height: eH(100),
								alignSelf: 'center',
								resizeMode: 'contain',
								// backgroundColor: 'red',
							}} />
						</View>
						<View style={{
								marginTop:eH(40),
								// backgroundColor: 'blue',
								// flex:1,
							}}>

							<Text style={{
								alignSelf:'center', 
								color:'#6f6f6f',
								fontFamily:'Montserrat',
								fontSize: eH(16),
								textAlign: 'center',
								}}>
								{strings.insertMail}
							</Text>

							
							<Input inputStyle={ styles.input }
							containerStyle={ [ styles.containerInput,styles.containerInputPassword ] }
							placeholder='Email' placeholderTextColor="#FFFFFF"
							 underlineColorAndroid='transparent'
							leftIcon={<Icon 
								name='envelope-o' 
								size= {eH(16)} 
								type='font-awesome'
								color='#FFFFFF'
								iconStyle={{fontWeight:'200'}}
							/>}
							leftIconContainerStyle={styles.leftIcon}
							onChangeText={(emailRecover) => this.setState({emailRecover})}
							/>

							<Button buttonStyle={{
								// textAlign:'center',
								backgroundColor:'#f39c10',
								borderRadius: eH(40),
								paddingLeft: eH(20),
								paddingRight: eH(20),
								marginTop: eH(40),
								height: eH(46),
								}}
								textStyle={{
									fontFamily:'Montserrat-Regular',
									fontSize: 18,
									color: '#FFFFFF',
									width:'100%',

								}}
								onPress={() => this.enviarEmail(this.state)}
								text={strings.recoverMyAccount}
			
								clear
							/>
			
							<Text style={{
								alignSelf:'center', 
								color:'#6f6f6f',
								fontFamily:'Montserrat',
								fontSize: eH(16),
								marginTop: eH(20),
								textAlign: 'center',
								}}
								onPress={() => Linking.openURL("https://www.andjobnow.com/")}
								>
								{strings.needHelp}
							</Text>


						</View>
					</View>
					<View style={{
						// alignItems:'center',
					}}>

						
						<Text style={{
							alignSelf:'center', 
							color:'#6f6f6f',
							fontFamily:'Montserrat',
							fontSize: eH(16),
							}}
							onPress={this.cambioAtras}
							>
							{strings.alreadyRegister}
						</Text>

					</View>
				</View>
				<Toast 
					ref="toastKo"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(140)}
				/>

				<Toast 
					ref="toastOk"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={[styles.toast,{backgroundColor:"#67bf09"}]}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>
			
			</Animatable.View>
		)

	}

}



const toastStyle = {

	width: eH(330),
	height: eH(80),
	fontSize: 15,
	borderRadius: 15,

}



const styles = StyleSheet.create({
	input: {
		flex:1,
		borderColor: 'white',
		height:eH(46),
		fontFamily:'Montserrat',
		fontSize: eH(20),
		fontWeight: '300',
		color: '#FFFFFF',
		marginLeft: 0,
		paddingLeft: eH(20),
		// backgroundColor: 'red',
	},
	containerInput: {
		paddingLeft: eH(20),
		paddingRight: eH(20),
		borderRadius: eH(40),
		backgroundColor:'#689c15',
		width:'100%',
		height:eH(46),
		borderBottomWidth:0,
	},
	containerInputPassword:{
		backgroundColor:'#d2d2d2',
		marginTop:eH(20),
	},
	leftIcon:{
		// backgroundColor:'blue',
		marginLeft:0,
	},
	toast:{
		width:300,
		borderRadius:20,
		backgroundColor:'#FF2B19'
	},
	toastText:{
		textAlign:'center',
		color:'white',
	}
});