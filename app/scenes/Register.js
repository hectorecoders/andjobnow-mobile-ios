import React, { Component } from 'react';
import {
	ScrollView,
	StyleSheet,
	Text,
	TextInput,
	View,
	Image,
	Linking,
	Keyboard
} from 'react-native';
import Toast, {DURATION} from 'react-native-easy-toast'
import { Button, Input, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
// import Icon from 'react-native-vector-icons/FontAwesome';

import Spinner from 'react-native-loading-spinner-overlay';
import * as Animatable from 'react-native-animatable';
import { globales, strings } from '../globales.js';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
export default class Register extends Component {


	constructor (props) {
		super(props)

		this.state = {
			mail: "",
			pass: "",
			pass2: "",
			loadingVisible: false,
		}
	}



	desaparecer = () => this.vista.fadeOutDown(250).then( endState => 
		this.props.returnPress()	
	);


	enterApp = () => this.vista.fadeOutDown(250).then( endState => 
		this.props.userRegistered()
	);




	validarRegistro(estado){
		Keyboard.dismiss();
		

		let regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		
		let resultado;

		if (regExp.test(estado.mail)){
		    if (estado.pass.length > 0){
		    	if (estado.pass == estado.pass2){
		    		resultado = true;
		    	}else{
		    		resultado = strings.diffPass;
		    	}
		    }else{
		    	resultado = strings.refill;
		    }
		}else{
			resultado = strings.formatMail;
		}
		    
		return resultado
			
	}


	registrarUsuario(){



		let validacion = this.validarRegistro(this.state); 

		if (validacion === true){

			// console.log("valor this.state",this.state);

			
			this.setState({
				loadingVisible: true,
			});


			let registerData ={
				"role":0,
				"user":{
				  "email": this.state.mail,
				  "password": this.state.pass
				}
				  
			}
	

			// let data = new FormData();
			// data.append( "json", JSON.stringify( loginData ) );

			let data = JSON.stringify( registerData ) ;
		


			return fetch(globales.apiURL+'/api/register',
		  	{
		  		method: "POST",
				headers: {
					Accept: 'application/json',
		    		'Content-Type': 'application/json',
			
				},
				body: data,
		  	})
		    .then((response) => {
		    	return response.json();
		    })
		    .then((responseJson) => {
		    	

				this.setState({
					loadingVisible: false,
				});

				if (responseJson["status"] == "success"){
					// this.cambioLogin();
					this.inputEmail.clear();
					this.inputPass1.clear();
					this.inputPass2.clear();

					this.refs.toastOk.show(strings.correctRegister,2000);

					// this.enterApp();

				}else{
					this.refs.toastKo.show(responseJson["message"],2000);

				}


		    	// tratarEmpresas(responseJson);
		    })
		    .catch((error) =>{
		      console.error(error);
		    });
		}else{


			this.refs.toastKo.show(validacion,2000);

		}


	}


	responseFB = (error, result) => {
		if (error) {
			console.log('Error fetching data: ' + JSON.stringify(error));
		} else {
			console.log('Success fetching data: ',result);
			
			if (result.email){
				console.log('Email?: ',result.email);


				this.setState({
					loadingVisible: true,
				});


				 let registerData ={
					"name": result.name,
					"role":0,
					"email": result.email,
					"password": result.id,
					"fb_login":true
					
					  
				}


				let data = JSON.stringify( registerData ) ;
				


				return fetch(globales.apiURL+'/api/register',
					{
						method: "POST",
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',

						},
						body: data,
					})
				.then((response) => {
					return response.json();
				})
				.then((responseJson) => {
					console.log("resp json register",responseJson,responseJson["token"])

					this.setState({
						loadingVisible: false,
					});

					if (responseJson["status"] == "success"){

						this.refs.toastOk.show(strings.correctRegister,2000);
						// this.enterApp();

					}else{
						this.refs.toastKo.show(responseJson["message"],2000);

					}


				})
				.catch((error) =>{
					this.setState({
						loadingVisible: false,
					});
					console.error(error);
				}); 


			} else {
				alert(strings.facebookAlert)
			}
		}
	}



	registroFb(){

		const infoRequest = new GraphRequest(
			'/me?fields=id,name,email',
			null,
			this.responseFB,
		);

		LoginManager.logInWithReadPermissions(['public_profile','email']).then(
			function(result) {
				if (result.isCancelled) {
					console.log('Login was cancelled');
				} else {
					console.log('Login was successful with permissions: ' + result.grantedPermissions.toString());
					console.log(result);
					new GraphRequestManager().addRequest(infoRequest).start();
				}
			},
			function(error) {
				alert( strings.loginFail + error);
			}
		);


	}



	render() {
		return (
			<Animatable.View animation="fadeInDown" duration={750} ref={(r) => this.vista = r} style={{
				flex:1, 
				padding: eH(20), 
				backgroundColor: "#f39c10",
				justifyContent: 'space-between',
			}}>
				<Spinner visible={this.state.loadingVisible} textStyle={{color: '#FFF'}} />
				<View>
					<View>
						<Image source={require('../img/logo.png')} style={{
							marginTop: eH(20),
							width:'70%',
							height: eH(100),
							alignSelf: 'center',
							resizeMode: 'contain',
							// backgroundColor: 'red',
						}} />
					</View>
					<View style={{
							marginTop:eH(40),
							// backgroundColor: 'blue',
							// flex:1,
						}}>

						<Input inputStyle={ styles.input }
						containerStyle={ styles.containerInput }
						placeholder={strings.yourMail} 
						autoCapitalize='none'
						ref={(component) => {this.inputEmail = component}}
						placeholderTextColor="#FFFFFF"
						underlineColorAndroid='transparent'
						onChangeText={(val) => this.state.mail = val } 
						leftIcon={<Icon 
								name='envelope-o' 
								size= {eH(16)} 
								type='font-awesome'
								color='#FFFFFF'
								iconStyle={{fontWeight:'200'}}
							/>}
						leftIconContainerStyle={styles.leftIcon}
						/>
						<Input inputStyle={ styles.input }
						containerStyle={ [ styles.containerInput,styles.containerInputPassword ] }
						placeholder={strings.yourPass} placeholderTextColor="#FFFFFF"
						ref={(component) => {this.inputPass1 = component}}
						secureTextEntry={true} underlineColorAndroid='transparent'
						onChangeText={(val) => this.state.pass = val } 
						leftIcon={<Icon 
							name='lock' 
							size= {eH(22)} 
							type='font-awesome'
							color='#FFFFFF'
							iconStyle={{fontWeight:'200'}}
						/>}
						leftIconContainerStyle={styles.leftIcon}
						/>
						<Input inputStyle={ styles.input }
						containerStyle={ [ styles.containerInput,styles.containerInputPassword ] }
						placeholder={strings.repeatPass} placeholderTextColor="#FFFFFF"
						ref={(component) => {this.inputPass2 = component}}
						secureTextEntry={true} underlineColorAndroid='transparent'
						onChangeText={(val) => this.state.pass2 = val } 
						leftIcon={<Icon 
							name='lock' 
							size= {eH(22)} 
							type='font-awesome'
							color='#FFFFFF'
							iconStyle={{fontWeight:'200'}}
						/>}
						leftIconContainerStyle={styles.leftIcon}
						/>

						<Button buttonStyle={{
							// textAlign:'center',
							backgroundColor:'#f7bc5a',
							borderRadius: eH(40),
							paddingLeft: eH(20),
							paddingRight: eH(20),
							marginTop: eH(40),
							height: eH(46),
							}}
							textStyle={{
								fontFamily:'Montserrat-Regular',
								fontSize: eH(18),
								color: '#FFFFFF',
								width:'100%',
							}}
							onPress={() => this.registrarUsuario()}
							text={strings.joinAnd}
		
							clear
						/>
		

					</View>
				</View>
				<View style={{
					// alignItems:'center',
						paddingTop:eH(16),
						borderTopWidth: 1,
						borderTopColor: "#ffffff"
				}}>

					<Button buttonStyle={{
						// textAlign:'center',
						backgroundColor:'#3f5a9a',
						borderRadius: eH(40),
						paddingLeft: eH(20),
						paddingRight: eH(20),
						marginBottom: eH(6),
						height: eH(46),
						}}
						textStyle={{
							fontSize:eH(18),
							color: '#FFFFFF',
							fontFamily:'Montserrat-Regular',
							width:'94%',

						}}
						onPress={() => this.registroFb()}
						text={strings.registerFB}
	
						clear
						icon={<Icon 
							name='facebook-f' 
							size= {eH(22)} 
							type='font-awesome'
							color='#FFFFFF'
							iconStyle={{fontWeight:'200'}}
						/>}
						iconStyle={styles.leftIcon}
					/>

					<View style={{ //bloque terminos y condiciones
						marginBottom: eH(10)
					}}>


						<Text style={{
							alignSelf:'center', 
							color:'white',
							fontFamily:'Montserrat-Regular',
							fontSize: eH(12),
							}}
							>
							{strings.conditions1}  
						</Text>


						<Text style={{
							alignSelf:'center', 
							color:'#7ae8ff',
							fontFamily:'Montserrat-Semibold',
							fontSize: eH(12),
							}}
							onPress={() => Linking.openURL("https://www.andjobnow.com/us-del-servei")}
							>
							{strings.conditions2}  
						</Text>

					</View>
					
					<Text style={{
						alignSelf:'center', 
						color:'white',
						fontFamily:'Montserrat',
						fontSize: eH(16),
						paddingTop:eH(8),
						borderTopWidth: 0.5,
						borderTopColor: "#ffffff"
						}}
						onPress={this.desaparecer}
						>
						{strings.alreadyRegister}
					</Text>

				</View>
				<Toast 
					ref="toastKo"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>

				<Toast 
					ref="toastOk"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={[styles.toast,{backgroundColor:"#333333"}]}
					textStyle={styles.toastText}
					positionValue={eH(320)}
				/>
			</Animatable.View>
		)

	}

}

const styles = StyleSheet.create({
	input: {
		flex:1,
		borderColor: 'white',
		height:eH(46),
		fontFamily:'Montserrat',
		fontSize: eH(20),
		fontWeight: '300',
		color: '#FFFFFF',
		marginLeft: 0,
		paddingLeft: eH(20),
		// backgroundColor: 'red',
	},
	containerInput: {
		paddingLeft: eH(20),
		paddingRight: eH(20),
		borderRadius: eH(40),
		backgroundColor:'#689c15',
		width:'100%',
		height:eH(46),
		borderBottomWidth:0,
	},
	containerInputPassword:{
		marginTop:eH(20),
	},
	leftIcon:{
		// backgroundColor:'blue',
		marginLeft:0,
	},
	toast:{
		width:300,
		borderRadius:20,
		backgroundColor:'#FF2B19'
	},
	toastText:{
		textAlign:'center',
		color:'white',
	}
});