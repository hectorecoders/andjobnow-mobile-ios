import React from 'react';
import { Text, View, Image, StyleSheet, ScrollView, TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
import { eW, eH, eM } from '../Escalas';
import { globales, strings } from '../globales.js';
import { Item, Input as InputBase } from 'native-base';

import RNRestart from 'react-native-restart';
import { LoginManager } from 'react-native-fbsdk';
import FCM from 'react-native-fcm';
import Toast, { DURATION } from 'react-native-easy-toast'
import { ListItem, Radio, Right, Left, Tab, Tabs } from 'native-base';
import SlackChat from './Chat/SlackChat';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import { WebView } from "react-native-webview";

export class Configuracion extends React.Component {




	constructor(props) {
		super(props)
		this.state = {
			editOldPass: "",
			editNewPass: "",
			editNewPass2: "",
			language: globales.language,
			showSupport: false,
			deleteText: "",
			loadingVisible: false,
		}
	}


	changeLanguage(lang) {
		Alert.alert(
			strings.changeLanguage,
			strings.languageAdvice,
			[
			  {text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
			  {text: strings.change, onPress: () => 
				{	
					AsyncStorage.setItem('jobLanguage', lang);
			  		strings.setLanguage(lang);
			  		this.setState({language: lang});
			  		this.setState({});
					  RNRestart.Restart()}
				},
			],
			{ cancelable: false }
		  )

	}

	deleteUser() {
		if (this.state.deleteText != "") {
				
			Alert.alert(
				strings.deleteUser,
				strings.deleteUserInfo,
				[
				  {text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
				  {text: strings.yes, onPress: () => {
					let dataBody = {
						"applicant": {
							user_id: globales.UID
						},
						"text": this.state.deleteText
					}
					let data = JSON.stringify(dataBody);
					this.setState({loadingVisible: true});
					return fetch(globales.apiURL + '/api/applicant/unsubscribe',
						{
							method: "POST",
							headers: {
								Accept: 'application/json',
								'Content-Type': 'application/json',
								'Authorization': globales.token
							},
							body: data
	
						})
						.then((response) => {
							console.log("delete user response",response);
							return response.json();
						})
						.then((responseJson) => {
							//console.log("setnewpass responseJson",responseJson);
							this.setState({loadingVisible: false});
							if (responseJson["status"] == "success") {
								this.logoutAccept();
							} else {
	
							}
	
						})
						.catch((error) => {
							this.setState({loadingVisible: false});
							console.error(error);
						});
				  }},
				],
				{ cancelable: false }
			  )


			} else {
				this.setState({loadingVisible: false});
				alert(strings.allInfo);
			}
	}
	

	sendNewPass() {

		let oldPass = this.state.editOldPass;
		let newPass = this.state.editNewPass;
		let newPass2 = this.state.editNewPass2;


		if (oldPass && newPass && newPass2) {


			if (newPass == newPass2) {


				//console.log("token ",globales.token);

				let dataBody = {
					"user": {
						id: globales.UID,
						pass_old: oldPass,
						pass_new: newPass
					}
				}

				let data = JSON.stringify(dataBody);
				//console.log("send data setnewpass",typeof(data),data);


				return fetch(globales.apiURL + '/api/set-password',
					{
						method: "POST",
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							'Authorization': globales.token
						},
						body: data

					})
					.then((response) => {
						c//onsole.log("setnewpass response",response);
						return response.json();
					})
					.then((responseJson) => {
						//console.log("setnewpass responseJson",responseJson);

						if (responseJson["status"] == "success") {
							this.setState({
								editOldPass: "",
								editNewPass: "",
								editNewPass2: "",
							});

							this.refs.input1.clear();
							this.refs.input2.clear();
							this.refs.input3.clear();

							this.refs.toastOk.show(responseJson["message"], 2000);
						} else {
							this.refs.toastKo.show(responseJson["message"], 2000);
						}


					})
					.catch((error) => {
						console.error(error);
					});

			} else {

				this.refs.toastKo.show(strings.newPassIncorrect, 2000);

			}

		} else {
			this.refs.toastKo.show(strings.allInfo, 2000);
		}

	}

	logoutAccept() {
		FCM.unsubscribeFromTopic('notifications_' + globales.UID);
		globales.UID = null;
		globales.token = '';
		globales.email = '';
		globales.apellidos = '';
		globales.nombre = '';
		//AsyncStorage.removeItem('tourjob');
		AsyncStorage.removeItem('jobToken');
		AsyncStorage.removeItem('jobUID');
		AsyncStorage.removeItem('jobNombre');
		AsyncStorage.removeItem('jobApellidos');
		AsyncStorage.removeItem('jobEmail');
		LoginManager.logOut();
		RNRestart.Restart()
	}

	logout() {


		// Works on both iOS and Android
		Alert.alert(
			strings.closeSession,
			strings.wantCloseSession,
			[
				{ text: strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: strings.closeSession, onPress: () => this.logoutAccept() },
			],
			{ cancelable: false }
		)


	}


	changuedTab(i){
		if(i===3){
			this.setState({showSupport: true})
		}
	}

	render() {
		return (
			<View style={{ flex: 1, }}>
							<Spinner visible={this.state.loadingVisible} textStyle={{ color: '#FFF' }} />

				<View style={styles.cabecera}>

					<View style={{ flex: 0.2, }}>
						<TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }}
							onPress={() => { this.logout() }}>
							<Icon name='power'
								type='feather'
								color='#FFFFFF'
								size={eH(28)}
							/>
						</TouchableOpacity>
					</View>
					<Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(20) }}>{strings.configuration}</Text>

					<View style={{ flex: 0.2 }}>
					</View>

				</View>

				<View style={{ flex: 1, backgroundColor: '#ecedeb' }}>

					<Tabs  tabBarUnderlineStyle={{backgroundColor:'#f39c10'}} style={{backgroundColor: '#ecedeb'}} onChangeTab={({ i }) => this.changuedTab(i)}>
					<Tab textStyle={styles.textTab} activeTextStyle={styles.textTabActive} heading={strings.language}>
						<ListItem onPress={() => this.changeLanguage('ca')}>
							<Left>
							<Text style={styles.languageText}>{strings.catalan}</Text>
							</Left>
							<Right>
							<Radio selected={this.state.language=='ca'} selectedColor='#f39c10'/>
							</Right>
						</ListItem>
						<ListItem onPress={() => this.changeLanguage('es')} >
							<Left>
							<Text style={styles.languageText}>{strings.spanish}</Text>
							</Left>
							<Right>
							<Radio selected={this.state.language=='es'} selectedColor='#f39c10'/>
							</Right>
						</ListItem>
						</Tab>
						
						<Tab textStyle={styles.textTab} activeTextStyle={styles.textTabActive} heading={strings.security}>
						<KeyboardAwareScrollView style={{padding: 20}} extraScrollHeight={150}>
						<Text style={styles.privacityTitle}>{strings.changePassword}</Text>
						<View >
								<View style={styles.filaInfo}>
									<Input
										ref="input1"
										inputStyle={styles.input}
										containerStyle={styles.containerInput}
										maxLength={30}
										placeholder={strings.actualPass} placeholderTextColor="#adadad"
										secureTextEntry={true} underlineColorAndroid='transparent'
										onChangeText={(val) => this.state.editOldPass = val}
									/>
								</View>
								<View style={styles.filaInfo}>
									<Input
										ref="input2"
										inputStyle={styles.input}
										containerStyle={styles.containerInput}
										maxLength={30}
										placeholder={strings.newPass} placeholderTextColor="#adadad"
										secureTextEntry={true} underlineColorAndroid='transparent'
										onChangeText={(val) => this.state.editNewPass = val}
									/>
								</View>
								<View style={styles.filaInfo}>
									<Input
										ref="input3"
										inputStyle={styles.input}
										containerStyle={styles.containerInput}
										maxLength={30}
										placeholder={strings.repeatNewPass} placeholderTextColor="#adadad"
										secureTextEntry={true} underlineColorAndroid='transparent'
										onChangeText={(val) => this.state.editNewPass2 = val}
									/>
								</View>
							</View>
							<View style={{ paddingVertical: eH(20) }}>
								<Button buttonStyle={styles.botonGuardar}
									textStyle={styles.textBotonGuardar}
									onPress={() => this.sendNewPass()}
									text={strings.save}
									clear
								/>
							</View>

							<Text style={styles.privacityTitle}>{strings.dropOut}</Text>
<Item stackedLabel style={{ width: '100%', borderWidth: 1, borderColor: 'grey', borderRadius: 8 }}>
	<InputBase style={styles.privacityDelete} placeholder={strings.textDeleteUser} value={this.state.deleteText} multiline={true} numberOfLines={3} maxLength={200} onChangeText={(deleteText) => this.setState({ deleteText })} />
</Item>
<View style={{ paddingVertical: eH(20) }}>
								<Button buttonStyle={[styles.botonGuardar, {backgroundColor: 'red'}]}
									textStyle={styles.textBotonGuardar}
									onPress={() => this.deleteUser()}
									text={strings.deleteUser}
									clear
								/>
							</View>
<Text style={styles.privacityTitle}></Text>
<Text style={styles.privacityTitle}></Text>
</KeyboardAwareScrollView >
						</Tab>
						<Tab textStyle={styles.textTab} activeTextStyle={styles.textTabActive} heading={strings.privacity}>
						<KeyboardAwareScrollView style={{padding: 20}} extraScrollHeight={150}>
							<Text style={styles.privacityTitle}>1.-Qui som.</Text>
							<Text style={styles.privacityText}>La Web-app "ANDJOBNOW" N.R.T.: L-713275-M, és un Portal especialitzat a gestionar l'ocupació i formació, En processos de selecció i tota sèrie d'activitats relacionades amb l'ocupació.
Inscripció de l'usuari de forma gratuïta en la nostra web-app a través del procés d'alta de candidats i acceptació de les presents condicions. Per a això, l'usuari podrà aportar tota la informació rellevant per optimitzar la cerca d'ocupació o formació.
ANDJOBNOW posarà a la disposició dels usuaris serveis i eines per a la gestió de la cerca d'ocupació.
L'usuari podrà seleccionar i determinar el nivell de privadesa del CV.
Ser buscat per les empreses demandants d'ocupació (d'acord amb el nivell de privadesa seleccionat) a través de la plataforma de ANDJOBNOW.
Servei d'Alertes, d'ocupació i formació, butlletins, comunicacions i informació d'interès (ANDJOBNOW, posa a la disposició dels seus usuaris mecanismes mitjançant els quals els candidats que ho sol·licitin puguin excloure's d'aquest servei)
Altres serveis que puguin crear-se i/o que ANDJOBNOW consideri d'interès per als seus usuaris com a formació, xarxa de contactes professionals i/o fòrums.
L'usuari podrà seguir a ANDJOBNOW a les xarxes socials en les quals estigui present.</Text>
<Text style={styles.privacityTitle}>2.- Usuaris</Text>
<Text style={styles.privacityText}>L'accés i/o ús d'aquesta pàgina Web-app atribueix a qui ho realitza la condició d'usuari, acceptant, des d'aquest mateix moment, plenament i sense reserva alguna, les presents Condicions Generals així com les Condicions Particulars que, si escau, complementin, modifiquin o substitueixin les Condicions Generals en relació amb determinats serveis i continguts d'aquesta pàgina Web-app. L'acceptació de les presents Condicions Generals és vinculant per als Usuaris / Candidats, que hauran de complir amb l'Avís Legal de la Web i amb les presents Condicions Legals.</Text>
<Text style={styles.privacityTitle}>3.- Ús de la pàgina Web-app, els seus serveis i continguts</Text>
<Text style={styles.privacityText}>L'usuari es compromet a utilitzar la Web i els seus serveis i continguts sense contravenir la legislació vigent, la bona fe, els usos generalment acceptats i l'ordre públic. Així mateix, queda prohibit l'ús de la Web-app amb finalitats il·lícites o lesius contra ANDJOBNOW o qualsevol tercer, o que, de qualsevol forma, puguin causar perjudici o impedir el normal funcionament del Web-app.
Respecte dels continguts (informacions, textos, gràfics, arxius de so i/o imatge, fotografies, dissenys, etc.), es prohibeix:
La seva reproducció, distribució o modificació, tret que es compti amb l'autorització dels seus legítims titulars o resulti legalment permès.
Qualsevol vulneració dels drets de ANDJOBNOW o dels seus legítims titulars sobre els mateixos.
La seva utilització per a tot tipus de finalitats comercials o publicitaris, diferents dels estrictament permesos.
Qualsevol intent d'obtenir els continguts del Web-app per qualsevol mitjà diferent dels quals es posin a la disposició dels usuaris així com dels quals habitualment s'emprin a la xarxa, sempre que no causin perjudici algun a aquesta Web-app.</Text>
<Text style={styles.privacityTitle}>4.- Modificació unilateral</Text>
<Text style={styles.privacityText}>ANDJOBNOW podrà modificar unilateralment i sense previ avís, sempre que ho consideri oportú, l'estructura i disseny del Web-app, així com modificar o eliminar, els serveis, els continguts i les condicions d'accés i/o ús del Web-app, incloent les condicions generals del Servei, les polítiques.</Text>
<Text style={styles.privacityTitle}>5.- Protecció de dades de caràcter personal</Text>
<Text style={styles.privacitySubtitle}>5.1.- Política de Privadesa</Text>
<Text style={styles.privacityText}>Finalitat: Les dades facilitades pels usuaris seran tractats per procedir a la tramitació de tot el relacionat amb la cerca d'ocupació, la participació en processos de selecció, la presentació de candidatures a ofertes laborals, la gestió de tots els serveis enumerats en el punt 1 de les presents condicions. Per fer efectiu el Servei d'Alertes d'ocupació i formació , així com l'enviament de tot tipus d'informació d'interès, per mitjà d'accions de permission e-mail màrqueting i màrqueting directe, s'elaboraran perfils, segmentacions sobre les dades. Aquestes comunicacions es remetran, per mitjans de comunicació electrònica o no, i podran versar sobre diversos sectors com a formació, selecció, Oci, Outlets, Financer, Editorial, Educació, Automoció, ONG, Telecomunicacions, Consultoria, Energia, Medicina, Tèxtil, Llar, Higiene, Salut, Bellesa, Cura Personal, Mobiliari, Alimentació, Col·leccionisme, Música, Vídeos, Passatemps, Material d'Oficina, Informàtica, Tecnologia, Viatges, Joieria, Drogueria i Neteja, Assegurances.
Consentiment de l'usuari: L'usuari consent a l'accepta les presents condicions legals del ANDJOBNOW. L'emplenament del formulari i el registre suposa consentiment, per part de l'usuari, a tots els tractaments de les dades d'acord amb les condicions de privadesa d'ANDJOBNOW definides en aquest text.
Les dades recaptades són adequats, pertinents i no excessius en relació amb l'àmbit, les finalitats determinades i explícites que ANDJOBNOW manifesta. No obstant això l'anterior, aquest consentiment podrà ser revocat a qualsevol moment per l'afectat, donant-se de baixa en els serveis de la Web-app
Compliment de les mesures de seguretat: El fitxer que emmagatzema les dades dels usuaris garanteix les mesures d'índole tècnica i organitzativa per prevenir la integritat i seguretat de la informació personal aportada d'acord amb l'estat actual de la tècnica. Protecció de dades de caràcter personal i ha establert tots els mitjans tècnics al seu abast per evitar la pèrdua, malament ús, alteració, accés no autoritzat i robatori de les dades que l'usuari faciliti, sense perjudici d'informar als usuaris que les mesures de seguretat en Internet no són inexpugnables.
Obligacions de l'Usuari. Per la seva banda, l'Usuari es compromet a complir amb les següents condicions: L'Usuari respon de la veracitat i exactitud de les dades facilitades i es compromet a mantenir-los degudament actualitzats. L'Usuari serà responsable dels danys i perjudicis que ANDJOBNOW, les societats vinculades a ANDJOBNOW i/o tercers poguessin sofrir com a conseqüència de la falta de veracitat, inexactitud, falta de vigència i autenticitat de les dades facilitades. Els serveis oferts per ANDJOBNOW estan dirigits a majors de 18 anys que puguin incorporar-se al mercat laboral. En tot cas, l'Usuari garanteix ser major de 18 anys i no estar impedit per prestar el seu consentiment</Text>
<Text style={styles.privacitySubtitle}>5.2.-Selecció de la Privadesa en el perfil d'Usuari / Candidat</Text>
<Text style={styles.privacityText}>L'usuari haurà de seleccionar entre els següents nivells de privadesa oferts per ANDJOBNOW; l'usuari podrà modificar, a qualsevol moment, el nivell de privadesa a la seva àrea de candidat: Alt: L'Usuari / Candidat autoritza la visualització del currículum i dades personals identificatives exclusivament a ANDJOBNOW i a les empreses a les ofertes de les quals s'hagi inscrit. D'aquesta manera, l'empresa oferent podrà visualitzar el currículum actualitzat del candidat. Estàndard:
L'Usuari / Candidat autoritza que el currículum sigui visible per ANDJOBNOW, i totes les empreses registrades a la Web-app. L'usuari podrà seleccionar a tot moment les dades personals identificatives que desitja estiguin accessibles davant una possible cerca efectuada per les oferents d'ocupació.</Text>
<Text style={styles.privacitySubtitle}>5.3.- Consentiment per a la comunicació de les dades personals</Text>
<Text style={styles.privacityText}>En emplenar el primer formulari de registre l'usuari manifesta haver llegit i accepta les condicions legals del servei en les quals s'inclou la política de privadesa, atorgant així el seu consentiment inequívoc al tractament de les seves dades personals a ANDJOBNOW conforme a les finalitats informades, així com la cessió de les seves dades personals a tercers (les empreses oferents d'ocupació que utilitzen els serveis de ANDJOBNOW per a la cerca i selecció de personal), per al compliment del servei; comunicació de dades que respectarà a tot moment el nivell de privadesa seleccionat pels usuaris.
L'usuari podrà revocar el seu consentiment o manifestar negativa al tractament de les seves dades.</Text>
<Text style={styles.privacityTitle}>6.- Cookies</Text>
<Text style={styles.privacityText}>Tota la informació sobre l'ús que fa El Site de cookies està recollida i detallada en la Política de Cookies.</Text>
<Text style={styles.privacityTitle}>7.- Exclusió de garanties i responsabilitat</Text>
<Text style={styles.privacityText}>ANDJOBNOW no atorga cap garantia ni es fa responsable, en cap cas, dels danys i perjudicis de qualsevol naturalesa que poguessin portar causa de:
La mancada disponibilitat, manteniment i efectiu funcionament de la Web-app i/o dels seus serveis o continguts.
La falta d'utilitat, adequació o validesa de la Web-app i/o dels seus serveis o continguts per satisfer necessitats, activitats o resultats concrets o expectatives dels usuaris.
La recepció, obtenció, emmagatzematge, difusió o transmissió, per part dels usuaris, dels continguts.
L'ús il·lícit, negligent, fraudulent, contrari a les presents Condicions Generals, a la bona fe, als usos generalment acceptats o a l'ordre públic, de la Web-app, els seus serveis o continguts, per part dels usuaris.
La falta de licitud, qualitat, fiabilitat, utilitat i disponibilitat dels serveis prestats per tercers i llocs a la disposició dels usuaris a la Web-app.
L'incompliment per part de tercers de les seves obligacions o compromisos en relació amb els serveis prestats als usuaris a través del Web-app.
De conformitat amb la Llei 15/2003, del 18 de desembre, qualificada de protecció de dades personals, se l’informa, en qualitat d’usuari, que les dades facilitades passaran a formar part del fitxer responsabilitat d’ANDJOBNOW, registrat a l’Agència Andorrana de Protecció de Dades. Les seves dades s’utilitzaran per comunicar-li qualsevol iniciativa amb finalitat promocional i publicitària mitjançant qualsevol canal d’informació. Igualment les seves dades podran ser cedides a empreses terceres que col·laboren amb ANDJOBNOW, amb la mateixa finalitat.
Com a usuari vostè haurà de respondre, en qualsevol cas, de la veracitat de les dades facilitades i de comunicar qualsevol modificació de les mateixes, quedant ANDJOBNOW exempta de qualsevol responsabilitat al respecte.
Així mateix, se l’informa que pot exercir els drets d’accés, rectificació, oposició i supressió de les seves dades, així com el dret de revocar el consentiment prestat per a la cessió de les dades. Aquests drets poden ser exercits per l’usuari mitjançant sol·licitud dirigida a l’adreça andjob@outlook.es amb indicació del seu nom i cognoms, domicili a efectes de notificacions, fotocòpia del document nacional d’identitat o passaport i esmentant el contingut concret del dret exercit.</Text>
<Text style={styles.privacityTitle}>8.- Durada</Text>
<Text style={styles.privacityText}>La durada de la prestació del servei de la Web-app i dels serveis és de caràcter indefinit.
Sense perjudici de l'anterior, ANDJOBNOW es reserva el dret per, interrompre, suspendre o acabar la prestació del servei de la Web-app o de qualsevol dels serveis que ho integren, en els mateixos termes que es recullen en la condició quarta.</Text>
<Text style={styles.privacityTitle}>9.- Legislació aplicable i Jurisdicció</Text>
<Text style={styles.privacityText}>Les presents Condicions Generals es regiran per la legislació andorrana. ANDJOBNOW i l'usuari, amb renúncia expressa a qualsevol altre fur que pogués correspondre'ls, se sotmeten a la Jurisdicció dels Jutjats i Tribunals de Andorra per quantes qüestions poguessin suscitar-se o accions exercitar-se derivades de la prestació del servei de la Web-app i dels seus serveis i continguts i sobre la interpretació, aplicació, compliment o incompliment de l'aquí establert. En el cas que l'Usuari tingui el seu domicili fora d'Andorra, ANDJOBNOW i l'Usuari, amb renúncia expressa a qualsevol altre fur que pogués correspondre'ls, se sotmeten a la Jurisdicció dels Jutjats i Tribunals de Andorra.</Text>
				
						
		
<Text style={styles.privacityTitle}></Text>

						</KeyboardAwareScrollView>
						</Tab>
						<Tab textStyle={styles.textTab} activeTextStyle={styles.textTabActive} heading={strings.support}>
						<WebView 
						originWhitelist={['*']}
						startInLoadingState={true}
						source={{ uri: 'https://landbot.io/u/H-126661-2I05664HLWRMHLW7/index.html' }} />
							
						</Tab>
					</Tabs>
				</View>
				<Toast
					ref="toastKo"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>

				<Toast
					ref="toastOk"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={[styles.toast, { backgroundColor: "#67bf09" }]}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>

			</View>
		);
	}


}





const styles = StyleSheet.create({

	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: eH(55),
		paddingHorizontal: 8,
		width: '100%',
	},
	textCabecera: {
		flex: 0.6,
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(20),
	},
	textTab: {
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(10),
	},
	textTabActive: {
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(10),
		color:'#f39c10'
	},
	languageText: {
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(16),
	},
	cancelarCabecera: {
		textAlign: 'left',
		paddingLeft: 10,
		flex: 0.2,
		color: 'white',
		fontSize: eH(20),

	},
	guardarCabecera: {
		textAlign: 'right',
		paddingRight: 10,
		flex: 0.2,
		color: 'white',
		fontSize: eH(20),

	},
	campos: {
		marginTop: eH(50),
	},
	filaInfo: {
		backgroundColor: '#FFFFFF',
		paddingHorizontal: eH(10),
		paddingVertical: eH(6),
		borderTopWidth: 0.5,
		borderTopColor: '#adadad',
	},
	colIzda: {
		color: '#adadad',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16)
	},
	colDcha: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
	},

	botonGuardar: {
		// textAlign:'center',
		backgroundColor: '#f39c10',
		borderRadius: 4,
		paddingHorizontal: eH(20),
		marginTop: eH(10),
		height: eH(54),
	},
	textBotonGuardar: {
		fontFamily: 'Montserrat-Bold',
		fontSize: eH(18),
		color: '#FFFFFF',
	},
	input: {
		// flex:1,
		// borderColor: 'white',
		// height:eH(46),
		color: '#6f6f6f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(16),
		// fontWeight: '300',
		// marginLeft: 0,
		// paddingLeft: eH(20),
		// backgroundColor: 'red',
	},
	privacityTitle: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(18),
		marginVertical: 10
	},
	privacityDelete: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(12),
		borderWidth: 1,
		borderColor: '#6f6f6f',
		borderRadius: 8
	},
	privacitySubtitle: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(14),
		marginVertical: 5
	},
	privacityText: {
		color: '#6f6f6f',
		fontFamily: 'Montserrat-Regular',
		fontSize: eH(11),
	},
	containerInput: {
		// paddingLeft: eH(20),
		// paddingRight: eH(20),
		// borderRadius: eH(40),
		// backgroundColor:'#689c15',
		// width:'100%',
		// height:eH(46),
		borderBottomWidth: 0,
	},
	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	}
});
