import React, { Component } from 'react';
import {
	View,
	StyleSheet, Dimensions
} from 'react-native';
import { GiftedChat, Bubble, Send, MessageText } from 'react-native-gifted-chat';
import { eW, eH, eM } from '../../Escalas';
import { globales, strings } from '../../globales.js';


export default class SlackChat extends Component {

	constructor(props) {
		super(props)
		this.state = {
			messages: [],
			channelName: '',
			token: 'xoxp-472736181458-472564792180-475467887270-77f1d1fb7779006909f3ea5512b35253',
			channelID: '',
			supportID: 'UDWGLPA5A',
		}
	}

	tratarMensajes(messages) {
		let newMessages =[];
		messages.map((message, index) => {
			if(message.subtype==="channel_join"){
				newMessages.push({
					_id: index,
					text: strings.chatSupport,
					createdAt: new Date(message.ts*1000),
					user: {
					  _id: 2,
					  name: 'AndJobNow',
					  avatar: require('../../img/logochat.png'),
					},
				  })
			} else {
				if(message.user===this.state.supportID){
					newMessages.push({
						_id: index,
						text: message.text,
						createdAt: new Date(message.ts*1000),
						user: {
						  _id: 2,
						  name: 'AndJobNow',
						  avatar: require('../../img/logochat.png'),
						},
					  })
				} else {
					newMessages.push({
						_id: index,
						text: message.text,
						createdAt: new Date(message.ts*1000),
						user: {
						  _id: 1,
						  name: 'Uno',
						},
					  })
				}
				
			}
		})

		this.setState({messages: newMessages});
	}

	channelHistory(){
		let completeURL = 'https://slack.com/api/channels.history?token='+this.state.token+'&channel='+this.state.channelID
		return fetch(completeURL,
	  	{
	  		method: "GET",
			headers: {
				Accept: 'application/json',
	    		'Content-Type': 'application/x-www-form-urlencoded',
			}
			
	  	})
	    .then((response) => {
	    	return response.json();
	    })
	    .then((responseJson) => {
			if(responseJson.ok){
				this.tratarMensajes(responseJson.messages)
			}
	    })
	    .catch((error) =>{
	      console.error(error);
	    });
	}

	sendMessageSlack(message){
			let dataBody = {
				'channel': this.state.channelName,
				'text': message,
				'as_user': 'false',
				'username': this.state.channelName
			}

		let data = JSON.stringify( dataBody ) ;
		let tokenSlack = "Bearer " +this.state.token;
		return fetch('https://slack.com/api/chat.postMessage',
	  	{
	  		method: "POST",
			headers: {
				Accept: 'application/json',
	    		'Content-Type': 'application/json',
	    		'Authorization': tokenSlack
			},
			body: data
			
	  	})
	    .then((response) => {
	    	return response.json();
	    })
	    .then((responseJson) => {
			//console.log('SlackSendMessage', responseJson)
			if(responseJson.ok){
				this.channelHistory();
			}
	    })
	    .catch((error) =>{
	      console.error(error);
	    });
	
	}
	joinChannel(){
		let created = false;
		let dataBody = [];
		if(this.state.channelName===''){
			let channelName = globales.nombre+globales.apellidos+globales.UID;
			this.setState({channelName})
			dataBody = {
				'name': channelName
			}
		} else {
			created = true;
			dataBody = {
				'name': this.state.channelName
			}
		}
		let data = JSON.stringify( dataBody ) ;
		let tokenSlack = "Bearer " +this.state.token;
		return fetch('https://slack.com/api/channels.join',
	  	{
	  		method: "POST",
			headers: {
				Accept: 'application/json',
	    		'Content-Type': 'application/json',
	    		'Authorization': tokenSlack
			},
			body: data
			
	  	})
	    .then((response) => {
	    	return response.json();
	    })
	    .then((responseJson) => {
			if(responseJson.ok){
				if(!created){
					this.setChannelName()
				}
				 this.setState({
					channelID: responseJson.channel.id, 
					supportID: responseJson.channel.members[0]}, () => {
						
						this.channelHistory();
						this.interval = setInterval(() => this.channelHistory(), 5000);
				});
			}
	    })
	    .catch((error) =>{
	      console.error(error);
	    });
	
	}
	setChannelName(){
		let dataBody = {
			user: {
                id: globales.UID
			},
			slack: {
                channel: this.state.channelName
            }
		}
		let data = JSON.stringify( dataBody ) ;
		return fetch(globales.apiURL+'/api/slack/set-channelname',
	  	{
	  		method: "POST",
			headers: {
				Accept: 'application/json',
	    		'Content-Type': 'application/json',
	    		'Authorization': globales.token
			},
			body: data
			
	  	})
	    .then((response) => {
	    	return response.json();
	    })
	    .then((responseJson) => {
			
	    })
	    .catch((error) =>{
	      console.error(error);
	    });
	}
	getChannelName(){
		let dataBody = {
			user: {
                id: globales.UID
            }
		}
		let data = JSON.stringify( dataBody ) ;
		return fetch(globales.apiURL+'/api/slack/get-channelname',
	  	{
	  		method: "POST",
			headers: {
				Accept: 'application/json',
	    		'Content-Type': 'application/json',
	    		'Authorization': globales.token
			},
			body: data
			
	  	})
	    .then((response) => {
	    	return response.json();
	    })
	    .then((responseJson) => {
			 if(responseJson.status==="success"){
				 if(responseJson.slack){
					this.setState({
						channelName: responseJson.slack.channel_name}, () => {
							this.joinChannel();
						});
				 } else {
					this.joinChannel();
				 }
				
			} 
	    })
	    .catch((error) =>{
	      console.error(error);
	    });
	}
	componentDidMount(){
		this.getChannelName();
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	  }

	onSend(messages) {
		console.log('Mensaje enviado',messages[0].text)
		this.sendMessageSlack(messages[0].text);
	}

	setCustomText = (text) => {
		this.setState({ text: text })
	}

	renderBubble(props) {
		return (
			<Bubble
				{...props}
				textStyle={{
					left: {
						fontFamily: 'Montserrat-Regular',
					},
					right: {
						fontFamily: 'Montserrat-Regular',
					}
				}}
				wrapperStyle={{
					left: {
						backgroundColor: '#dfdee4',
						marginVertical: 2,
					},
					right: {
						backgroundColor: '#2299ff',
						marginVertical: 2,
					}
				}}
			/>
		);
	}

	renderMessageText(props) {
		return (
			<View>
				<MessageText
					{...props}
				/>}
				
			</View>
		);
	}

	renderSend(props) {
		return (
			<Send
				{...props}
				label={strings.send}
			>
			</Send>
		);
	}

	render() {
		return (
			<View style={{ flex: 1, backgroundColor: '#F2EAEA' }}>
				<GiftedChat
					messages={this.state.messages}
					onSend={messages => this.onSend(messages)}
					user={{
						_id: 1
					}}
					bottomOffset={eH(60)}
					onLongPress={(ctx, currentMessage) => { console.log("currentMessage", currentMessage) }}
					placeholder={strings.writeYourMessage}
					showUserAvatar={true}
					maxInputLength={100}
					renderBubble={this.renderBubble}
					renderSend={this.renderSend}
					renderMessageText={this.renderMessageText}

				/>
			</View>
		)
	}
}



const styles = StyleSheet.create({

	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 8,
		width: '100%',
	},
	textCabecera: {
		flex: 0.7,
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(20),
	},
	cancelarCabecera: {
		flex: 0.15,

	},
	izqCabecera: {
		alignItems: 'flex-start',
		paddingLeft: 10,
	},
	bloqueCabecera: {
		flex: 0.15,

	},
	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	},

});