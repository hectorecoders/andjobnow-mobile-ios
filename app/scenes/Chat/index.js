import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Text,
	TextInput,
	StyleSheet,
	Image,
	TouchableOpacity,
	KeyboardAvoidingView, Alert, Linking
} from 'react-native';
import { GiftedChat, Bubble, Send, MessageText } from 'react-native-gifted-chat';
import firebase from 'react-native-firebase';
import { Icon } from 'react-native-elements';
import { eW, eH, eM } from '../../Escalas';
import { globales, strings } from '../../globales.js';

import Toast, { DURATION } from 'react-native-easy-toast';
import { Colors, Styles } from './Shared';

import ImagePicker from 'react-native-image-picker';

import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import * as mime from 'react-native-mime-types';
const uuidV4 = require('uuid/v4');

// import TextField from './Components/TextField';
// import Button from './Components/Button';
// import Separator from './Components/Separator';


// Prepare Blob support





var dbRef = firebase.database().ref();
var refChats = dbRef.child("chats");
var refUsers = dbRef.child("users");
// var refChatAct = refChats.child("-LEQyRAelUU-Lqq8eN41");
var refChatAct;

let primero = true;


let userUno;
let userDos;

export default class Chat extends Component {


	constructor(props) {
		super(props)
/* 		const Blob = RNFetchBlob.polyfill.Blob;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob; */
		this.state = {
			messages: [],
			// userUno: globales.chatUserUno,
			// userDos: globales.chatUserDos
		}

	}










	cargarChat() {


		refUsers.child(globales.UID + "/chats/" + globales.otherUID).once("value")
			.then((dataSS) => {


				var valor = dataSS.val();
				//console.log("valor", valor);

				if (valor) {
					refChatAct = refChats.child(valor);
					this.cargarMensajes();

				} else {
					refChatAct = refChats.push();
					refUsers.child(globales.UID + "/chats/" + globales.otherUID).set(refChatAct.key);
					refUsers.child(globales.otherUID + "/chats/" + globales.UID).set(refChatAct.key);
				}


			});


	}


	cargarMensajes() {


		let arrMessages = [];

		refChatAct.once("value")
			.then((snapshot) => {
				snapshot.forEach(function (childSnapshot) {
					var key = childSnapshot.key;
					var childData = childSnapshot.val();
					// console.log("ChSS ",key,childData);

					let avatar;

					if (childData.user["_id"] == globales.UID) {
						avatar = globales.avatar;
					} else {
						
						avatar = globales.otherAvatar;
					}
					//console.log("childata", childData);
					//console.log("ChSS id avatar", childData.user["_id"], avatar);
					//console.log("Key", key);
					childData.user.avatar = avatar;
					childData.user._id = ''+childData.user._id+'';

					arrMessages.unshift(childData);
				});
				//console.log("arrmensajes", arrMessages);
				this.setState({
					messages: arrMessages
				})

				refChatAct.limitToLast(1).on("child_added", (childSS) => {
					if (primero) {
						primero = false;
					} else {
						this.nuevoMensajeActivo(childSS);
					}
				});
			});

	}


	nuevoMensajeActivo(dataSS) {
		//console.log("nuevoMensajeActivo dataSS", dataSS);

		var mensaje = dataSS.val();

		if (mensaje.user["_id"] == globales.otherUID) {

			this.setState(previousState => ({
				messages: GiftedChat.append(previousState.messages, mensaje)
			}))
		}
	}


	componentDidMount(){
		refUsers.child(globales.UID + "/chats/" + globales.otherUID).once("value")
			.then((dataSS) => {
				var valor = dataSS.val();
				firebase.database().ref().child("chats").child(valor).limitToLast(1).on('child_added', function(childSnapshot) {
					 
					var snap = childSnapshot.val();
					if(snap.user._id!=globales.UID){
					snap.read=true;
					var adaNameRef = firebase.database().ref(childSnapshot.ref.path);

					adaNameRef.update({ read: true});
					
					}
					//console.log("Last messag", snap);
			   });
			}
			)
		
	}


	componentWillMount() {
		userUno = {
			_id: ''+globales.UID+'',
			name: 'Uno',
			avatar: globales.avatar
		}
		userDos = {
			_id: ''+globales.otherUID+'',
			name: 'Dos'
		}
		console.log("globales.otherUID", globales.otherUID)
		this.cargarChat();

	}

	onSend(messages = []) {
		//console.log("mess send", messages);
		var objSend = messages[0];
		refChatAct.push(objSend);
		// console.log("r ch",refChats);
		this.setState(previousState => ({
			messages: GiftedChat.append(previousState.messages, messages),
		}))
	}

	setCustomText = (text) => {


		this.setState({ text: text })
	}

	renderBubble(props) {
		return (
			<Bubble
				{...props}
				textStyle={{
					left: {
						fontFamily: 'Montserrat-Regular',
					},
					right: {
						fontFamily: 'Montserrat-Regular',
					}
				}}
				wrapperStyle={{
					left: {
						backgroundColor: '#dfdee4',
						marginVertical: 2,
					},
					right: {
						backgroundColor: '#2299ff',
						marginVertical: 2,
					}
				}}
			/>
		);
	}

	renderMessageText(props) {
		//console.log('props', props)
		let myUser = props.currentMessage.user._id == globales.UID;
		return (
			<View>
				
				{props.currentMessage.hasOwnProperty('file') ?

				<TouchableOpacity {...props} style={{padding:10}} onPress={() => 
					Linking.canOpenURL(props.currentMessage.fileUrl).then(supported => {
						if (supported) {
						  Linking.openURL(props.currentMessage.fileUrl);
						} else {
						  console.log("Don't know how to open URI: " + props.currentMessage.fileUrl);
						}
					  })}
				>
				<Icon name='md-attach'
								type='ionicon'
								color= {myUser ? 'white' : 'black'}
								size={30}
							/>
					<Text style={{color: myUser ? 'white' : 'black'}}>{props.currentMessage.fileName}</Text>
					
				</TouchableOpacity>
				: 
				props.currentMessage.hasOwnProperty('statusCall')?

				<View  style={{padding:10}} >
<Icon name='md-videocam'
								type='ionicon'
								color= {myUser ? 'white' : 'black'}
								size={30}
							/>
					<Text style={{color: myUser ? 'white' : 'black'}}>{strings.videoCallFinished}</Text>
					</View>

				:
				<MessageText
					{...props}
				/>}
				
			</View>
		);
	}

	renderSend(props) {
		return (
			<Send
				{...props}
				label={strings.send}
			>
			</Send>
		);
	}



	uploadFile() {
		
		var timeStamp = Math.floor(Date.now() / 1000);
		var options = {
			            title: 'Actualitza la teva foto',
			            cancelButtonTitle: 'Cancel·lar',
			            takePhotoButtonTitle: 'Fer una foto',
			            chooseFromLibraryButtonTitle: 'Escollir una foto',
			        };

			DocumentPicker.show({
				filetype: [DocumentPickerUtil.allFiles()],
			  },(error,res) => {
				// Android

				let mimeType = mime.lookup(res.uri);
				if(!mime){
					console.log('Tipo de archivo no reconocido', mimeType)
					alert(strings.wrongFile);
				} else {
					/* console.log('MYME', mimeType)
					console.log('URI', res.uri)
					console.log('type', mimeType)
					console.log('fileName', res.fileName)
					console.log('fileSize', res.fileSize) */
					firebase.storage().ref('files').child(timeStamp)
					.put(res.uri, { contentType: mimeType}) //--> here just pass a uri
					.then((snapshot) => {
						let url = snapshot.downloadURL;
	
						let message = [{
							_id: uuidV4(),
							text: ".",
							file: true,
							fileUrl: snapshot.downloadURL,
							fileName: res.fileName,
							createdAt: new Date().toISOString(),
							user: {
								_id: globales.UID
							}
						}];
						//console.log('Mensaje', snapshot);
						this.onSend(message);
					})
				}
				
			  });		
		/* 	ImagePicker.showImagePicker(options, (response) => {
				if (response.didCancel) {
				  console.log('User cancelled photo picker');
				}
				else if (response.error) {
				  console.log('ImagePicker Error: '+response.error);
				}
				else if (response.customButton) {
				  console.log('User tapped custom button: '+response.customButton);
				}
				else {
					let fileName= response.fileName;
				  firebase.storage().ref('files').child(timeStamp)
				  .put(response.uri, { contentType: 'image/jpeg'}) //--> here just pass a uri
				  .then((snapshot) => {
					  let url = snapshot.downloadURL;

					  let message = [{
						  _id: timeStamp,
						  text: url,
						  file: true,
						  fileUrl: snapshot.downloadURL,
						  fileName: fileName,
						  createdAt: new Date().toISOString(),
						  user: {
							  _id: globales.UID
						  }
					  }];
					  console.log('Mensaje', snapshot);
					  this.onSend(message);
				  })

				}}) */
			  


	}

	render() {
		return (
			<View style={{ flex: 1, backgroundColor: '#F2EAEA' }}>
				<View style={styles.cabecera}>
					<TouchableOpacity style={styles.cancelarCabecera} onPress={this.props.closeChat}>
						<View style={styles.izqCabecera}>
							<Icon name='ios-arrow-back'
								type='ionicon'
								color='#FFFFFF'
								size={eH(35)}
							/>
						</View>
					</TouchableOpacity>
					<Text style={styles.textCabecera}>Chat</Text>
					<View style={styles.bloqueCabecera} />
				</View>
				<GiftedChat
					messages={this.state.messages}
					onSend={messages => this.onSend(messages)}
					user={userUno}
					//onInputTextChanged={(text) => this.setCustomText(text)}
					//textInputProps={{}}
					bottomOffset={eH(60)}
					onLongPress={(ctx, currentMessage) => { console.log("currentMessage", currentMessage) }}
					placeholder={strings.writeYourMessage}
					showUserAvatar
					maxInputLength={100}
					renderBubble={this.renderBubble}
					renderSend={this.renderSend}
					renderMessageText={this.renderMessageText}

				/>
				<TouchableOpacity style={{
					bottom: eH(56),
					left: eH(12),
					// width:eH(40),
					// height:eH(40),
					// justifyContent: 'flex-end'
					position: 'absolute',
				}} onPress={() => this.uploadFile()}
				>
					<Image source={require('../../img/attach.png')}
						style={{
							resizeMode: 'contain',
							width: eH(36),
							height: eH(36),
						}}
					/>
				</TouchableOpacity>

				<Toast
					ref="toastKo"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={styles.toast}
					textStyle={styles.toastText}
					positionValue={eH(200)}
				/>

				<Toast
					ref="toastOk"
					opacity={0.9}
					fadeInDuration={500}
					fadeOutDuration={800}
					style={[styles.toast, { backgroundColor: "#67bf09" }]}
					textStyle={styles.toastText}
					positionValue={eH(600)}
				/>
			</View>

		)
	}

}



const styles = StyleSheet.create({

	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 8,
		width: '100%',
	},
	textCabecera: {
		flex: 0.7,
		textAlign: 'center',
		color: 'white',
		fontFamily: 'Montserrat-SemiBold',
		fontSize: eH(20),
	},
	cancelarCabecera: {
		flex: 0.15,

	},
	izqCabecera: {
		alignItems: 'flex-start',
		paddingLeft: 10,
	},
	bloqueCabecera: {
		flex: 0.15,

	},
	toast: {
		width: 300,
		borderRadius: 20,
		backgroundColor: '#FF2B19'
	},
	toastText: {
		textAlign: 'center',
		color: 'white',
	},

});