import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView, Image, TouchableOpacity, findNodeHandle, ActivityIndicator, TextInput, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { ViewPager } from 'rn-viewpager';
import { Form, Item, Input, Label, Card, CardItem, Body, StyleProvider, Button, Icon } from 'native-base';
import StepIndicator from 'react-native-step-indicator';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { eW, eH, eM } from '../Escalas';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import DatePicker from 'react-native-datepicker';
import RNPickerSelect from 'react-native-picker-select';
import { CheckBox, Icon as NewIcon } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { globales, strings } from '../globales.js';
import ImagePicker from 'react-native-image-picker';
import Modal from "react-native-modal";
import Spinner from 'react-native-loading-spinner-overlay';

var expKeys = 0;
var eduKeys = 0;
var timeZoneOffsetInHours = (-1) * (new Date()).getTimezoneOffset() / 60;

const secondIndicatorStyles = {
	stepIndicatorSize: 30,
	currentStepIndicatorSize: 40,
	separatorStrokeWidth: 2,
	currentStepStrokeWidth: 3,
	stepStrokeCurrentColor: '#689c15',
	stepStrokeWidth: 3,
	stepStrokeFinishedColor: '#689c15',
	stepStrokeUnFinishedColor: '#AECC7F',
	separatorFinishedColor: '#689c15',
	separatorUnFinishedColor: '#AECC7F',
	stepIndicatorFinishedColor: '#689c15',
	stepIndicatorUnFinishedColor: '#ffffff',
	stepIndicatorCurrentColor: '#ffffff',
	stepIndicatorLabelFontSize: 13,
	currentStepIndicatorLabelFontSize: 13,
	stepIndicatorLabelCurrentColor: '#689c15',
	stepIndicatorLabelFinishedColor: '#ffffff',
	stepIndicatorLabelUnFinishedColor: '#AECC7F',
	labelColor: 'white',
	labelSize: 13,
	currentStepLabelColor: '#689c15'
}

function totalString(val) {
	if (val) {
		val = val.toString();
	} else {
		val = "";
	}
	return val;
}


function getDate(timeStamp) {
	var date = new Date(timeStamp * 1000);
	var dia = date.getDate();
	var mes = (date.getMonth() + 1);
	var year = date.getFullYear();
	return year + "/" + mes + "/" + dia;

}

const getStepIndicatorIconConfig = ({ position, stepStatus }) => {
	const iconConfig = {
		name: 'feed',
		color: stepStatus === 'finished' ? '#ffffff' : '#F39C12',
		size: 15,
	};
	switch (position) {
		case 0: {
			iconConfig.name = 'person';
			break;
		}
		case 1: {
			iconConfig.name = 'description';
			break;
		}
		case 2: {
			iconConfig.name = 'work';
			break;
		}
		case 3: {
			iconConfig.name = 'school';
			break;
		}
		case 4: {
			iconConfig.name = 'language';
			break;
		}
		case 5: {
			iconConfig.name = 'photo-camera';
			break;
		}
		default: {
			break;
		}
	}
	return iconConfig;
};

export default class Wizard extends Component {

	constructor() {
		super();
		this.inputRefs = {};
		this.state = {
			pages: [{ key: 0, title: strings.personalInfo }, { key: 1, title: strings.description }, { key: 2, title: strings.workExp }, { key: 3, title: strings.formation }, { key: 4, title: strings.languages }, { key: 5, title: strings.picture }],
			currentPage: 0,
			itemsSex: [{ label: strings.man, value: 1 }, { label: strings.woman, value: 2 }],
			itemsResident: [{ label: strings.yes, value: 0 }, { label: strings.no, value: 1 }],
			itemsCountries: countries,
			description: '',
			hasExp: true,
			hasEdu: true,
			experiences: [],
			education: [],
			selectedItems: [],
			sector: null,
			cno: null,
			sector2: null,
			cno2: null,
			sector3: null,
			cno3: null,
			languageSelected: null,
			levelSelected: null,
			languagesSelected: null,
			levelsSelected: null,
			contract: null,
			image: null,
			b64Img: null,
			page0Loaded: false,
			page1Loaded: false,
			page2Loaded: false,
			page3Loaded: false,
			page4Loaded: false,
			page5Loaded: false,
			isModalVisible: false,
			loadingVisible: false,
			modalType: '',
			modalIndex: null,
			showButtons: true,
			descriptionAux: '',
			languages: [{
						"value": 1,
						"label": strings.catalan
					},
					{
						"value": 2,
						"label": strings.spanish
					},
					{
						"value": 3,
						"label": strings.french
					},
					{
						"value": 4,
						"label": strings.english
					},
					{
						"value": 5,
						"label": strings.russian
					},
					{
						"value": 6,
						"label": strings.portuguese
					},
					{
						"value": 7,
						"label": strings.others
					}],
			sectors: [
				{
					"value": 1,
					"label": strings.activity1
				},
				{
					"value": 2,
					"label": strings.activity2
				},
				{
					"value": 3,
					"label": strings.activity3
				},
				{
					"value": 4,
					"label": strings.activity4
				},
				{
					"value": 5,
					"label": strings.activity5
				},
				{
					"value": 6,
					"label": strings.activity6
				},
				{
					"value": 7,
					"label": strings.activity7
				},
				{
					"value": 8,
					"label": strings.activity8
				},
				{
					"value": 9,
					"label": strings.activity9
				},
				{
					"value": 10,
					"label": strings.activity10
				},
				{
					"value": 11,
					"label": strings.activity11
				},
				{
					"value": 12,
					"label": strings.activity12
				},
				{
					"value": 13,
					"label": strings.activity13
				},
				{
					"value": 14,
					"label": strings.activity14
				},
				{
					"value": 15,
					"label": strings.activity15
				},
				{
					"value": 16,
					"label": strings.activity16
				},
				{
					"value": 17,
					"label": strings.activity17
				},
				{
					"value": 18,
					"label": strings.activity18
				},
				{
					"value": 19,
					"label": strings.activity19
				},
				{
					"value": 20,
					"label": strings.activity20
				},
				{
					"value": 21,
					"label": strings.activity21
				},
				{
					"value": 22,
					"label": strings.activity22
				},
				{
					"value": 23,
					"label": strings.activity23
				}
			],
			contracts: [
				{
					"value": 1,
					"label": strings.duration1
				},
				{
					"value": 2,
					"label": strings.duration2
				},
				{
					"value": 3,
					"label": strings.duration3
				}
			],
			cnos: [
				{
					"value": 1,
					"label": strings.cno1
				},
				{
					"value": 2,
					"label": strings.cno2
				},
				{
					"value": 3,
					"label": strings.cno3
				},
				{
					"value": 4,
					"label": strings.cno4
				},
				{
					"value": 5,
					"label": strings.cno5
				},
				{
					"value": 6,
					"label": strings.cno6
				}
			],
			level: [
				{
					"value": 1,
					"label": strings.level1
				},
				{
					"value": 2,
					"label": strings.level2
				},
				{
					"value": 3,
					"label": strings.level3
				},
				{
					"value": 4,
					"label": strings.level4
				},
				{
					"value": 5,
					"label": strings.level5
				}
			],
			textInputValue: '',
			
		}
		strings.setLanguage(globales.language);
		this.setState({});
	}
	_toggleModal(type, index) {
				this.setState({
			isModalVisible: !this.state.isModalVisible,
			modalType: type,
			modalIndex: index,
			textInputValue: type === 'exp' ? this.state.experiences[index].description : this.state.education[index].description
		});
		
	}

	_toggleModalClose(type, index) {
		this.changeInput(type, this.state.textInputValue, index, 'description');
		this.setState({
			isModalVisible: !this.state.isModalVisible,
			modalType: type,
			modalIndex: index,
			textInputValue: ''
		});
	}


	componentDidMount() {
		//this.props.navigation.state.params.noti.
		this.getUser()
	}

	//Check validation

	guardarGenerales() {
		let { sector1Check, sector2Check, sector3Check, sectorAll, } = false;
		let count = 0;
		if((this.state.sector && this.state.cno) || (!this.state.sector && !this.state.cno)){
			console.log('Está correcto el sector1', this.state.sector, this.state.cno)
			sector1Check = true;
			count = this.state.sector && this.state.cno ? count+1 : count;
		}
		if((this.state.sector2 && this.state.cno2) || (!this.state.sector2 && !this.state.cno2)){
			console.log('Está correcto el sector2', this.state.sector2, this.state.cno2)
			sector2Check = true;
			count = this.state.sector2 && this.state.cno2 ? count+1 : count;
		}
		if((this.state.sector3 && this.state.cno3) || (!this.state.sector3 && !this.state.cno3)){
			console.log('Está correcto el sector23', this.state.sector3, this.state.cno3)
			sector3Check = true;
			count = this.state.sector3 && this.state.cno3 ? count+1 : count;
		}
		if(sector1Check && sector2Check && sector3Check && count>0){
			sectorAll = true;
		}
		if (!this.state.nombre || !this.state.apellidos || !this.state.sexo ||
				!this.state.nacimiento || !this.state.telefono || (this.state.resident != 0 && this.state.resident != 1) ||
				!this.state.nationality || !this.state.contract || 
				!(sectorAll)
			) {
			alert(strings.allInfo);
			this.setState({ showButtons: true });
		} else {
			console.log('CNOS', this.state.cno, this.state.cno2, this.state.cno3)
			let dataBody = {
				"user": {
					"id": globales.UID,
					"name": this.state.nombre,
					"surname": this.state.apellidos
				},
				"applicant": {
					"sex": this.state.sexo,
					"resident": this.state.resident,
					"birthdate": this.state.nacimiento,
					"phone": this.state.telefono,
					"nationality": this.state.nationality,
					"sector": this.state.sector,
					"sector2": this.state.sector2,
					"sector3": this.state.sector3,
					"cno": this.state.cno,
					"cno2": this.state.cno2,
					"cno3": this.state.cno3,
					"contract_duration": this.state.contract
				}
			}
			return fetch(globales.apiURL + '/api/applicant/update-applicant-profile',
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'Authorization': globales.token
					},
					body: JSON.stringify(dataBody)

				})
				.then((response) => {

					return response.json();
				})
				.then((responseJson) => {
					//console.log("editar responseJson", responseJson);
					console.log('SE guaró bien', responseJson)
					if (responseJson.status == "success") {
						this.getDescription()
						this.nextPage();

					}
					this.setState({ showButtons: true });
				})
				.catch((error) => {
					console.error(error);
				});
		}
	}

	sendExp(type, action, exp, index = null) {
		let dataExperience = exp;
		if (!exp.id && action == 'delete') {
			if (type === 'experience') {
				let experiences = this.state.experiences;
				experiences.splice(index, 1);
				this.setState({
					experiences
				})
			} else {
				let education = this.state.education;
				education.splice(index, 1);
				this.setState({
					education
				})
			}
		} else {
			let dataBody;
			if (action === 'delete') {
				action = "delete";
				dataExperience = {
					"id": exp.id
				}
			}
			if (type === 'experience') {
				dataBody = {
					"user": {
						"id": globales.UID // <-- globales.UID
					},
					"applicant": {
						'experience': dataExperience,
					}
				}
			} else {
				dataBody = {
					"user": {
						"id": globales.UID // <-- globales.UID
					},
					"applicant": {
						'education': dataExperience,
					}
				}
			}

			return fetch(globales.apiURL + '/api/applicant/' + action + '-applicant-' + type,
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'Authorization': globales.token
					},
					body: JSON.stringify(dataBody)

				})
				.then((response) => {
					return response.json();
				})
				.then((responseJson) => {
					console.log('ENVIADA EXP', responseJson)
					if (responseJson["status"] == "success") {
						if (action === 'delete') {
							if (type === 'experience') {
								let experiences = this.state.experiences;
								experiences.splice(index, 1);
								this.setState({
									experiences
								})
							} else {
								let education = this.state.education;
								education.splice(index, 1);
								this.setState({
									education
								})
							}

						}
					} else {

					}
				})
				.catch((error) => {
					console.error(error);
				});
		}

	}
	guardarExperiencias(type) {
		let send = true
		console.log('HAS EXP', this.state.hasExp)
		console.log('HAS EDU', this.state.hasEdu)
		if (type === 'experience') {
			if (this.state.experiences.length > 0 && this.state.hasExp) {
				this.state.experiences.map((exp) => {
					if (!exp.departament || !exp.company || !exp.start_year || !exp.end_year || !exp.description) {
						send = false;
					}
				})
			} else {
				send = false;
			}

		} else {
			if (this.state.education.length > 0 && this.state.hasEdu) {
				this.state.education.map((exp) => {
					//console.log('exp edu',exp)
					if (!exp.name || !exp.center || !exp.start_year || !exp.end_year || !exp.description) {
						send = false;
					}
				})
			} else {
				send = false;
			}

		}
		if (send) {
			expToSend = type === 'experience' ? this.state.experiences : this.state.education
			expToSend.map((exp) => {

				let action = 'set';
				if (exp.id) {
					action = 'update'
				}
				console.log(type, exp.id, action)
				this.sendExp(type, action, exp)
			})
			this.nextPage()
			type === 'experience' ? this.getExperiences('education') : this.obtenerLenguajes();
			this.setState({ showButtons: true });
		} else {
			this.setState({ showButtons: true });
			if (type === 'experience' && !this.state.hasExp){
				this.getExperiences('education');
				this.nextPage();
			} else if(type === 'education' && !this.state.hasEdu){
				this.obtenerLenguajes();
				this.nextPage();
			} else {
				alert(strings.allInfo);
			}
			
		}
	}

	getUser() {
		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}
		return fetch(globales.apiURL + '/api/applicant/get-applicant-profile-new',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				console.log("obtenerUsuario responseJson", responseJson)
				this.setState({ page0Loaded: true, page5Loaded: true });
				this.tratarUsuario(responseJson.applicant);
			})
			.catch((error) => {
				console.error(error);
			});

	}

	obtenerLenguajes() {
		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}
		return fetch(globales.apiURL + '/api/applicant/get-applicant-language',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				console.log('Recibe lenguajes', responseJson)
				let languages = responseJson.language;
				let languagesNum = languages ? languages.length : 0;
				let levels = responseJson.nivel_idiomas;
				let levelsNum = levels ? levels.length : 0;
				for(let i=0; i<languagesNum; i++){
					if(!Number.isInteger(languages[i])){
						console.log('No es lenguaje int: ', languages[i]);
						languages = [];
						levels = [];
						i = languagesNum;
					} 
				}
				for(let i=0; i<levelsNum; i++){
					if(!Number.isInteger(levels[i])){
						console.log('No es Nivel int: ', languages[i]);
						languages = [];
						levels = [];
						i = levelsNum;
					} 
				}

				this.setState({
					languagesSelected: languages,
					levelsSelected: levels,
					page4Loaded: true,
					showButtons: true
				})
			})
			.catch((error) => {
				console.error(error);
				this.setState({ showButtons: true });
			});

	}
	guardarIdiomas() {
		console.log('Hay estos lenguahes', this.state.languagesSelected);
		console.log('Hay estos niveles', this.state.levelsSelected);
		const lengthLanguage = this.state.languagesSelected ? this.state.languagesSelected.length : 0;
		const lengthLevels = this.state.levelsSelected ? this.state.levelsSelected.length : 0;
		if (lengthLanguage > 0 && lengthLevels===lengthLanguage) {
			let dataBody = {
				"user": {
					"id": globales.UID
				},

				"applicant": {
					"language": this.state.languagesSelected,
					"nivel_idiomas": this.state.levelsSelected
				}
			}
			return fetch(globales.apiURL + '/api/applicant/set-applicant-language',
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'Authorization': globales.token
					},
					body: JSON.stringify(dataBody)

				})
				.then((response) => {
					console.log("editar languajes", response);
					return response.json();
				})
				.then((responseJson) => {
					// console.log("editar responseJson",responseJson);

					if (responseJson["status"] == "success") {
						this.nextPage()
					}
					this.setState({ showButtons: true });
				})
				.catch((error) => {
					console.error(error);
					this.setState({ showButtons: true });
				});

		} else {
			alert(strings.allInfo);
			this.setState({ showButtons: true });
		}
	}
	getExperiences(type) {
		let dataBody = {
			"user": {
				"id": globales.UID,
			}
		}
		return fetch(globales.apiURL + '/api/applicant/get-applicant-' + type,
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {
				return response.json();
			})
			.then((responseJson) => {
				//console.log('experiences', responseJson)
				this.setState({ showButtons: true });
				if (type === 'experience') {
					this.tratarExperiencias(type, responseJson.experience);
					this.setState({ page2Loaded: true });
				} else {
					this.tratarExperiencias(type, responseJson.education);
					this.setState({ page3Loaded: true });
				}

			})
			.catch((error) => {
				console.error(error);
			});

	}


	tratarExperiencias(type, expAll) {

		if (type === 'experience') {
			if (expAll) {
				expAll.map((exp) => {
					exp.key = this.getKeyExp();
					exp.start_year = exp.start_year ? exp.start_year.toString() : '';
					exp.end_year = exp.end_year ? exp.end_year.toString() : '';
				})
			} else {
				expAll.push({ id: '', departament: '', company: '', start_year: '', end_year: '', description: '', key: this.getKeyExp() })
			}
			//this.edicionExperiencia(jsonExperiencias);
			this.setState({ experiences: expAll });
		} else {
			if (expAll) {
				expAll.map((exp) => {
					exp.key = this.getKeyEdu();
					exp.start_year = exp.start_year ? exp.start_year.toString() : '';
					exp.end_year = exp.end_year ? exp.end_year.toString() : '';
				})
			} else {
				expAll.push({ id: '', name: '', center: '', start_year: '', end_year: '', description: '', key: this.getKeyEdu() })
			}
			//this.edicionExperiencia(jsonExperiencias);
			this.setState({ education: expAll });
		}


	}

	tratarUsuario(usuarioJson) {
		let nacimiento = "1981/01/01"
		if (usuarioJson.birthdate != 0) {
			nacimiento = getDate(usuarioJson.birthdate);
		}

		this.setState({
			nombre: usuarioJson.name,
			apellidos: usuarioJson.surname,
			image: { uri: globales.imageUrl + usuarioJson.image },
			sexo: usuarioJson.sex,
			nacimiento: nacimiento,
			telefono: totalString(usuarioJson.phone),
			resident: usuarioJson.resident,
			nationality: usuarioJson.nationality,
			hasExp: !usuarioJson.work_inexperience,
			hasEdu: !usuarioJson.academic_inexperience,
			sector: usuarioJson.sector,
			sector2: usuarioJson.sector2,
			sector3: usuarioJson.sector3,
			cno: usuarioJson.cno,
			cno2: usuarioJson.cno2,
			cno3: usuarioJson.cno3,
			contract: usuarioJson.contract_duration
		});
	}

	onSelectedItemsChange = (selectedItems) => {
		this.setState({ selectedItems });
	}

	checkData() {
		if (this.state.showButtons) {
			this.setState({ showButtons: false })
			switch (this.state.currentPage) {
				case 0:
					if (this.state.page0Loaded) {
						this.guardarGenerales();
					}
					break;
				case 1:
					if (this.state.page1Loaded) {
						this.setDescription();
					}
					break;
				case 2:
					if (this.state.page2Loaded) {
						this.guardarExperiencias('experience');
					}
					break;
				case 3:
					if (this.state.page3Loaded) {
						this.guardarExperiencias('education');
					}
					break;
				case 4:
					if (this.state.page4Loaded) {
						this.guardarIdiomas();
					}
					break;
				case 5:
					if (this.state.page5Loaded) {
						this.saveImage();
					}
					break;
				default: break;
			}
		}
	}

	saveImage() {
		if (this.state.b64Img) {
			let dataBody = {
				"user": {
					"id": globales.UID,
				},
				"applicant": {
					"image": this.state.b64Img,
				}
			}

			return fetch(globales.apiURL + '/api/applicant/set-applicant-image-cropped',
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'Authorization': globales.token
					},
					body: JSON.stringify(dataBody)

				})
				.then((response) => {
					return response.json();
				})
				.then((responseJson) => {
					if (responseJson["status"] == "success") {
						if (this.props.navigation) {
							this.props.navigation.state.params.reload()
							this.props.navigation.pop()
						} else {
							this.props.userRegistered()
						}

					} else {

					}
					this.setState({ showButtons: true });
				})
				.catch((error) => {
					console.error(error);
				});

		} else {
			if (this.state.image) {
				console.log('imagen',this.state.image)
				if(this.state.image.uri!=="https://www.andjobnow.com:8080/images/false"){
					this.setProfileComplete();
					if (this.props.navigation) {
						this.props.navigation.state.params.reload()
						this.props.navigation.pop()
					} else {
						this.props.userRegistered()
					}
				} else {
					alert(strings.allInfo);
				}
				
			} else {
				alert(strings.allInfo);
			}
			this.setState({ showButtons: true });
		}

	}

	setProfileComplete(){
		let dataBody = {
			"user": {
				"id": globales.UID,
			},
			"applicant": {
				"resume": true,
			}
		}
		return fetch(globales.apiURL + '/api/applicant/change-status-resume',
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'Authorization': globales.token
					},
					body: JSON.stringify(dataBody)

				})
				.then((response) => {
					return response.json();
				})
				.then((responseJson) => {
					console.log('ESTATUS UPDATE', responseJson)
					if (responseJson["status"] == "success") {

					}
				})
				.catch((error) => {
					console.error(error);
				});
	}

	nextPage() {
		this.viewPager.setPage(this.state.currentPage + 1);
		this.setState({
			currentPage: this.state.currentPage + 1
		})
	}

	previousPage() {
		this.setState({ showButtons: true });
		if (this.state.currentPage != 0) {
			this.viewPager.setPage(this.state.currentPage - 1);
			this.setState({
				currentPage: this.state.currentPage - 1
			})
		}
	}

	componentWillReceiveProps(nextProps, nextState) {
		if (nextState.currentPage != this.state.currentPage) {
			if (this.viewPager) {
				this.viewPager.setPage(nextState.currentPage)
			}
		}
	}

	hasExp() {
		this.setHasExperience('work');
		if (this.state.hasExp) {
			this.setState({ hasExp: false })
		} else {
			this.setState({ hasExp: true })
			if (this.state.experiences.length == 0) {
				let experiences = [];
				experiences.push({ id: '', name: '', company: '', start_year: '', end_year: '', description: '', key: this.getKeyExp() })
				this.setState({
					experiences
				})
			}
		}
	}
	hasEdu() {
		this.setHasExperience('academic');
		if (this.state.hasEdu) {
			this.setState({ hasEdu: false })
		} else {
			this.setState({ hasEdu: true })
			if (this.state.education.length == 0) {
				let education = [];
				education.push({ id: '', name: '', center: '', start_year: '', end_year: '', description: '', key: this.getKeyEdu() })
				this.setState({
					education
				})
			}
		}
	}
	addExp() {
		let experiences = this.state.experiences;
		experiences.push({ id: '', name: '', company: '', start: '', end: '', description: '', key: this.getKeyExp() })
		this.setState({
			experiences
		})
		setTimeout(() => { this.scrollViewExp.scrollToEnd({ animated: true }) }, 500)
	}

	addEdu() {

		let education = this.state.education;
		education.push({ name: '', center: '', start_year: '', end_year: '', description: '', key: this.getKeyEdu() })
		this.setState({
			education
		})
		setTimeout(() => { this.scrollViewEdu.scrollToEnd({ animated: true }) }, 500)

	}
	addLanguage() {
		let {languageSelected, levelSelected, languagesSelected, levelsSelected} = this.state;
		console.log('Entra en añadir lenguaje', languageSelected, levelSelected, languagesSelected, levelsSelected);
		
		if(!levelsSelected || !languagesSelected){
			languagesSelected = [];
			levelsSelected = []; 
		}
		if(languageSelected && levelSelected){
			if(languagesSelected){
				var index = languagesSelected.indexOf(languageSelected);
				if (index > -1) {
					console.log('Tiene este lenguaje, no se añade', languageSelected)
				} else {
					console.log('No Tiene este lenguaje,  se añade', languageSelected)
					languagesSelected.push(languageSelected);
					levelsSelected.push(levelSelected);
				}
			} else {
				languagesSelected.push(languageSelected);
				levelsSelected.push(levelSelected);
			}
			
			this.setState({languagesSelected: languagesSelected,
				levelsSelected: levelsSelected,
				languageSelected: null,
				levelSelected: null,

			});
			console.log('Añade lenguaje', languagesSelected, levelsSelected)
		}
		

	}

	deleteLanguage(id){
		let {languagesSelected, levelsSelected} = this.state;
		var index = languagesSelected.indexOf(id);
				if (index > -1) {
					console.log('Tiene este lenguaje, se elimina', languagesSelected)
					languagesSelected.splice(index, 1);
					levelsSelected.splice(index, 1);
					this.setState({languagesSelected: [...languagesSelected], 
									levelsSelected: [...levelsSelected]
								});
				} else {
					console.log('No Tiene este lenguaje, no hace nada')
				}
		console.log('delete lenguaje', index)
	}

	deleteEdu(index) {
		let education = this.state.education;
		education.splice(index, 1);
		this.setState({
			education
		})
	}

	changeInput(type, text, index, subtype) {
		if (type === "exp") {
			const experiences = this.state.experiences;
			experiences[index][subtype] = text;
			this.setState({
				experiences
			});
		} else {
			const education = this.state.education;
			education[index][subtype] = text;
			this.setState({
				education
			});
		}
	}
	getKeyExp() {
		return expKeys++;
	}
	getKeyEdu() {
		return eduKeys++;
	}

	actualizarImagen() {



		var options = {
			title: strings.updatePhoto,
			cancelButtonTitle: strings.cancel,
			takePhotoButtonTitle: strings.takePhoto,
			chooseFromLibraryButtonTitle: strings.choosePhoto,
		};
		
		ImagePicker.showImagePicker(options, (response) => {


			if (response.didCancel) {
				console.log('User cancelled image picker');
			}
			else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			}
			else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			}
			else {

				let b64Img = 'data:image/jpeg;base64,' + response.data;
				this.setState({
					image: { uri: b64Img },
					b64Img: b64Img
				});


			}
		});

	}

	getDescription() {

		let dataBody = {
			"user": {
				"id": globales.UID
			}
		}

		return fetch(globales.apiURL + '/api/applicant/get-applicant-introduction',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {

				return response.json();
			})
			.then((responseJson) => {
				console.log("DESCRIPTION", responseJson);
				if (responseJson.status == "success") {
					this.setState({
						description: responseJson.introduction,
						page1Loaded: true,
					})
				}
			})
			.catch((error) => {
				console.error(error);
			});
	}
	setDescription() {
		if (this.state.description === '') {
			alert(strings.allInfo);
			this.setState({ showButtons: true });
		} else {
			let dataBody = {
				"user": {
					"id": globales.UID,
				},
				"applicant": {
					"introduction": this.state.description
				}
			}

			return fetch(globales.apiURL + '/api/applicant/set-applicant-introduction',
				{
					method: "POST",
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						'Authorization': globales.token
					},
					body: JSON.stringify(dataBody)

				})
				.then((response) => {

					return response.json();
				})
				.then((responseJson) => {
					if (responseJson.status == "success") {
						this.getExperiences('experience');
						this.nextPage()
					}
					this.setState({ showButtons: true });
				})
				.catch((error) => {
					console.error(error);
				});
		}

	}

	setHasExperience(type) {
		let databody = null;
		if (type === 'work') {
			dataBody = {
				"user": {
					"id": globales.UID,
				},
				"applicant": {
					"work_inexperience": this.state.hasExp
				}
			}
		} else {
			dataBody = {
				"user": {
					"id": globales.UID,
				},
				"applicant": {
					"academic_inexperience": this.state.hasEdu
				}
			}
		}


		return fetch(globales.apiURL + '/api/applicant/set-applicant-' + type + '-inexperience',
			{
				method: "POST",
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Authorization': globales.token
				},
				body: JSON.stringify(dataBody)

			})
			.then((response) => {

				return response.json();
			})
			.then((responseJson) => {
				if (responseJson.status == "success") {

				}
			})
			.catch((error) => {
				console.error(error);
			});
	}
	goBack() {
		this.props.navigation.state.params.reload();
		this.props.navigation.pop();
	}
	render() {
		console.log('PORRRPS', this.props.navigation, this.props)
		return (
			<View style={styles.container}>
										<Spinner visible={this.state.loadingVisible} textStyle={{ color: '#FFF' }} />

				{this.props.navigation ?
					<View style={styles.cabecera}>

						<View style={{ flex: 0.2, }}>
							<TouchableOpacity style={{ alignItems: 'flex-start', paddingLeft: eH(10) }} onPress={() => this.goBack()}>
								<NewIcon name='arrow-back'
									type='ionicons'
									color='#FFFFFF'
									size={eH(28)}
								/>
							</TouchableOpacity>
						</View>
						<Text style={{ paddingTop: 15, flex: 0.6, textAlign: 'center', color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: eH(20) }}>{strings.editCV}</Text>

						<View style={{ flex: 0.2 }} />

					</View>
					:
					<View style={styles.containerText}>
						<Text style={styles.textTitle}>{strings.welcomeTo}</Text>
						<Text style={styles.textTitle}>AndJobNow</Text>
						<Text style={styles.textTitle2}>{strings.finishProfile}</Text>
					</View>
				}
				<View style={styles.stepIndicator}>
					<StepIndicator
						stepCount={6}
						renderStepIndicator={this.renderStepIndicator}
						customStyles={secondIndicatorStyles}
						currentPosition={this.state.currentPage}
					/>
				</View>
				<ViewPager
					style={{ flexGrow: 1 }}
					scrollEnabled={false}
					ref={(viewPager) => { this.viewPager = viewPager }}
				>
					{this.state.pages.map((page) => this.renderViewPagerPage(page))}
				</ViewPager>
			</View>
		);
	}


	renderLanguage(id, index){
		const {languages, level, levelsSelected} = this.state;
		let returnLanguage = null;
		let returnLevel = null;
		console.log('ENtra en renderLanguage', id, index)
		for(let j=0; j<level.length; j++){
			if(levelsSelected && levelsSelected.length>index){
				if(level[j].value === levelsSelected[index]){
					console.log('Encuentra level',level[j].label)
					returnLevel = level[j].label;
					j=level.length;
				}
			}
			
		}

		for(let i=0; i<languages.length; i++){
			if(languages[i].value===id){
				returnLanguage = languages[i].label;
				console.log('Encuentra languagej',returnLanguage)
				i=languages.length;
				
			}
		}
		console.log('Va a mostrar lenguage? ', returnLanguage && returnLevel);
		return returnLanguage && returnLevel ? 
		<View key={'language'+index} style={{width: '100%',justifyContent: 'flex-start',alignItems: 'flex-start', flexDirection: 'row', borderWidth: 1, borderRadius: 5, borderColor: 'gray', padding: 5, marginBottom: 10}}>
		<Text style={{width: '100%', color: 'gray',fontFamily: 'Montserrat-Medium', fontSize: eH(12)}}>{returnLanguage} - {returnLevel}</Text>
		<TouchableOpacity onPress={() => this.deleteLanguage(id)} style={{marginLeft: 5}}>
		<NewIcon name='close'
			type='ionicons'
			color='gray'
			size={eH(13)}
		/></TouchableOpacity>
	</View> : null
	}

onChangeText = (text) => {
	console.log('TEXT description', text)
	this.setState({textInputValue: text})
}

	renderViewPagerPage = (data) => {
		let pageContent = <View style={{ flex: 1, justifyContent: 'center' }}>
			<ActivityIndicator size="large" color="#F39C12" />
		</View>;
		switch (data.key) {
			case 0:
				if (this.state.page0Loaded) {
					pageContent = <KeyboardAwareScrollView enableResetScrollToCoords={false} style={{ flex: 1, width: '100%', paddingLeft: 15 }} extraScrollHeight={-20}>

						<StyleProvider style={getTheme(material)}>

							<Form style={{ width: '90%', marginBottom: 20 }}>
								<Item stackedLabel>
									<Label textStyle={styles.inputText}>{strings.name}</Label>
									<Input style={styles.inputText} value={this.state.nombre} onChangeText={(nombre) => this.setState({ nombre })} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.lastName}</Label>
									<Input style={styles.inputText} value={this.state.apellidos} onChangeText={(apellidos) => this.setState({ apellidos })} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.contactPhone}</Label>
									<Input style={styles.inputText} value={this.state.telefono} onChangeText={(telefono) => this.setState({ telefono })} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.birthDate}</Label>
									<DatePicker
										style={{
											width: '100%',
										}}
										date={this.state.nacimiento}
										mode="date"
										format="YYYY/MM/DD"
										timeZoneOffsetInMinutes={timeZoneOffsetInHours * 60}
										confirmBtnText="Confirmar"
										cancelBtnText="Cancel·lar"
										showIcon={false}
										customStyles={{

											dateText: {
												width: '100%',
												borderWidth: 0,
												borderColor: 'white',
												alignSelf: 'flex-start',
												paddingHorizontal: eH(4),
												alignItems: 'flex-start',
												textAlign: 'left',
												color: 'black',
												fontFamily: 'Montserrat-Regular',
												fontSize: eH(14),
											},
											dateInput: {
												borderWidth: 0,
												width: '100%',
												alignSelf: 'flex-start',

											}


										}}
										onDateChange={(nacimiento) => { this.setState({ nacimiento }) }}
									/>
								</Item>

								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.sex}</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: 0,
										}}
										items={this.state.itemsSex}
										onValueChange={(sexo) => {
											this.setState({
												sexo
											});
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.sexo}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.resident}</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: 2,
										}}

										items={this.state.itemsResident}
										onValueChange={(resident) => {
											this.setState({
												resident
											});
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.resident}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.nation}</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: 0,
										}}
										items={this.state.itemsCountries}
										onValueChange={(nationality) => {
											this.setState({ nationality });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.nationality}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								
								<Item stackedLabel  style={{marginBottom: 10}}>
									<Label style={styles.inputText}>{strings.contractType}</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.contracts}
										onValueChange={(contract) => {
											this.setState({ contract });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.contract}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Text style={styles.titlePage}>{strings.sectors}</Text>

								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.chooseSector} 1</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.sectors}
										onValueChange={(sector) => {
											this.setState({ sector });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.sector}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel style={{marginBottom: 10}}>
									<Label style={styles.inputText}>{strings.chooseCno} 1</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.cnos}
										onValueChange={(cno) => {
											this.setState({ cno });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.cno}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.chooseSector} 2</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.sectors}
										onValueChange={(sector2) => {
											this.setState({ sector2 });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.sector2}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel style={{marginBottom: 10}}>
									<Label style={styles.inputText}>{strings.chooseCno} 2</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.cnos}
										onValueChange={(cno2) => {
											this.setState({ cno2 });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.cno2}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>

							<Item stackedLabel>
									<Label style={styles.inputText}>{strings.chooseSector} 3</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.sectors}
										onValueChange={(sector3) => {
											this.setState({ sector3 });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.sector3}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel style={{marginBottom: 10}}>
									<Label style={styles.inputText}>{strings.chooseCno} 3</Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.cnos}
										onValueChange={(cno3) => {
											this.setState({ cno3 });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.cno3}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
										
							</Form>
						</StyleProvider>
					</KeyboardAwareScrollView>
				}


				break;
			case 1:
				if (this.state.page1Loaded) {
					pageContent = <KeyboardAwareScrollView enableResetScrollToCoords={false} style={{ flex: 1, width: '100%', paddingLeft: 15 }} extraScrollHeight={-20}>
						<StyleProvider style={getTheme(material)}>
							<Form style={{
								width: '90%', justifyContent: 'center',
								alignItems: 'center'
							}}>
								<View style={{
									marginLeft: 15, width: '100%', justifyContent: 'center',
									alignItems: 'center'
								}}>
									<Item stackedLabel style={{ width: '100%' }}>
										<Label>{strings.presentation}</Label>
										<Input value={this.state.description} multiline={true} numberOfLines={10} maxLength={200} onChangeText={(description) => this.setState({ description })} />
									</Item>
									{/* <Text style={styles.countLetters}>{this.state.description.length}/200</Text> */}
								</View>
							</Form>
						</StyleProvider>
					</KeyboardAwareScrollView>
				}
				break;
			case 2:
				if (this.state.page2Loaded) {
					pageContent = <KeyboardAwareScrollView style={{ flex: 1, width: '100%', paddingLeft: 15 }}
						extraScrollHeight={-20}
						innerRef={ref => this.scrollViewExp = ref}>
						<StyleProvider style={getTheme(material)}>
							<Form style={{
								width: '90%', justifyContent: 'center',
								alignItems: 'center'
							}}>
								<View style={{
									marginLeft: 15, width: '100%', justifyContent: 'center',
									alignItems: 'center'
								}}>
									<CheckBox
										center
										title={strings.noExp}
										checked={!this.state.hasExp}
										onPress={() => this.hasExp()}
										checkedColor="#689c15"
									/>
									{this.state.hasExp ?
										<View style={{ width: '100%' }}>
											{this.state.experiences.map((exp, index) => {

												return <Card key={exp.key}>
													<CardItem>
														<Body>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label textStyle={styles.inputText}>{strings.charge}</Label>
																<Input style={styles.inputText} value={exp.departament} onChangeText={(text) => this.changeInput('exp', text, index, 'departament')} />
															</Item>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label style={styles.inputText}>{strings.company}</Label>
																<Input style={styles.inputText} value={exp.company} onChangeText={(text) => this.changeInput('exp', text, index, 'company')} />
															</Item>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label style={styles.inputText}>{strings.startYear}</Label>
																<Input style={styles.inputText} value={exp.start_year} onChangeText={(text) => this.changeInput('exp', text, index, 'start_year')} />
															</Item>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label style={styles.inputText}>{strings.endYear}</Label>
																<Input style={styles.inputText} value={exp.end_year} onChangeText={(text) => this.changeInput('exp', text, index, 'end_year')} />
															</Item>
															<TouchableOpacity onPress={() => this._toggleModal('exp', index)}>
																<View style={{ marginVertical: 5, width: '100%', flexDirection: 'column', alignItems: 'flex-start' }}>
																	<Label style={{ color: 'grey' }}>{strings.description}</Label>
																	<Text style={styles.inputText}>{exp.description}</Text>
																</View>
															</TouchableOpacity>
														</Body>
													</CardItem>
													<CardItem button footer onPress={() => this.sendExp('experience', 'delete', exp, index)} style={{
														backgroundColor: '#EC4242', justifyContent: 'center',
														alignItems: 'center'
													}}>
														<Text style={styles.textAdd}>{strings.delete}</Text>
													</CardItem>
												</Card>
											})}

										</View>
										: null}
								</View>

							</Form>
						</StyleProvider>
					</KeyboardAwareScrollView>
				}
				break;
			case 3:
				if (this.state.page3Loaded) {
					pageContent = <KeyboardAwareScrollView enableResetScrollToCoords={false} style={{ flex: 1, width: '100%', paddingLeft: 15 }} extraScrollHeight={-20} ref={ref => this.scrollViewEdu = ref}>
						<StyleProvider style={getTheme(material)}>
							<Form style={{
								width: '90%', justifyContent: 'center',
								alignItems: 'center'
							}}>
								<View style={{
									marginLeft: 15, width: '100%', justifyContent: 'center',
									alignItems: 'center'
								}}>
									<CheckBox
										center
										title={strings.noFormation}
										checked={!this.state.hasEdu}
										onPress={() => this.hasEdu()}
										checkedColor="#689c15"
									/>
									{this.state.hasEdu ?
										<View style={{ width: '100%' }}>
											{this.state.education.map((edu, index) => {

												return <Card key={edu.key}>
													<CardItem>
														<Body>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label textStyle={styles.inputText}>{strings.name}</Label>
																<Input style={styles.inputText} value={edu.name} onChangeText={(text) => this.changeInput('edu', text, index, 'name')} />
															</Item>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label style={styles.inputText}>{strings.academy}</Label>
																<Input style={styles.inputText} value={edu.center} onChangeText={(text) => this.changeInput('edu', text, index, 'center')} />
															</Item>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label style={styles.inputText}>{strings.startYear}</Label>
																<Input style={styles.inputText} value={edu.start_year} onChangeText={(text) => this.changeInput('edu', text, index, 'start_year')} />
															</Item>
															<Item floatingLabel style={{ marginVertical: 5 }}>
																<Label style={styles.inputText}>{strings.endYear}</Label>
																<Input style={styles.inputText} value={edu.end_year} onChangeText={(text) => this.changeInput('edu', text, index, 'end_year')} />
															</Item>
															<TouchableOpacity onPress={() => this._toggleModal('edu', index)}>
																<View style={{ marginVertical: 5, width: '100%', flexDirection: 'column', alignItems: 'flex-start' }}>
																	<Label style={{ color: 'grey' }}>{strings.description}</Label>
																	<Text style={styles.inputText}>{edu.description}</Text>
																</View>
															</TouchableOpacity>
														</Body>
													</CardItem>
													<CardItem button footer onPress={() => this.sendExp('education', 'delete', edu, index)} style={{
														backgroundColor: '#EC4242', justifyContent: 'center',
														alignItems: 'center'
													}}>
														<Text style={styles.textAdd}>{strings.delete}</Text>
													</CardItem>
												</Card>
											})}

										</View>
										: null}
								</View>

							</Form>
						</StyleProvider>
					</KeyboardAwareScrollView>
				}
				break;
			case 4:
				if (this.state.page4Loaded) {
					pageContent = <ScrollView style={{ width: '100%', paddingLeft: 15 }}>
						<StyleProvider style={getTheme(material)}>
							<Form style={{
								width: '90%'
							}}>
								<View style={{ width: '100%', paddingLeft: 15 }}>
								{this.state.languagesSelected.map((id, index) => {
									return this.renderLanguage(id, index)
								})}
								
								<Text></Text>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.selectLanguage} </Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.languages}
										onValueChange={(languageSelected) => {
											this.setState({ languageSelected });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.languageSelected}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
								<Item stackedLabel>
									<Label style={styles.inputText}>{strings.selectLevel} </Label>
									<RNPickerSelect style={{ ...pickerSelectStyles }}
										placeholder={{
											label: strings.select,
											value: null,
										}}
										items={this.state.level}
										onValueChange={(levelSelected) => {
											this.setState({ levelSelected });
										}}
										onUpArrow={() => {
											this.inputRefs.name.focus();
										}}
										onDownArrow={() => {
											this.inputRefs.picker2.togglePicker();
										}}

										value={this.state.levelSelected}
										ref={(el) => {
											this.inputRefs.picker = el;
										}} />
								</Item>
										
									
								</View>

							</Form>
						</StyleProvider>
					</ScrollView>
				}
				break;
			case 5:
				if (this.state.page5Loaded) {
					pageContent = <ScrollView style={{ width: '100%', paddingLeft: 15 }}>
						<StyleProvider style={getTheme(material)}>
							<Form style={{ width: '90%' }}>
								<TouchableOpacity
									style={{ marginLeft: 10, marginTop: 30, width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}
									onPress={() => this.actualizarImagen()}
								>
									<Image source={this.state.image ? this.state.image.uri!= "https://www.andjobnow.com:8080/images/false" ? this.state.image : require('../img/default.png') : require('../img/default.png')} style={styles.imgPerfil} />
									<Text style={styles.textActImagen}>{strings.updatePhoto}</Text>
								</TouchableOpacity>
							</Form>
						</StyleProvider>
					</ScrollView>
				}
				break;
			default: break;
		}
		return (<View key={data.key} style={styles.page}>
			<View style={styles.contentPage}>
				<Text style={styles.titlePage}>{data.title}</Text>
				{pageContent}
				{this.state.showButtons ?
					<View style={styles.buttonsBottom}>
						<Button onPress={() => this.previousPage()} iconRight style={styles.buttons}>

							<Icon name='arrow-back' color='white' style={{ alignSelf: 'center', marginRight: 0 }} />
						</Button>
						{(data.key === 2 && this.state.hasExp) ?
							<Button onPress={() => this.addExp()} iconRight style={styles.buttonAdd}>
								<Text style={styles.textAdd}>{strings.add}</Text>
							</Button>
							:
							(data.key === 3 && this.state.hasEdu) ?
								<Button onPress={() => this.addEdu()} iconRight style={styles.buttonAdd}>
									<Text style={styles.textAdd}>{strings.add}</Text>
								</Button>
								:
								(data.key === 4 ) ?
								<Button disabled={(!this.state.levelSelected || !this.state.languageSelected)} onPress={() => this.addLanguage()} iconRight style={styles.buttonAdd}>
									<Text style={styles.textAdd}>{strings.add}</Text>
								</Button>
								:
								null}

						<Button onPress={() => this.checkData()} iconRight style={styles.buttons}>
							<Icon name='arrow-forward' color='white' style={{ alignSelf: 'center', marginRight: 0 }} />
						</Button>
					</View>

					: null}

				<Modal avoidKeyboard={true} isVisible={this.state.isModalVisible} >
				<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{ flex: 1, width: '100%', height: '100%' }}>
					<View style={{justifyContent: 'space-between', alignItems: 'flex-start', width: '100%', height: '60%', backgroundColor: 'white', borderRadius: 10, paddingBottom: 20 }}>
						<TouchableOpacity style={{ position: 'absolute', right: 15, top: 15 }} onPress={() => this._toggleModalClose(this.state.modalType, this.state.modalIndex)}>
							<Icon name='close' color='#6F6F6F' size={24} />
						</TouchableOpacity>
						<StyleProvider style={getTheme(material)}>
							<Form style={{
								marginTop: 20,
								width: '90%', justifyContent: 'flex-start',
								alignItems: 'flex-start'
							}}>

								<Item stackedLabel style={{ width: '100%',height: '80%' }}>
									<Label style={{marginBottom: 5}}>{strings.description}</Label>
									{this.state.modalIndex != null ?
										<Input ref={component => { this.addTodoRef = component }}
										onChangeText={this.onChangeText} style={{height: '80%'}} value={this.state.textInputValue}
											multiline={true} maxLength={200} />
										: null
									}

								</Item>



							</Form>
						</StyleProvider>
						<Button style={styles.buttonSaveModal} onPress={() => this._toggleModalClose(this.state.modalType, this.state.modalIndex)}>
							<Text style={styles.textBotonGuardar}>{strings.close}</Text>
						</Button>
					</View>
				</TouchableWithoutFeedback>
				</Modal>
			</View>

		</View>)
	}

	renderStepIndicator = params => (
		<MaterialIcon {...getStepIndicatorIconConfig(params)} />
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#F39C12',

	},
	stepIndicator: {
		marginTop: -30,
		marginBottom: 0,
	},
	page: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	contentPage: {
		flex: 0.9,
		width: '90%',
		justifyContent: 'flex-start',
		alignItems: 'center',
		backgroundColor: 'white',
		borderRadius: 20
	},
	containerText: {
		flex: 1,
		minHeight: '22%',
		marginTop: 30,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	textTitle: {
		fontSize: eH(25),
		fontFamily: 'Montserrat-Regular',
		fontWeight: '600',
		color: '#FFFFFF',
	},
	textTitle2: {
		fontSize: eH(16),
		fontFamily: 'Montserrat-Regular',
		fontWeight: '400',
		color: '#FFFFFF',
		margin: 15,
		textAlign: 'center'
	},
	titlePage: {
		fontSize: eH(20),
		fontFamily: 'Montserrat-Regular',
		fontWeight: '400',
		color: '#F39C12',
		margin: 15,
		textAlign: 'center'
	},
	inputText: {
		fontFamily: 'Montserrat-Regular',
	},

	countLetters: {
		fontFamily: 'Montserrat-Regular',
		color: 'grey',
		textAlign: 'center'
	},
	textAdd: {
		fontFamily: 'Montserrat-Regular',
		color: 'white',
		fontSize: eH(16),
		textAlign: 'center'
	},
	textDelete: {
		fontFamily: 'Montserrat-Regular',
		color: 'white',
		fontSize: eH(14),
		textAlign: 'center'
	},
	buttonsBottom: {
		width: '90%',
		margin: 15,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	buttons: {
		backgroundColor: '#689c15',
		width: '20%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonAdd: {
		backgroundColor: '#F39C12',
		width: '50%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	imgPerfil: {
		height: 150,
		width: 150,
		borderRadius: 75
	},
	textActImagen: {
		color: '#f39c10',
		marginTop: 20,
		fontFamily: 'Montserrat-Medium',
		fontSize: eH(16),
	},
	cabecera: {
		backgroundColor: '#f39c10',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: eH(55),
		paddingHorizontal: 8,
		marginBottom: 40,
		width: '100%',
	},
	buttonEnter: {
		position: 'absolute',
		bottom: 15,
		alignSelf: 'center',
		width: '90%',
		backgroundColor: '#689c15',
		borderRadius: 5,
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonSaveModal: {
		alignSelf: 'center',
		marginBottom: 30,
		width: '90%',
		backgroundColor: '#689c15',
		borderRadius: 5,
		justifyContent: 'center',
		alignItems: 'center',
	},
	textBotonGuardar: {
		fontFamily: 'Montserrat-Bold',
		fontSize: eH(17),
		color: '#FFFFFF',
		textAlign: 'center',
		fontWeight: '600',
	},
});
const stylesSelect = StyleSheet.create({
	button: { backgroundColor: "#689c15" },
	confirmText: { fontFamily: 'Montserrat-Regular' },
	chipText: { fontFamily: 'Montserrat-Regular' },
	selectToggleText: { fontFamily: 'Montserrat-Regular' },
});
const pickerSelectStyles = StyleSheet.create({

	inputIOS: {
		fontSize: 16,
		fontFamily: 'Montserrat-Regular',
		marginTop: 13,
		paddingRight: 10,
		paddingBottom: 5,
		borderWidth: 0,
		borderColor: 'gray',
		backgroundColor: 'white',
		color: 'black',
	},
});

const countries = [
	{ value: 1, label: "Afganistán" },
	{ value: 2, label: "Afgana" },
	{ value: 3, label: "Alemania" },
	{ value: 4, label: "Andorra" },
	{ value: 5, label: "Arabia Saudita" },
	{ value: 6, label: "Argentina" },
	{ value: 7, label: "Australia" },
	{ value: 8, label: "Bélgica" },
	{ value: 9, label: "Bolivia" },
	{ value: 10, label: "Brasil" },
	{ value: 11, label: "Camboya" },
	{ value: 12, label: "Canadá" },
	{ value: 13, label: "Cataluña" },
	{ value: 14, label: "Chile" },
	{ value: 15, label: "China" },
	{ value: 16, label: "Colombia" },
	{ value: 17, label: "Corea" },
	{ value: 18, label: "Costa Rica" },
	{ value: 19, label: "Cuba" },
	{ value: 20, label: "Dinamarca" },
	{ value: 21, label: "Ecuador" },
	{ value: 22, label: "Egipto" },
	{ value: 23, label: "El Salvador" },
	{ value: 24, label: "España" },
	{ value: 25, label: "Estados Unidos" },
	{ value: 26, label: "Estonia" },
	{ value: 27, label: "Etiopia" },
	{ value: 28, label: "Filipinas" },
	{ value: 29, label: "Finlandia" },
	{ value: 30, label: "Francia" },
	{ value: 31, label: "Gales" },
	{ value: 32, label: "Grecia" },
	{ value: 33, label: "Guatemala" },
	{ value: 34, label: "Haití" },
	{ value: 35, label: "Holanda" },
	{ value: 36, label: "Honduras" },
	{ value: 37, label: "Indonesia" },
	{ value: 38, label: "Inglaterra" },
	{ value: 39, label: "Irak" },
	{ value: 40, label: "Irán" },
	{ value: 41, label: "Irlanda" },
	{ value: 42, label: "Israel" },
	{ value: 43, label: "Italia" },
	{ value: 44, label: "Japón" },
	{ value: 45, label: "Jordania" },
	{ value: 46, label: "Laos" },
	{ value: 47, label: "Letonia" },
	{ value: 48, label: "Lituania" },
	{ value: 49, label: "Malasia" },
	{ value: 50, label: "Marrueco" },
	{ value: 51, label: "México" },
	{ value: 52, label: "Nicaragua" },
	{ value: 53, label: "Noruega" },
	{ value: 54, label: "Nueva Zelanda" },
	{ value: 55, label: "Panamá" },
	{ value: 56, label: "Paraguay" },
	{ value: 57, label: "Perú" },
	{ value: 58, label: "Polonia" },
	{ value: 59, label: "Portugal" },
	{ value: 60, label: "Puerto" },
	{ value: 61, label: "República Dominicana" },
	{ value: 62, label: "Rumania" },
	{ value: 63, label: "Rusia" },
	{ value: 64, label: "Suecia" },
	{ value: 65, label: "Suiza" },
	{ value: 66, label: "Tailandia" },
	{ value: 67, label: "Taiwán" },
	{ value: 68, label: "Turquía" },
	{ value: 69, label: "Ucrania" },
	{ value: 70, label: "Uruguay" },
	{ value: 71, label: "Venezuela" },
	{ value: 72, label: "Vietnam" }
]


