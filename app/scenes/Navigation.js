import React from 'react';
import { Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { StackNavigator, TabNavigator, TabBarBottom, NavigationActions } from 'react-navigation';
import { Candidaturas } from './Candidaturas';
import { Curriculum } from './Curriculum';
import { Configuracion } from './Configuracion';
import { Ofertas } from './Ofertas';
import { Noticias } from './Noticias';
import { Noticia } from './Noticia';
import{ Video } from './Video';
import Wizard from './Wizard';
import { globales, strings } from '../globales.js';
import { OfertasRechazadas } from './OfertasRechazadas';


//#f39c10

const OfertasStack = StackNavigator({
	Ofertas: {
	  screen: Ofertas,
	  navigationOptions: () => ({
		title: 'Explorar Ofertes',
		swipeEnabled: false,
	  }),
	},
	OfertasRechazadas: {
		screen: OfertasRechazadas,
		navigationOptions: () => ({
		  title: 'Explorar Ofertes',
		  swipeEnabled: false,
		}),
	  },
	Video: {
		screen: Video,
		navigationOptions: () => ({
		  title: 'Videocall',
		  swipeEnabled: false,
		}),
	  }
},
	{
		initialRouteName: 'Ofertas',
		headerMode: 'none' ,
		navigationOptions:({ navigation }) => ({
		  tabBarOnPress: ({ scene, jumpToIndex }) => {
			const { route, focused, index } = scene;
			if (focused) {
			  if (route.index > 0) {
				const { routeName, key } = route.routes[1]
				navigation.dispatch(NavigationActions.back({ key }))
			  }
			} else {
			  jumpToIndex(index);
			}
		  },
		}),
	  },
	);

	const CVStack = StackNavigator({
		Curriculum: {
			screen: Curriculum,
			navigationOptions: ({ navigation }) => ({
				title: 'Currículum Vitae',
				swipeEnabled: false,
			}),
		},
		Wizard: {
			screen: Wizard,
			navigationOptions: () => ({
				title: 'Wizard',
				swipeEnabled: false,
			}),
			}
	},
		{
			initialRouteName: 'Curriculum',
			headerMode: 'none' ,
			navigationOptions:({ navigation }) => ({
				tabBarOnPress: ({ scene, jumpToIndex }) => {
				const { route, focused, index } = scene;
				if (focused) {
					if (route.index > 0) {
					const { routeName, key } = route.routes[1]
					navigation.dispatch(NavigationActions.back({ key }))
					}
				} else {
					jumpToIndex(index);
				}
				},
			}),
			},
		);

		const NewsStack = StackNavigator({
			Noticias: {
				screen: Noticias,
				navigationOptions: ({ navigation }) => ({
					title: 'Blog',
					swipeEnabled: false,
				}),
			},
			Noticia: {
				screen: Noticia,
				navigationOptions: () => ({
					title: 'Blog',
					swipeEnabled: false,
				}),
				}
		},
			{
				initialRouteName: 'Noticias',
				headerMode: 'none' ,
				navigationOptions:({ navigation }) => ({
					tabBarOnPress: ({ scene, jumpToIndex }) => {
					const { route, focused, index } = scene;
					if (focused) {
						if (route.index > 0) {
						const { routeName, key } = route.routes[1]
						navigation.dispatch(NavigationActions.back({ key }))
						}
					} else {
						jumpToIndex(index);
					}
					},
				}),
				},
			);

export default TabNavigator(
	{
		Ofertas: {
			screen: OfertasStack,   
			navigationOptions: ({ navigation }) => ({
				title: strings.exploreOffers,
				swipeEnabled: false,
			}),
		},
		Curriculum: {
			screen: CVStack,
			navigationOptions: ({ navigation }) => ({
				title: 'Currículum Vitae',
			}),
		},
		Candidaturas: {
			screen: Candidaturas,
			navigationOptions: ({ navigation }) => ({
				title: strings.candidacies,
			}),
		},
		Noticias: {
			screen: NewsStack,
			navigationOptions: ({ navigation }) => ({
				title: 'Blog',
			}),
		},
		Configuracion: {
			screen: Configuracion,
			navigationOptions: ({ navigation }) => ({
				title: strings.configuration,
			}),
		},
	},
	{
		initialRouteName: 'Ofertas',

		navigationOptions: ({ navigation }) => ({
			swipeEnabled: false,
			tabBarIcon: ({ tintColor, focused }) => {
				const { routeName } = navigation.state;
				let iconName, iconType, iconSize;
				switch (routeName) {
					case 'Ofertas':
						iconName = focused ? 'ios-apps' : 'ios-apps-outline'
						iconType = 'ionicon'
						iconSize = 32
						break;
					case 'Curriculum':
						iconName = focused ? 'file-text' : 'file-text-o'
						iconType = 'font-awesome'
						iconSize = 26
						break;
					case 'Candidaturas':
						iconName = focused ? 'handshake-o' : 'handshake-o'
						iconType = 'font-awesome'
						iconSize = 26
						break;
					case 'Noticias':
						iconName = focused ? 'newspaper-o' : 'newspaper-o'
						iconType = 'font-awesome'
						iconSize = 26
						break;
					case 'Configuracion':
						iconName = focused ? 'user-circle' : 'user-circle-o'
						iconType = 'font-awesome'
						iconSize = 26
						break;
				}

				return (
					<Icon
						name={iconName}
						size={iconSize}
						color={tintColor}
						type={iconType}
						style={{}}
					/>
				);
			},
		}),
		tabBarComponent: TabBarBottom,
		tabBarPosition: 'bottom',

		tabBarOptions: {
			style: {
				paddingTop: 4,
				height: 52,
			},
			labelStyle: {
				fontSize: 11,
				marginTop: 0,
				padding: 0,
				marginBottom: 4,
				// color: 'red',
				// height:100,
				// width:100,
				// flex: 0.4,
				// display: 'block',
			},
			activeTintColor: '#f39c10',
			inactiveTintColor: '#6f6f6f',
		},
		animationEnabled: true,
		swipeEnabled: true,
	}
);