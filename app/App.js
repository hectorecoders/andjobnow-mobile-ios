
console.ignoredYellowBox = ['Remote debugger'];

import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground, Image, Text, StatusBar, AsyncStorage, Platform } from 'react-native';

import Home from './scenes/Home';
import Login from './scenes/Login';
import Wizard from './scenes/Wizard';
import Register from './scenes/Register';
import Recover from './scenes/Recover';
// import {Descripcion} from './scenes/Descripcion';
import { Ofertas } from './scenes/Ofertas';
// import {Curriculum} from './scenes/Curriculum';
import TabNavigator from './scenes/Navigation';
import SplashScreen from 'react-native-splash-screen';

import Swiper from 'react-native-swiper';

import { globales, strings } from './globales.js';

//import FCM, {FCMEvent} from 'react-native-fcm';



console.disableYellowBox = true; 


export default class MainApp extends Component {
	constructor(props) {
		super(props)
		//AsyncStorage.setItem('tourjob', 'true');
		this.state = {
			tour: 'true',
			screen: 'login',
			
		}
	}

	_storeData = async () => {
		this.setState({tour: 'false'})
		try {
			await AsyncStorage.setItem('tourjob', 'false');
		} catch (error) {
			// Error saving data
		}
	}

	_retrieveData = async () => {
		
		try {
			const value = await AsyncStorage.getItem('tourjob');
			const language = await AsyncStorage.getItem('jobLanguage');
			
			if (value !== null) {
				// We have data!!
				this.setState({tour: value})
			} else {
				console.log('tour', true);
			}

			if (language !== null) {
				strings.setLanguage(language);
				globales.language = language;
				this.setState({});
			} 

			
		} catch (error) {

		}
	}

	componentDidMount() {

		this._retrieveData();
		
	}
	
	onIndexChanged = (index) => {
		if(index==4){
			this._storeData()
		}
		
	  };

	renderSwiper(){
		StatusBar.setHidden(true);
		SplashScreen.hide();
		return <View style={{ flex: 1, backgroundColor: '#f39c10' }}>
		<Swiper loop={false} onIndexChanged={this.onIndexChanged}  activeDotColor={'#f39c10'} showsButtons={false}>
		<ImageBackground source={require('./img/backtour1.png')} style={{ width: '100%', height: '100%', flexDirection: 'row', alignItems: 'flex-end' }}>
			<View style={{ alignSelf: 'flex-end', alignItems: 'center', justifyContent: 'center', height: '22%', width: '100%', backgroundColor: 'rgba(16, 16, 16, 0.9)' }}>
				<Text style={{ paddingHorizontal: '10%', color: 'white', fontSize: 17, fontWeight: '500', textAlign: 'center' }}>1. Descubre todas las ofertas interesantes del Principado de Andorra</Text>
			</View>
		</ImageBackground>
		<View style={{ width: '100%', height: '100%' }}>
			<Image resizeMode='contain' source={require('./img/backtour2.png')} style={{ position: 'absolute', top: 0, width: '100%', height: '78%' }} />

			<View style={{ position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center', height: '22%', width: '100%', backgroundColor: 'rgba(16, 16, 16, 0.9)' }}>
				<Text style={{ paddingHorizontal: '10%', color: 'white', fontSize: 17, fontWeight: '500', textAlign: 'center' }}>2. Desliza a la derecha cuando quieras registrarte a la oferta o a la izquierda cuando no</Text>
			</View>
		</View>
		<View style={{ width: '100%', height: '100%', backgroundColor: 'white' }}>
		<View style={{ position: 'absolute', top: 0, width: '100%', height: '78%' }}>
		<Image resizeMode='contain' source={require('./img/backtour3.png')} style={{ position: 'absolute', top: '10%', width: '100%', height: '80%' }} />


		</View>
			
			<View style={{ position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center', height: '22%', width: '100%', backgroundColor: 'rgba(16, 16, 16, 0.9)' }}>
				<Text style={{ paddingHorizontal: '10%', color: 'white', fontSize: 17, fontWeight: '500', textAlign: 'center' }}>3. Si la empresa desvela tu identidad, entonces, ¡Entrarás en el proceso de selección!</Text>
			</View>
		</View>

		<View style={{ width: '100%', height: '100%' }}>
			<Image resizeMode='contain' source={require('./img/backtour4.png')} style={{ position: 'absolute', top: 0, width: '100%', height: '78%' }} />

			<View style={{ position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center', height: '22%', width: '100%', backgroundColor: 'rgba(16, 16, 16, 0.9)' }}>
				<Text style={{ paddingHorizontal: '10%', color: 'white', fontSize: 17, fontWeight: '500', textAlign: 'center' }}>4 Solo puedes enviar un mensaje a las empresas que inicien el chat, para concretar una entrevista.</Text>
			</View>
		</View>
		<View></View>
	</Swiper>
	</View>
	}
	render() {
		
		return (
			
			 this.state.tour=='true' ? this.renderSwiper()
					: 
						
			<View style={{ flex: 1, backgroundColor: '#f39c10' }}><MainScreens /></View> 
			

		);
	}
}

class MainScreens extends Component {
	constructor(props) {
		super(props)

		this.state = {
			screen: 'login'
		}
	}

	


	_retrieveData = async () => {
		
		try {
			const value = await AsyncStorage.getItem('tourjob');
			const token = await AsyncStorage.getItem('jobToken');
			const UID = await AsyncStorage.getItem('jobUID');
			const name = await AsyncStorage.getItem('jobNombre');
			const surname = await AsyncStorage.getItem('jobApellidos');
			const email = await AsyncStorage.getItem('jobEmail');
			
			if (value !== null) {
				// We have data!!
				this.setState({tour: value})
			} else {
				//console.log('tour', true);
			}

			if (UID !== null) {
				// We have data!!
				globales.UID = UID;
				console.log('TIENE UID', UID)
			} else {
				//console.log('NO TIENE UID')
			}
			if (name !== null) {
				// We have data!!
				globales.nombre = name;
				//console.log('TIENE NAME', name)
			} else {
				//console.log('NO TIENE name')
			}
			if (surname !== null) {
				// We have data!!
				globales.apellidos = surname;
				//console.log('TIENE surname', surname)
			} else {
				//console.log('NO TIENE surname')
			}
			if (email !== null) {
				// We have data!!
				globales.email = email;
				//console.log('TIENE email', email)
			} else {
				//console.log('NO TIENE email')
			}
			if (token !== null) {
				// We have data!!
				console.log('token', token)
				let dataBody = {
					            "user": {
					                "id": UID
					            }
					        }
				return fetch(globales.apiURL+'/api/applicant/get-applicant-profile-new',
            {
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify(dataBody)

            })
            .then((response) => {
	console.log("status START APP1", response)
                return response.json();
            })
            .then((responseJson) => {
                console.log("status START APP", responseJson)
				globales.token = token;
				//console.log('TIENE TOKEN', token)
				if(responseJson.status=="success"){
					if(responseJson.applicant.resume){
						console.log("Va a application")
						this.setState({ screen: 'application' })
					} else {
						console.log("Va a wizard")
						this.setState({ screen: 'wizard' })
					}
				}
            })
            .catch((error) => {
                console.error(error);
            });
			} else {
				//console.log('NO TIENE TOKEN')
			}
		} catch (error) {

		}
	}

	logout(){

		globales.UID = null;
		globales.token = null;
		globales.email = null;
		globales.apellidos = null;
		globales.nombre = null;
		AsyncStorage.removeItem('tourjob');
		AsyncStorage.removeItem('jobToken');
		AsyncStorage.removeItem('jobUID');
		AsyncStorage.removeItem('jobNombre');
		AsyncStorage.removeItem('jobApellidos');
		AsyncStorage.removeItem('jobEmail');
		this.setState({ screen: 'login' })

	}

	render() {
		//console.log('screen',this.state.screen)
		if (this.state.screen == 'home') {
			return (
				<Home
					enterLogin={() => this.setState({ screen: 'login' })}
				/>
			);
		}
		else if (this.state.screen == 'login') {
			 return (
				<Login
					loginPress={() => this._retrieveData()}
					fbLoginPress={() => this.setState({ screen: 'applicationSinNav' })}
					recoverPress={() => this.setState({ screen: 'recover' })}
					registerPress={() => this.setState({ screen: 'register' })}
				/>
			); 
		}
		else if (this.state.screen == 'recover') {
			return (
				<Recover
					returnPress={() => this.setState({ screen: 'login' })}
				/>
			);
		}
		else if (this.state.screen == 'register') {
			return (<Register
				returnPress={() => this.setState({ screen: 'login' })}
				userRegistered={() => this.setState({ screen: 'application' })}
			/>
			);
		}
		else if (this.state.screen == 'wizard') {
			return (<Wizard userRegistered={() => this.setState({ screen: 'application' })}/>
			);
		}
		else if (this.state.screen == 'application') {
			return (<TabNavigator onLogoutPress={() => this.logout()} />);
		}
		else if (this.state.screen == 'applicationSinNav') {
			return (<TabNavigator onLogoutPress={() => this.logout()} />);
		}
	}
	componentDidMount() {
		StatusBar.setHidden(true);
		SplashScreen.hide();
		console.log('loadingg')
				this._retrieveData();
			
	}

}


const styles = StyleSheet.create({
	selfView: {
	  width: 200,
	  height: 300,
	}
  });